#!/bin/bash
#
# Copyright (c) Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3
#

# Use Namespace already provided or the default given here
NAMESPACE="${NAMESPACE:=iot-broker}"
FULLNAME="blockchain-connector"

# Upgrade or install
helm --set-string="fullnameOverride=blockchain-connector" upgrade -n "$NAMESPACE" -i blockchain-connector . || exit 1

# Ensure image stream picks up the new docker image right away
oc -n "$NAMESPACE" import-image "${FULLNAME}-client"
#oc -n "$NAMESPACE" import-image "${FULLNAME}-chain"