/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
import { HttpModule } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';

import { AppService } from './app.service';
import { Job, Payload } from './dto/job.dto';
import { DeviceData } from './dto/publish.dto';
import { AppConfiguration } from './settings/app.config';
import { DeviceSources } from './settings/deviceSources.enum';
import tendermintConfig from './settings/tendermint.config';
import { DragonPuckService } from './shared/datastorage/dragon-puck/dragon-puck.service';
import { MockService } from './shared/datastorage/mock/mock.service';
import { SensingPuckService } from './shared/datastorage/sensing-puck/sensing-puck.service';
import { TendermintService } from './shared/tendermint/tendermint.service';

describe('AppService', () => {
  let appService: AppService;
  let mockService: MockService;
  let dragonService: DragonPuckService;
  let sensingService: SensingPuckService;
  const data: DeviceData = new DeviceData(
    12,
    5,
    4711,
    undefined,
    '2020-08-13',
    '2020-08-11',
    ['data'],
  );

  beforeEach(async () => {
    let app: TestingModule = await Test.createTestingModule({
      imports: [
        HttpModule,
        ConfigModule.forRoot({
          isGlobal: true,
          envFilePath: ['.env', '.env.tendermint'],
          load: [tendermintConfig],
        }),
      ],
      providers: [
        AppService,
        MockService,
        DragonPuckService,
        SensingPuckService,
        AppConfiguration,
        TendermintService,
      ],
    }).compile();

    mockService = app.get<MockService>(MockService);
    dragonService = app.get<DragonPuckService>(DragonPuckService);
    sensingService = app.get<SensingPuckService>(SensingPuckService);

    appService = app.get<AppService>(AppService);
  });

  it('should be defined', () => {
    expect(appService).toBeDefined();
  });

  describe('publish', () => {
    it('should not throw an exception', async () => {
      jest
        .spyOn(mockService, 'publishDeviceData')
        .mockImplementation(() => Promise.resolve());
      jest
        .spyOn(sensingService, 'publishDeviceData')
        .mockImplementation(() => Promise.resolve());

      await expect(
        appService.publishToBC(data, DeviceSources.sensingPuck),
      ).resolves.not.toThrow();
    });
  });

  describe('readDeviceData', () => {
    it('should return device data by job Id', async () => {
      let payloads: Payload[] = [
        new Payload(data.comTimestamp, data.lastComCause, data.data),
      ];

      const result: Job = new Job(data.jobId, String(data.deviceId), payloads);
      jest.spyOn(mockService, 'readJob').mockResolvedValue(result);
      jest.spyOn(dragonService, 'readJob').mockResolvedValue(result);

      expect(
        await appService.getByJobId(5, DeviceSources.dragonPuck),
      ).toStrictEqual(result);
    });
  });
});
