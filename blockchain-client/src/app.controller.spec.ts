/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
import { HttpModule } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { Test } from '@nestjs/testing';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Job, Payload } from './dto/job.dto';
import { DeviceData } from './dto/publish.dto';
import { AppConfiguration } from './settings/app.config';
import { DeviceSources } from './settings/deviceSources.enum';
import tendermintConfig from './settings/tendermint.config';
import { DragonPuckService } from './shared/datastorage/dragon-puck/dragon-puck.service';
import { MockService } from './shared/datastorage/mock/mock.service';
import { SensingPuckService } from './shared/datastorage/sensing-puck/sensing-puck.service';
import { TendermintService } from './shared/tendermint/tendermint.service';

describe('AppController', () => {
  let appController: AppController;
  let appService: AppService;
  let deviceData: DeviceData;
  let configService: ConfigService;

  beforeEach(async () => {
    const app = await Test.createTestingModule({
      imports: [
        HttpModule,
        ConfigModule.forRoot({
          isGlobal: true,
          envFilePath: ['.env', '.env.tendermint'],
          load: [tendermintConfig],
        }),
      ],
      controllers: [AppController],
      providers: [
        AppService,
        MockService,
        TendermintService,
        AppConfiguration,
        SensingPuckService,
        DragonPuckService,
      ],
    }).compile();

    deviceData = new DeviceData(
      12,
      5,
      4711,
      undefined,
      '2020-08-13',
      '2020-08-11',
      ['data'],
    );
    configService = app.get<ConfigService>(ConfigService);
    appController = app.get<AppController>(AppController);
    appService = app.get<AppService>(AppService);
  });

  describe('publish', () => {
    it('should not throw an exception', async () => {
      jest
        .spyOn(appService, 'publishToBC')
        .mockImplementation(() => Promise.resolve());

      await expect(
        appController.publish(deviceData, DeviceSources.sensingPuck),
      ).resolves.not.toThrow();
    });
  });

  describe('readDeviceData', () => {
    it('should return device data by job Id', async () => {
      const payload = new Payload(
        deviceData.comTimestamp,
        deviceData.lastComCause,
        deviceData.data,
      );

      const result = new Job(deviceData.jobId, String(deviceData.deviceId), [
        payload,
      ]);
      jest.spyOn(appService, 'getByJobId').mockResolvedValue(result);

      expect(await appController.getByJobId(5, DeviceSources.dragonPuck)).toBe(
        result,
      );
    });
  });
});
