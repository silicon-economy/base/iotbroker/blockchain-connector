/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
import { ApiProperty } from '@nestjs/swagger';

export class Payload {
  @ApiProperty({
    name: 'comTimestamp',
    type: String,
  })
  comTimestamp: string;
  @ApiProperty({
    name: 'lastComCause',
    type: String,
  })
  lastComCause: string;
  @ApiProperty({
    name: 'data',
    type: [Object],
    description: 'List of generic objects representing the device data.',
  })
  data: any[];

  constructor(comTimestamp: string, lastComCause: string, data: any[]) {
    this.comTimestamp = comTimestamp;
    this.lastComCause = lastComCause;
    this.data = data;
  }
}

export class Job {
  @ApiProperty({
    name: 'jobId',
    type: Number,
  })
  jobId: number;
  @ApiProperty({
    name: 'deviceId',
    type: String,
  })
  deviceId: string;
  @ApiProperty({
    name: 'firmwareDescriptor',
    type: {},
  })
  firmwareDescriptor: any | undefined;

  @ApiProperty({
    name: 'comObj',
    type: [Payload],
    description: 'List of communication objects from the device.',
  })
  comObj: Payload[] = [];

  constructor(jobId: number, deviceId: string, payload: Payload[]) {
    this.jobId = jobId;
    this.deviceId = deviceId;
    this.comObj = this.comObj.concat(payload);
  }
}
