/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
import { ApiProperty } from '@nestjs/swagger';

export class FirmwareDescriptor {
  constructor(
    versionNumber: string,
    commitCounter: number,
    releaseFlag: boolean,
    commitHash: number,
    dirtyFlag: boolean,
  ) {
    this.versionNumber = versionNumber;
    this.commitCounter = commitCounter;
    this.releaseFlag = releaseFlag;
    this.commitHash = commitHash;
    this.dirtyFlag = dirtyFlag;
  }

  @ApiProperty({
    name: 'versionNumber',
    type: String,
    description: 'The version number of the firmware.',
    example: '0.1.5',
    required: true,
  })
  versionNumber: string;
  @ApiProperty({
    name: 'commitCounter',
    type: Number,
    description: 'The number of commits since the last version tag.',
    required: true,
  })
  commitCounter: number;
  @ApiProperty({
    name: 'releaseFlag',
    type: Boolean,
    description:
      'Indicates whether the last version tag was a release version tag.',
    required: true,
  })
  releaseFlag: boolean;
  @ApiProperty({
    name: 'commitHash',
    type: Number,
    description: 'The first 32 bit of the firmwares commit hash.',
    required: true,
  })
  commitHash: number;
  @ApiProperty({
    name: 'dirtyFlag',
    type: Boolean,
    description:
      'Indicates whether the local repository used to build the firmware was dirty.',
    required: true,
  })
  dirtyFlag: boolean;
}

export class DeviceData {
  constructor(
    protocolVersion: number,
    jobId: number,
    deviceId: number,
    firmwareDescriptor: FirmwareDescriptor | undefined,
    comTimestamp: string,
    lastComCause: string,
    data: any[],
  ) {
    this.protocolVersion = protocolVersion;
    this.jobId = jobId;
    this.deviceId = deviceId;
    this.firmwareDescriptor = firmwareDescriptor;
    this.comTimestamp = comTimestamp;
    this.lastComCause = lastComCause;
    this.data = data;
  }

  @ApiProperty({
    name: 'protocolVersion',
    type: Number,
    description: 'The protocol version.',
    required: true,
  })
  protocolVersion: number;
  @ApiProperty({
    name: 'jobId',
    type: Number,
    description: 'Identifier of the device data object.',
    required: true,
  })
  jobId: number;
  @ApiProperty({
    name: 'deviceId',
    type: Number,
    description: 'Identifier of the device.',
    required: true,
  })
  deviceId: number;

  @ApiProperty({
    name: 'firmwareDescriptor',
    type: FirmwareDescriptor,
    description: 'Identifier of the device.',
    required: true,
  })
  firmwareDescriptor: FirmwareDescriptor | undefined;

  @ApiProperty({
    name: 'comTimestamp',
    type: String,
    example: '2021-07-23T10:31:33Z',
    required: true,
  })
  comTimestamp: string;
  @ApiProperty({
    name: 'lastComCause',
    type: String,
    required: true,
  })
  lastComCause: string;
  @ApiProperty({
    name: 'data',
    type: [],
    description: 'List of generic objects representing the device data.',
    required: true,
  })
  data: any;
}
