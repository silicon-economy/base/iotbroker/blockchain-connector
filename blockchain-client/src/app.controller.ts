/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
import { Body, Controller, Get, HttpCode, Param, Post } from '@nestjs/common';
import { ApiBody, ApiOperation, ApiParam, ApiResponse } from '@nestjs/swagger';

import { AppService } from './app.service';
import { Job } from './dto/job.dto';
import { DeviceData } from './dto/publish.dto';
import { DeviceSources } from './settings/deviceSources.enum';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post('/devices/:source/publish')
  @HttpCode(201)
  @ApiOperation({
    summary: 'Persist device data on blockchain network.',
  })
  @ApiParam({
    name: 'source',
    enum: DeviceSources,
    description: 'Identifier of the source use case.',
  })
  @ApiBody({
    type: DeviceData,
    description: 'Device data to be persisted on the blockchain',
  })
  async publish(
    @Body() data: DeviceData,
    @Param('source') devicesource: DeviceSources,
  ): Promise<void> {
    this.appService.publishToBC(data, devicesource);
  }

  @Get('/devices/:source/:jobId')
  @HttpCode(200)
  @ApiOperation({
    summary: 'Get device data by job id from blockchain.',
  })
  @ApiParam({
    name: 'jobId',
    type: Number,
    description: 'Identifier of a persisted device data object.',
  })
  @ApiParam({
    name: 'source',
    enum: DeviceSources,
    description: 'Identifier of the source use case.',
  })
  @ApiResponse({
    type: Job,
  })
  async getByJobId(
    @Param('jobId') jobId: number,
    @Param('source') devicesource: DeviceSources,
  ): Promise<Job> {
    return this.appService.getByJobId(jobId, devicesource);
  }
}
