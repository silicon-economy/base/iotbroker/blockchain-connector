/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

import { TendermintModule } from '../../tendermint/tendermint.module';
import { DragonPuckService } from './dragon-puck.service';

@Module({
  imports: [TendermintModule],
  controllers: [],
  providers: [DragonPuckService, ConfigService],
  exports: [DragonPuckService],
})
export class DragonPuckModule {}
