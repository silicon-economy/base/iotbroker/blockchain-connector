/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

import { Job, Payload } from '../../../dto/job.dto';
import { DeviceData } from '../../../dto/publish.dto';
import { Dragon_Identifier } from '../../../settings/dragon.config';
import { TendermintConfig } from '../../../settings/tendermint.config';
import { TendermintService } from '../../tendermint/tendermint.service';
import { BCConnector } from '../bc-connector';
import { GetDragonJobDto } from './dto/get-devicedata.dto';
import {
  DragonDeviceData,
  DragonFirmwareDescriptor,
} from './dto/upload-devicedata.dto';
import { MsgUploadDeviceData } from './dto/generated/tx';

@Injectable()
export class DragonPuckService implements BCConnector {
  private logger = new Logger(DragonPuckService.name);
  private dragonConfig: TendermintConfig;

  constructor(
    private tendermint: TendermintService,
    private configService: ConfigService,
  ) {
    this.dragonConfig =
      this.configService.get<TendermintConfig>(Dragon_Identifier);
  }

  async publishDeviceData(job: DeviceData): Promise<void> {
    try {
      let msgType: string = this.dragonConfig.createJobMsgType;
      let dragonPublishJob: DragonDeviceData = this.createMsgUploadDevice(job);

      await this.tendermint.writeToChain(
        msgType,
        dragonPublishJob,
        this.dragonConfig,
        this.createCosmosTypes(),
      );
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }

  async readJob(jobId: number): Promise<Job> {
    try {
      let responseDto: GetDragonJobDto = await this.tendermint.readFromChain(
        `${this.dragonConfig.restGetJob}${jobId}`,
        this.dragonConfig,
      );

      let payloads: Payload[] = this.createPayloadFromBCResponse(responseDto);
      let retVal = new Job(
        responseDto.deviceData[0].jobId,
        responseDto.deviceData[0].deviceId,
        payloads,
      );
      retVal.firmwareDescriptor = responseDto.deviceData[0].firmwareDescriptor;
      return retVal;
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }

  private createPayloadFromBCResponse(responseDto: GetDragonJobDto): Payload[] {
    let payloads: Payload[] = [];
    responseDto.deviceData.forEach((deviceData) => {
      deviceData.data.forEach((element, index: number) => {
        try {
          deviceData.data[index] = JSON.parse(element);
        } catch (e) {
          deviceData.data[index] = element;
        }
      });
      payloads.push(
        new Payload(
          deviceData.comTimestamp,
          deviceData.lastComCause,
          deviceData.data,
        ),
      );
    });
    return payloads;
  }

  private createMsgUploadDevice(job: DeviceData): DragonDeviceData {
    let firmwareDescriptor = undefined;
    if (job.firmwareDescriptor !== undefined) {
      firmwareDescriptor = new DragonFirmwareDescriptor(job);
    }
    let uploadMsg = new DragonDeviceData(job, firmwareDescriptor);

    job.data.forEach((element, index: number) => {
      uploadMsg.data[index] = element;
    });
    return uploadMsg;
  }

  private createCosmosTypes(): any[][] {
    return [
      [
        `${this.dragonConfig.chainType}.${this.dragonConfig.createJobMsgType}`,
        MsgUploadDeviceData,
      ],
    ];
  }
}
