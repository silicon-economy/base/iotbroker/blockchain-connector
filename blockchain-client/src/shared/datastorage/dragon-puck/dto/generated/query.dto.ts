/**
 * Copyright 2022 Fraunhofer IML
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */

export interface QueryDto {
  nodeUri?: string;
  chainId?: string;
  path: string;
}
