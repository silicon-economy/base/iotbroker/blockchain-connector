/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/* eslint-disable */
import * as Long from 'long';
import { util, configure, Writer, Reader } from 'protobufjs/minimal';
import { TransportDocument } from './/transport_document';
import { DeviceJobData } from './/device_job_data';
import { TransportDocumentIdByOrderPositionId } from './/transport_document_id_by_order_position_id';
import { DangerousGoodRegistration } from './/dangerous_good_registration';
import { PastEvent } from './/past_event';

export const protobufPackage =
  'git.openlogisticsfoundation.org.silicon_economy.services.dangerous.core.apps.generated.businesslogic';

/** GenesisState defines the dangerousblockchain module's genesis state. */
export interface GenesisState {
  transportDocumentList: TransportDocument[];
  transportDocumentCount: number;
  deviceJobDataList: DeviceJobData[];
  deviceJobDataCount: number;
  transportDocumentIdByOrderPositionIdList: TransportDocumentIdByOrderPositionId[];
  dangerousGoodRegistrationList: DangerousGoodRegistration[];
  pastEventList: PastEvent[];
  /** this line is used by starport scaffolding # genesis/proto/state */
  pastEventCount: number;
}

const baseGenesisState: object = { transportDocumentCount: 0, deviceJobDataCount: 0, pastEventCount: 0 };

export const GenesisState = {
  encode(message: GenesisState, writer: Writer = Writer.create()): Writer {
    for (const v of message.transportDocumentList) {
      TransportDocument.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.transportDocumentCount !== 0) {
      writer.uint32(16).uint64(message.transportDocumentCount);
    }
    for (const v of message.deviceJobDataList) {
      DeviceJobData.encode(v!, writer.uint32(26).fork()).ldelim();
    }
    if (message.deviceJobDataCount !== 0) {
      writer.uint32(32).uint64(message.deviceJobDataCount);
    }
    for (const v of message.transportDocumentIdByOrderPositionIdList) {
      TransportDocumentIdByOrderPositionId.encode(v!, writer.uint32(42).fork()).ldelim();
    }
    for (const v of message.dangerousGoodRegistrationList) {
      DangerousGoodRegistration.encode(v!, writer.uint32(50).fork()).ldelim();
    }
    for (const v of message.pastEventList) {
      PastEvent.encode(v!, writer.uint32(58).fork()).ldelim();
    }
    if (message.pastEventCount !== 0) {
      writer.uint32(64).uint64(message.pastEventCount);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): GenesisState {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseGenesisState } as GenesisState;
    message.transportDocumentList = [];
    message.deviceJobDataList = [];
    message.transportDocumentIdByOrderPositionIdList = [];
    message.dangerousGoodRegistrationList = [];
    message.pastEventList = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.transportDocumentList.push(TransportDocument.decode(reader, reader.uint32()));
          break;
        case 2:
          message.transportDocumentCount = longToNumber(reader.uint64() as Long);
          break;
        case 3:
          message.deviceJobDataList.push(DeviceJobData.decode(reader, reader.uint32()));
          break;
        case 4:
          message.deviceJobDataCount = longToNumber(reader.uint64() as Long);
          break;
        case 5:
          message.transportDocumentIdByOrderPositionIdList.push(
            TransportDocumentIdByOrderPositionId.decode(reader, reader.uint32())
          );
          break;
        case 6:
          message.dangerousGoodRegistrationList.push(DangerousGoodRegistration.decode(reader, reader.uint32()));
          break;
        case 7:
          message.pastEventList.push(PastEvent.decode(reader, reader.uint32()));
          break;
        case 8:
          message.pastEventCount = longToNumber(reader.uint64() as Long);
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GenesisState {
    const message = { ...baseGenesisState } as GenesisState;
    message.transportDocumentList = [];
    message.deviceJobDataList = [];
    message.transportDocumentIdByOrderPositionIdList = [];
    message.dangerousGoodRegistrationList = [];
    message.pastEventList = [];
    if (object.transportDocumentList !== undefined && object.transportDocumentList !== null) {
      for (const e of object.transportDocumentList) {
        message.transportDocumentList.push(TransportDocument.fromJSON(e));
      }
    }
    if (object.transportDocumentCount !== undefined && object.transportDocumentCount !== null) {
      message.transportDocumentCount = Number(object.transportDocumentCount);
    } else {
      message.transportDocumentCount = 0;
    }
    if (object.deviceJobDataList !== undefined && object.deviceJobDataList !== null) {
      for (const e of object.deviceJobDataList) {
        message.deviceJobDataList.push(DeviceJobData.fromJSON(e));
      }
    }
    if (object.deviceJobDataCount !== undefined && object.deviceJobDataCount !== null) {
      message.deviceJobDataCount = Number(object.deviceJobDataCount);
    } else {
      message.deviceJobDataCount = 0;
    }
    if (
      object.transportDocumentIdByOrderPositionIdList !== undefined &&
      object.transportDocumentIdByOrderPositionIdList !== null
    ) {
      for (const e of object.transportDocumentIdByOrderPositionIdList) {
        message.transportDocumentIdByOrderPositionIdList.push(TransportDocumentIdByOrderPositionId.fromJSON(e));
      }
    }
    if (object.dangerousGoodRegistrationList !== undefined && object.dangerousGoodRegistrationList !== null) {
      for (const e of object.dangerousGoodRegistrationList) {
        message.dangerousGoodRegistrationList.push(DangerousGoodRegistration.fromJSON(e));
      }
    }
    if (object.pastEventList !== undefined && object.pastEventList !== null) {
      for (const e of object.pastEventList) {
        message.pastEventList.push(PastEvent.fromJSON(e));
      }
    }
    if (object.pastEventCount !== undefined && object.pastEventCount !== null) {
      message.pastEventCount = Number(object.pastEventCount);
    } else {
      message.pastEventCount = 0;
    }
    return message;
  },

  toJSON(message: GenesisState): unknown {
    const obj: any = {};
    if (message.transportDocumentList) {
      obj.transportDocumentList = message.transportDocumentList.map((e) =>
        e ? TransportDocument.toJSON(e) : undefined
      );
    } else {
      obj.transportDocumentList = [];
    }
    message.transportDocumentCount !== undefined && (obj.transportDocumentCount = message.transportDocumentCount);
    if (message.deviceJobDataList) {
      obj.deviceJobDataList = message.deviceJobDataList.map((e) => (e ? DeviceJobData.toJSON(e) : undefined));
    } else {
      obj.deviceJobDataList = [];
    }
    message.deviceJobDataCount !== undefined && (obj.deviceJobDataCount = message.deviceJobDataCount);
    if (message.transportDocumentIdByOrderPositionIdList) {
      obj.transportDocumentIdByOrderPositionIdList = message.transportDocumentIdByOrderPositionIdList.map((e) =>
        e ? TransportDocumentIdByOrderPositionId.toJSON(e) : undefined
      );
    } else {
      obj.transportDocumentIdByOrderPositionIdList = [];
    }
    if (message.dangerousGoodRegistrationList) {
      obj.dangerousGoodRegistrationList = message.dangerousGoodRegistrationList.map((e) =>
        e ? DangerousGoodRegistration.toJSON(e) : undefined
      );
    } else {
      obj.dangerousGoodRegistrationList = [];
    }
    if (message.pastEventList) {
      obj.pastEventList = message.pastEventList.map((e) => (e ? PastEvent.toJSON(e) : undefined));
    } else {
      obj.pastEventList = [];
    }
    message.pastEventCount !== undefined && (obj.pastEventCount = message.pastEventCount);
    return obj;
  },

  fromPartial(object: DeepPartial<GenesisState>): GenesisState {
    const message = { ...baseGenesisState } as GenesisState;
    message.transportDocumentList = [];
    message.deviceJobDataList = [];
    message.transportDocumentIdByOrderPositionIdList = [];
    message.dangerousGoodRegistrationList = [];
    message.pastEventList = [];
    if (object.transportDocumentList !== undefined && object.transportDocumentList !== null) {
      for (const e of object.transportDocumentList) {
        message.transportDocumentList.push(TransportDocument.fromPartial(e));
      }
    }
    if (object.transportDocumentCount !== undefined && object.transportDocumentCount !== null) {
      message.transportDocumentCount = object.transportDocumentCount;
    } else {
      message.transportDocumentCount = 0;
    }
    if (object.deviceJobDataList !== undefined && object.deviceJobDataList !== null) {
      for (const e of object.deviceJobDataList) {
        message.deviceJobDataList.push(DeviceJobData.fromPartial(e));
      }
    }
    if (object.deviceJobDataCount !== undefined && object.deviceJobDataCount !== null) {
      message.deviceJobDataCount = object.deviceJobDataCount;
    } else {
      message.deviceJobDataCount = 0;
    }
    if (
      object.transportDocumentIdByOrderPositionIdList !== undefined &&
      object.transportDocumentIdByOrderPositionIdList !== null
    ) {
      for (const e of object.transportDocumentIdByOrderPositionIdList) {
        message.transportDocumentIdByOrderPositionIdList.push(TransportDocumentIdByOrderPositionId.fromPartial(e));
      }
    }
    if (object.dangerousGoodRegistrationList !== undefined && object.dangerousGoodRegistrationList !== null) {
      for (const e of object.dangerousGoodRegistrationList) {
        message.dangerousGoodRegistrationList.push(DangerousGoodRegistration.fromPartial(e));
      }
    }
    if (object.pastEventList !== undefined && object.pastEventList !== null) {
      for (const e of object.pastEventList) {
        message.pastEventList.push(PastEvent.fromPartial(e));
      }
    }
    if (object.pastEventCount !== undefined && object.pastEventCount !== null) {
      message.pastEventCount = object.pastEventCount;
    } else {
      message.pastEventCount = 0;
    }
    return message;
  },
};

declare var self: any | undefined;
declare var window: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== 'undefined') return globalThis;
  if (typeof self !== 'undefined') return self;
  if (typeof window !== 'undefined') return window;
  if (typeof global !== 'undefined') return global;
  throw 'Unable to locate global object';
})();

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error('Value is larger than Number.MAX_SAFE_INTEGER');
  }
  return long.toNumber();
}

if (true) {
  util.Long = Long as any;
  configure();
}
