/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/* eslint-disable */

import { Writer, Reader } from "protobufjs/minimal";
import { Company, Freight } from "./transportDocument.dto";

export const protobufPackage = "git.openlogisticsfoundation.org.silicon_economy.services.dangerous.core.apps.generated.businesslogic";

export interface DangerousGoodRegistration {
  creator: string;
  id: string;
  sender: Company | undefined;
  freight: Freight | undefined;
  createdDate: number;
  lastUpdate: number;
}

const baseDangerousGoodRegistration: object = {
  creator: "",
  id: "",
  createdDate: 0,
  lastUpdate: 0,
};

export const DangerousGoodRegistration = {
  encode(
    message: DangerousGoodRegistration,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.sender !== undefined) {
      Company.encode(message.sender, writer.uint32(26).fork()).ldelim();
    }
    if (message.freight !== undefined) {
      Freight.encode(message.freight, writer.uint32(34).fork()).ldelim();
    }
    if (message.createdDate !== 0) {
      writer.uint32(41).double(message.createdDate);
    }
    if (message.lastUpdate !== 0) {
      writer.uint32(49).double(message.lastUpdate);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): DangerousGoodRegistration {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseDangerousGoodRegistration,
    } as DangerousGoodRegistration;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.sender = Company.decode(reader, reader.uint32());
          break;
        case 4:
          message.freight = Freight.decode(reader, reader.uint32());
          break;
        case 5:
          message.createdDate = reader.double();
          break;
        case 6:
          message.lastUpdate = reader.double();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): DangerousGoodRegistration {
    const message = {
      ...baseDangerousGoodRegistration,
    } as DangerousGoodRegistration;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    if (object.sender !== undefined && object.sender !== null) {
      message.sender = Company.fromJSON(object.sender);
    } else {
      message.sender = undefined;
    }
    if (object.freight !== undefined && object.freight !== null) {
      message.freight = Freight.fromJSON(object.freight);
    } else {
      message.freight = undefined;
    }
    if (object.createdDate !== undefined && object.createdDate !== null) {
      message.createdDate = Number(object.createdDate);
    } else {
      message.createdDate = 0;
    }
    if (object.lastUpdate !== undefined && object.lastUpdate !== null) {
      message.lastUpdate = Number(object.lastUpdate);
    } else {
      message.lastUpdate = 0;
    }
    return message;
  },

  toJSON(message: DangerousGoodRegistration): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.sender !== undefined &&
      (obj.sender = message.sender
        ? Company.toJSON(message.sender)
        : undefined);
    message.freight !== undefined &&
      (obj.freight = message.freight
        ? Freight.toJSON(message.freight)
        : undefined);
    message.createdDate !== undefined &&
      (obj.createdDate = message.createdDate);
    message.lastUpdate !== undefined && (obj.lastUpdate = message.lastUpdate);
    return obj;
  },

  fromPartial(
    object: DeepPartial<DangerousGoodRegistration>
  ): DangerousGoodRegistration {
    const message = {
      ...baseDangerousGoodRegistration,
    } as DangerousGoodRegistration;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    if (object.sender !== undefined && object.sender !== null) {
      message.sender = Company.fromPartial(object.sender);
    } else {
      message.sender = undefined;
    }
    if (object.freight !== undefined && object.freight !== null) {
      message.freight = Freight.fromPartial(object.freight);
    } else {
      message.freight = undefined;
    }
    if (object.createdDate !== undefined && object.createdDate !== null) {
      message.createdDate = object.createdDate;
    } else {
      message.createdDate = 0;
    }
    if (object.lastUpdate !== undefined && object.lastUpdate !== null) {
      message.lastUpdate = object.lastUpdate;
    } else {
      message.lastUpdate = 0;
    }
    return message;
  },
};

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;
