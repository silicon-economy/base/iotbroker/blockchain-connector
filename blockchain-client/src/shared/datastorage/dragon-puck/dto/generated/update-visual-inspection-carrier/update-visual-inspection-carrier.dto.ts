/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/* eslint-disable */
import * as Long from 'long';
import { util, configure, Writer, Reader } from 'protobufjs/minimal';

export const protobufPackage =
  'git.openlogisticsfoundation.org.silicon_economy.services.dangerous.core.apps.generated.businesslogic';

export interface MsgOrderVisualInspectionCarrierDto {
  orderId: string;
  orderPositions: MsgOrderPositionVisualInspectionCarrierDto[];
}

export interface MsgOrderPositionVisualInspectionCarrierDto {
  orderPositionId: number;
  acceptTransportability: boolean;
  acceptLabeling: boolean;
  comment: string;
}

const baseMsgOrderVisualInspectionCarrierDto: object = { orderId: '' };

export const MsgOrderVisualInspectionCarrierDto = {
  encode(message: MsgOrderVisualInspectionCarrierDto, writer: Writer = Writer.create()): Writer {
    if (message.orderId !== '') {
      writer.uint32(10).string(message.orderId);
    }
    for (const v of message.orderPositions) {
      MsgOrderPositionVisualInspectionCarrierDto.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgOrderVisualInspectionCarrierDto {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgOrderVisualInspectionCarrierDto } as MsgOrderVisualInspectionCarrierDto;
    message.orderPositions = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.orderId = reader.string();
          break;
        case 2:
          message.orderPositions.push(MsgOrderPositionVisualInspectionCarrierDto.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgOrderVisualInspectionCarrierDto {
    const message = { ...baseMsgOrderVisualInspectionCarrierDto } as MsgOrderVisualInspectionCarrierDto;
    message.orderPositions = [];
    if (object.orderId !== undefined && object.orderId !== null) {
      message.orderId = String(object.orderId);
    } else {
      message.orderId = '';
    }
    if (object.orderPositions !== undefined && object.orderPositions !== null) {
      for (const e of object.orderPositions) {
        message.orderPositions.push(MsgOrderPositionVisualInspectionCarrierDto.fromJSON(e));
      }
    }
    return message;
  },

  toJSON(message: MsgOrderVisualInspectionCarrierDto): unknown {
    const obj: any = {};
    message.orderId !== undefined && (obj.orderId = message.orderId);
    if (message.orderPositions) {
      obj.orderPositions = message.orderPositions.map((e) =>
        e ? MsgOrderPositionVisualInspectionCarrierDto.toJSON(e) : undefined
      );
    } else {
      obj.orderPositions = [];
    }
    return obj;
  },

  fromPartial(object: DeepPartial<MsgOrderVisualInspectionCarrierDto>): MsgOrderVisualInspectionCarrierDto {
    const message = { ...baseMsgOrderVisualInspectionCarrierDto } as MsgOrderVisualInspectionCarrierDto;
    message.orderPositions = [];
    if (object.orderId !== undefined && object.orderId !== null) {
      message.orderId = object.orderId;
    } else {
      message.orderId = '';
    }
    if (object.orderPositions !== undefined && object.orderPositions !== null) {
      for (const e of object.orderPositions) {
        message.orderPositions.push(MsgOrderPositionVisualInspectionCarrierDto.fromPartial(e));
      }
    }
    return message;
  },
};

const baseMsgOrderPositionVisualInspectionCarrierDto: object = {
  orderPositionId: 0,
  acceptTransportability: false,
  acceptLabeling: false,
  comment: '',
};

export const MsgOrderPositionVisualInspectionCarrierDto = {
  encode(message: MsgOrderPositionVisualInspectionCarrierDto, writer: Writer = Writer.create()): Writer {
    if (message.orderPositionId !== 0) {
      writer.uint32(8).uint64(message.orderPositionId);
    }
    if (message.acceptTransportability === true) {
      writer.uint32(16).bool(message.acceptTransportability);
    }
    if (message.acceptLabeling === true) {
      writer.uint32(24).bool(message.acceptLabeling);
    }
    if (message.comment !== '') {
      writer.uint32(34).string(message.comment);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgOrderPositionVisualInspectionCarrierDto {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgOrderPositionVisualInspectionCarrierDto } as MsgOrderPositionVisualInspectionCarrierDto;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.orderPositionId = longToNumber(reader.uint64() as Long);
          break;
        case 2:
          message.acceptTransportability = reader.bool();
          break;
        case 3:
          message.acceptLabeling = reader.bool();
          break;
        case 4:
          message.comment = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgOrderPositionVisualInspectionCarrierDto {
    const message = { ...baseMsgOrderPositionVisualInspectionCarrierDto } as MsgOrderPositionVisualInspectionCarrierDto;
    if (object.orderPositionId !== undefined && object.orderPositionId !== null) {
      message.orderPositionId = Number(object.orderPositionId);
    } else {
      message.orderPositionId = 0;
    }
    if (object.acceptTransportability !== undefined && object.acceptTransportability !== null) {
      message.acceptTransportability = Boolean(object.acceptTransportability);
    } else {
      message.acceptTransportability = false;
    }
    if (object.acceptLabeling !== undefined && object.acceptLabeling !== null) {
      message.acceptLabeling = Boolean(object.acceptLabeling);
    } else {
      message.acceptLabeling = false;
    }
    if (object.comment !== undefined && object.comment !== null) {
      message.comment = String(object.comment);
    } else {
      message.comment = '';
    }
    return message;
  },

  toJSON(message: MsgOrderPositionVisualInspectionCarrierDto): unknown {
    const obj: any = {};
    message.orderPositionId !== undefined && (obj.orderPositionId = message.orderPositionId);
    message.acceptTransportability !== undefined && (obj.acceptTransportability = message.acceptTransportability);
    message.acceptLabeling !== undefined && (obj.acceptLabeling = message.acceptLabeling);
    message.comment !== undefined && (obj.comment = message.comment);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgOrderPositionVisualInspectionCarrierDto>
  ): MsgOrderPositionVisualInspectionCarrierDto {
    const message = { ...baseMsgOrderPositionVisualInspectionCarrierDto } as MsgOrderPositionVisualInspectionCarrierDto;
    if (object.orderPositionId !== undefined && object.orderPositionId !== null) {
      message.orderPositionId = object.orderPositionId;
    } else {
      message.orderPositionId = 0;
    }
    if (object.acceptTransportability !== undefined && object.acceptTransportability !== null) {
      message.acceptTransportability = object.acceptTransportability;
    } else {
      message.acceptTransportability = false;
    }
    if (object.acceptLabeling !== undefined && object.acceptLabeling !== null) {
      message.acceptLabeling = object.acceptLabeling;
    } else {
      message.acceptLabeling = false;
    }
    if (object.comment !== undefined && object.comment !== null) {
      message.comment = object.comment;
    } else {
      message.comment = '';
    }
    return message;
  },
};

declare var self: any | undefined;
declare var window: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== 'undefined') return globalThis;
  if (typeof self !== 'undefined') return self;
  if (typeof window !== 'undefined') return window;
  if (typeof global !== 'undefined') return global;
  throw 'Unable to locate global object';
})();

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error('Value is larger than Number.MAX_SAFE_INTEGER');
  }
  return long.toNumber();
}

if (true) {
  util.Long = Long as any;
  configure();
}
