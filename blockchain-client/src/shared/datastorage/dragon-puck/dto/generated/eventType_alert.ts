/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/* eslint-disable */
import * as Long from 'long';
import { util, configure, Writer, Reader } from 'protobufjs/minimal';

export const protobufPackage =
  'git.openlogisticsfoundation.org.silicon_economy.services.dangerous.core.apps.generated.businesslogic';

export interface EventTypeAlert {
  isEventTypeAlert: boolean;
  creator: string;
  deviceId: number;
  jobId: number;
  temperature: number;
  upperTemperatureThreshold: number;
  lowerTemperatureThreshold: number;
  humidity: number;
  upperHumidityThreshold: number;
  lowerHumidityThreshold: number;
  timestamp: string;
}

const baseEventTypeAlert: object = {
  isEventTypeAlert: false,
  creator: '',
  deviceId: 0,
  jobId: 0,
  temperature: 0,
  upperTemperatureThreshold: 0,
  lowerTemperatureThreshold: 0,
  humidity: 0,
  upperHumidityThreshold: 0,
  lowerHumidityThreshold: 0,
  timestamp: '',
};

export const EventTypeAlert = {
  encode(message: EventTypeAlert, writer: Writer = Writer.create()): Writer {
    if (message.isEventTypeAlert === true) {
      writer.uint32(8).bool(message.isEventTypeAlert);
    }
    if (message.creator !== '') {
      writer.uint32(18).string(message.creator);
    }
    if (message.deviceId !== 0) {
      writer.uint32(24).uint64(message.deviceId);
    }
    if (message.jobId !== 0) {
      writer.uint32(32).uint64(message.jobId);
    }
    if (message.temperature !== 0) {
      writer.uint32(45).float(message.temperature);
    }
    if (message.upperTemperatureThreshold !== 0) {
      writer.uint32(53).float(message.upperTemperatureThreshold);
    }
    if (message.lowerTemperatureThreshold !== 0) {
      writer.uint32(61).float(message.lowerTemperatureThreshold);
    }
    if (message.humidity !== 0) {
      writer.uint32(69).float(message.humidity);
    }
    if (message.upperHumidityThreshold !== 0) {
      writer.uint32(77).float(message.upperHumidityThreshold);
    }
    if (message.lowerHumidityThreshold !== 0) {
      writer.uint32(85).float(message.lowerHumidityThreshold);
    }
    if (message.timestamp !== '') {
      writer.uint32(90).string(message.timestamp);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): EventTypeAlert {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseEventTypeAlert } as EventTypeAlert;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.isEventTypeAlert = reader.bool();
          break;
        case 2:
          message.creator = reader.string();
          break;
        case 3:
          message.deviceId = longToNumber(reader.uint64() as Long);
          break;
        case 4:
          message.jobId = longToNumber(reader.uint64() as Long);
          break;
        case 5:
          message.temperature = reader.float();
          break;
        case 6:
          message.upperTemperatureThreshold = reader.float();
          break;
        case 7:
          message.lowerTemperatureThreshold = reader.float();
          break;
        case 8:
          message.humidity = reader.float();
          break;
        case 9:
          message.upperHumidityThreshold = reader.float();
          break;
        case 10:
          message.lowerHumidityThreshold = reader.float();
          break;
        case 11:
          message.timestamp = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): EventTypeAlert {
    const message = { ...baseEventTypeAlert } as EventTypeAlert;
    if (object.isEventTypeAlert !== undefined && object.isEventTypeAlert !== null) {
      message.isEventTypeAlert = Boolean(object.isEventTypeAlert);
    } else {
      message.isEventTypeAlert = false;
    }
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = '';
    }
    if (object.deviceId !== undefined && object.deviceId !== null) {
      message.deviceId = Number(object.deviceId);
    } else {
      message.deviceId = 0;
    }
    if (object.jobId !== undefined && object.jobId !== null) {
      message.jobId = Number(object.jobId);
    } else {
      message.jobId = 0;
    }
    if (object.temperature !== undefined && object.temperature !== null) {
      message.temperature = Number(object.temperature);
    } else {
      message.temperature = 0;
    }
    if (object.upperTemperatureThreshold !== undefined && object.upperTemperatureThreshold !== null) {
      message.upperTemperatureThreshold = Number(object.upperTemperatureThreshold);
    } else {
      message.upperTemperatureThreshold = 0;
    }
    if (object.lowerTemperatureThreshold !== undefined && object.lowerTemperatureThreshold !== null) {
      message.lowerTemperatureThreshold = Number(object.lowerTemperatureThreshold);
    } else {
      message.lowerTemperatureThreshold = 0;
    }
    if (object.humidity !== undefined && object.humidity !== null) {
      message.humidity = Number(object.humidity);
    } else {
      message.humidity = 0;
    }
    if (object.upperHumidityThreshold !== undefined && object.upperHumidityThreshold !== null) {
      message.upperHumidityThreshold = Number(object.upperHumidityThreshold);
    } else {
      message.upperHumidityThreshold = 0;
    }
    if (object.lowerHumidityThreshold !== undefined && object.lowerHumidityThreshold !== null) {
      message.lowerHumidityThreshold = Number(object.lowerHumidityThreshold);
    } else {
      message.lowerHumidityThreshold = 0;
    }
    if (object.timestamp !== undefined && object.timestamp !== null) {
      message.timestamp = String(object.timestamp);
    } else {
      message.timestamp = '';
    }
    return message;
  },

  toJSON(message: EventTypeAlert): unknown {
    const obj: any = {};
    message.isEventTypeAlert !== undefined && (obj.isEventTypeAlert = message.isEventTypeAlert);
    message.creator !== undefined && (obj.creator = message.creator);
    message.deviceId !== undefined && (obj.deviceId = message.deviceId);
    message.jobId !== undefined && (obj.jobId = message.jobId);
    message.temperature !== undefined && (obj.temperature = message.temperature);
    message.upperTemperatureThreshold !== undefined &&
      (obj.upperTemperatureThreshold = message.upperTemperatureThreshold);
    message.lowerTemperatureThreshold !== undefined &&
      (obj.lowerTemperatureThreshold = message.lowerTemperatureThreshold);
    message.humidity !== undefined && (obj.humidity = message.humidity);
    message.upperHumidityThreshold !== undefined && (obj.upperHumidityThreshold = message.upperHumidityThreshold);
    message.lowerHumidityThreshold !== undefined && (obj.lowerHumidityThreshold = message.lowerHumidityThreshold);
    message.timestamp !== undefined && (obj.timestamp = message.timestamp);
    return obj;
  },

  fromPartial(object: DeepPartial<EventTypeAlert>): EventTypeAlert {
    const message = { ...baseEventTypeAlert } as EventTypeAlert;
    if (object.isEventTypeAlert !== undefined && object.isEventTypeAlert !== null) {
      message.isEventTypeAlert = object.isEventTypeAlert;
    } else {
      message.isEventTypeAlert = false;
    }
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = '';
    }
    if (object.deviceId !== undefined && object.deviceId !== null) {
      message.deviceId = object.deviceId;
    } else {
      message.deviceId = 0;
    }
    if (object.jobId !== undefined && object.jobId !== null) {
      message.jobId = object.jobId;
    } else {
      message.jobId = 0;
    }
    if (object.temperature !== undefined && object.temperature !== null) {
      message.temperature = object.temperature;
    } else {
      message.temperature = 0;
    }
    if (object.upperTemperatureThreshold !== undefined && object.upperTemperatureThreshold !== null) {
      message.upperTemperatureThreshold = object.upperTemperatureThreshold;
    } else {
      message.upperTemperatureThreshold = 0;
    }
    if (object.lowerTemperatureThreshold !== undefined && object.lowerTemperatureThreshold !== null) {
      message.lowerTemperatureThreshold = object.lowerTemperatureThreshold;
    } else {
      message.lowerTemperatureThreshold = 0;
    }
    if (object.humidity !== undefined && object.humidity !== null) {
      message.humidity = object.humidity;
    } else {
      message.humidity = 0;
    }
    if (object.upperHumidityThreshold !== undefined && object.upperHumidityThreshold !== null) {
      message.upperHumidityThreshold = object.upperHumidityThreshold;
    } else {
      message.upperHumidityThreshold = 0;
    }
    if (object.lowerHumidityThreshold !== undefined && object.lowerHumidityThreshold !== null) {
      message.lowerHumidityThreshold = object.lowerHumidityThreshold;
    } else {
      message.lowerHumidityThreshold = 0;
    }
    if (object.timestamp !== undefined && object.timestamp !== null) {
      message.timestamp = object.timestamp;
    } else {
      message.timestamp = '';
    }
    return message;
  },
};

declare var self: any | undefined;
declare var window: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== 'undefined') return globalThis;
  if (typeof self !== 'undefined') return self;
  if (typeof window !== 'undefined') return window;
  if (typeof global !== 'undefined') return global;
  throw 'Unable to locate global object';
})();

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error('Value is larger than Number.MAX_SAFE_INTEGER');
  }
  return long.toNumber();
}

if (true) {
  util.Long = Long as any;
  configure();
}
