/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/* eslint-disable */
import * as Long from 'long';
import { configure, Reader, util, Writer } from 'protobufjs/minimal';
import { AcceptDangerousGoodRegistrationDto } from './acceptDangerousGoodRegistration.dto';
import { DangerousGoodRegistration } from './dangerousGoodRegistration.dto';
import { Data, FirmwareDescriptor } from './deviceJobData.dto';
import { Carrier, Company, Freight, LogEntry, TransportDocument } from './transportDocument.dto';
import { MsgOrderVisualInspectionCarrierDto } from './update-visual-inspection-carrier/update-visual-inspection-carrier.dto';
import { MsgOrderVisualInspectionConsigneeDto } from './update-visual-inspection-consignee/update-visual-inspection-consignee.dto';

export const protobufPackage = 'git.openlogisticsfoundation.org.silicon_economy.services.dangerous.core.apps.generated.businesslogic';

export interface MsgCreateTransportDocument {
  creator: string;
  sender: Company | undefined;
  carrier: Carrier | undefined;
  freight: Freight | undefined;
  logEntries: LogEntry[];
  status: string;
  createdDate: number;
  lastUpdate: number;
}

export interface MsgCreateTransportDocumentResponse {
  transportDocument: TransportDocument | undefined;
}

export interface MsgUpdateTransportDocument {
  creator: string;
  id: string;
  sender: Company | undefined;
  carrier: Carrier | undefined;
  freight: Freight | undefined;
  logEntries: LogEntry[];
  status: string;
  createdDate: number;
  lastUpdate: number;
}

export interface MsgUpdateTransportDocumentResponse {
  transportDocument: TransportDocument | undefined;
}

export interface MsgDeleteTransportDocument {
  creator: string;
  id: string;
}

export interface MsgDeleteTransportDocumentResponse {}

export interface MsgUploadDeviceData {
  creator: string;
  jobId: number;
  deviceId: number;
  protocolVersion: number;
  firmwareDescriptor: FirmwareDescriptor | undefined;
  comTimestamp: number;
  lastComCause: string;
  data: Data[];
}

export interface MsgUploadDeviceDataResponse {}

/** UpdateVisualInspectionCarrier */
export interface MsgUpdateVisualInspectionCarrier {
  creator: string;
  transportDocumentId: string;
  orders: MsgOrderVisualInspectionCarrierDto[];
}

export interface MsgUpdateVisualInspectionCarrierResponse {
  transportDocument: TransportDocument | undefined;
}

/** UpdateVisualInspectionConsignee */
export interface MsgUpdateVisualInspectionConsignee {
  creator: string;
  transportDocumentId: string;
  order: MsgOrderVisualInspectionConsigneeDto | undefined;
}

export interface MsgUpdateVisualInspectionConsigneeResponse {
  transportDocument: TransportDocument | undefined;
}

export interface MsgAddDeviceToOrderPosition {
  creator: string;
  transportDocumentId: string;
  orderId: string;
  orderPositionId: number;
  deviceId: number;
}

export interface MsgAddDeviceToOrderPositionResponse {
  transportDocument: TransportDocument | undefined;
}

export interface MsgRevertToGenesis {
  creator: string;
}

export interface MsgRevertToGenesisResponse {}

export interface MsgFinishIoTBrokerJob {
  creator: string;
  jobId: number;
  deviceId: number;
}

export interface MsgFinishIoTBrokerJobResponse {}

export interface MsgRemoveDeviceFromOrderPosition {
  creator: string;
  orderPositionId: number;
}

export interface MsgRemoveDeviceFromOrderPositionResponse {}

export interface MsgCreateDangerousGoodRegistration {
  creator: string;
  sender: Company | undefined;
  freight: Freight | undefined;
  createdDate: number;
  lastUpdate: number;
}

export interface MsgCreateDangerousGoodRegistrationResponse {
  dangerousGoodRegistration: DangerousGoodRegistration | undefined;
}

export interface MsgUpdateDangerousGoodRegistration {
  creator: string;
  id: string;
  sender: Company | undefined;
  freight: Freight | undefined;
  createdDate: number;
}

export interface MsgUpdateDangerousGoodRegistrationResponse {
  dangerousGoodRegistration: DangerousGoodRegistration | undefined;
}

export interface MsgDeleteDangerousGoodRegistration {
  creator: string;
  id: string;
}

export interface MsgDeleteDangerousGoodRegistrationResponse {}

export interface MsgAcceptDangerousGoodRegistration {
  creator: string;
  acceptDangerousGoodRegistrationDto: AcceptDangerousGoodRegistrationDto | undefined;
}

export interface MsgAcceptDangerousGoodRegistrationResponse {
  transportDocument: TransportDocument | undefined;
}

const baseMsgCreateTransportDocument: object = {
  creator: '',
  status: '',
  createdDate: 0,
  lastUpdate: 0,
};

export const MsgCreateTransportDocument = {
  encode(message: MsgCreateTransportDocument, writer: Writer = Writer.create()): Writer {
    if (message.creator !== '') {
      writer.uint32(10).string(message.creator);
    }
    if (message.sender !== undefined) {
      Company.encode(message.sender, writer.uint32(18).fork()).ldelim();
    }
    if (message.carrier !== undefined) {
      Carrier.encode(message.carrier, writer.uint32(26).fork()).ldelim();
    }
    if (message.freight !== undefined) {
      Freight.encode(message.freight, writer.uint32(34).fork()).ldelim();
    }
    for (const v of message.logEntries) {
      LogEntry.encode(v!, writer.uint32(42).fork()).ldelim();
    }
    if (message.status !== '') {
      writer.uint32(50).string(message.status);
    }
    if (message.createdDate !== 0) {
      writer.uint32(57).double(message.createdDate);
    }
    if (message.lastUpdate !== 0) {
      writer.uint32(65).double(message.lastUpdate);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgCreateTransportDocument {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgCreateTransportDocument,
    } as MsgCreateTransportDocument;
    message.logEntries = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.sender = Company.decode(reader, reader.uint32());
          break;
        case 3:
          message.carrier = Carrier.decode(reader, reader.uint32());
          break;
        case 4:
          message.freight = Freight.decode(reader, reader.uint32());
          break;
        case 5:
          message.logEntries.push(LogEntry.decode(reader, reader.uint32()));
          break;
        case 6:
          message.status = reader.string();
          break;
        case 7:
          message.createdDate = reader.double();
          break;
        case 8:
          message.lastUpdate = reader.double();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateTransportDocument {
    const message = {
      ...baseMsgCreateTransportDocument,
    } as MsgCreateTransportDocument;
    message.logEntries = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = '';
    }
    if (object.sender !== undefined && object.sender !== null) {
      message.sender = Company.fromJSON(object.sender);
    } else {
      message.sender = undefined;
    }
    if (object.carrier !== undefined && object.carrier !== null) {
      message.carrier = Carrier.fromJSON(object.carrier);
    } else {
      message.carrier = undefined;
    }
    if (object.freight !== undefined && object.freight !== null) {
      message.freight = Freight.fromJSON(object.freight);
    } else {
      message.freight = undefined;
    }
    if (object.logEntries !== undefined && object.logEntries !== null) {
      for (const e of object.logEntries) {
        message.logEntries.push(LogEntry.fromJSON(e));
      }
    }
    if (object.status !== undefined && object.status !== null) {
      message.status = String(object.status);
    } else {
      message.status = '';
    }
    if (object.createdDate !== undefined && object.createdDate !== null) {
      message.createdDate = Number(object.createdDate);
    } else {
      message.createdDate = 0;
    }
    if (object.lastUpdate !== undefined && object.lastUpdate !== null) {
      message.lastUpdate = Number(object.lastUpdate);
    } else {
      message.lastUpdate = 0;
    }
    return message;
  },

  toJSON(message: MsgCreateTransportDocument): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.sender !== undefined && (obj.sender = message.sender ? Company.toJSON(message.sender) : undefined);
    message.carrier !== undefined && (obj.carrier = message.carrier ? Carrier.toJSON(message.carrier) : undefined);
    message.freight !== undefined && (obj.freight = message.freight ? Freight.toJSON(message.freight) : undefined);
    if (message.logEntries) {
      obj.logEntries = message.logEntries.map((e) => (e ? LogEntry.toJSON(e) : undefined));
    } else {
      obj.logEntries = [];
    }
    message.status !== undefined && (obj.status = message.status);
    message.createdDate !== undefined && (obj.createdDate = message.createdDate);
    message.lastUpdate !== undefined && (obj.lastUpdate = message.lastUpdate);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgCreateTransportDocument>): MsgCreateTransportDocument {
    const message = {
      ...baseMsgCreateTransportDocument,
    } as MsgCreateTransportDocument;
    message.logEntries = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = '';
    }
    if (object.sender !== undefined && object.sender !== null) {
      message.sender = Company.fromPartial(object.sender);
    } else {
      message.sender = undefined;
    }
    if (object.carrier !== undefined && object.carrier !== null) {
      message.carrier = Carrier.fromPartial(object.carrier);
    } else {
      message.carrier = undefined;
    }
    if (object.freight !== undefined && object.freight !== null) {
      message.freight = Freight.fromPartial(object.freight);
    } else {
      message.freight = undefined;
    }
    if (object.logEntries !== undefined && object.logEntries !== null) {
      for (const e of object.logEntries) {
        message.logEntries.push(LogEntry.fromPartial(e));
      }
    }
    if (object.status !== undefined && object.status !== null) {
      message.status = object.status;
    } else {
      message.status = '';
    }
    if (object.createdDate !== undefined && object.createdDate !== null) {
      message.createdDate = object.createdDate;
    } else {
      message.createdDate = 0;
    }
    if (object.lastUpdate !== undefined && object.lastUpdate !== null) {
      message.lastUpdate = object.lastUpdate;
    } else {
      message.lastUpdate = 0;
    }
    return message;
  },
};

const baseMsgCreateTransportDocumentResponse: object = {};

export const MsgCreateTransportDocumentResponse = {
  encode(message: MsgCreateTransportDocumentResponse, writer: Writer = Writer.create()): Writer {
    if (message.transportDocument !== undefined) {
      TransportDocument.encode(message.transportDocument, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgCreateTransportDocumentResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgCreateTransportDocumentResponse,
    } as MsgCreateTransportDocumentResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.transportDocument = TransportDocument.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateTransportDocumentResponse {
    const message = {
      ...baseMsgCreateTransportDocumentResponse,
    } as MsgCreateTransportDocumentResponse;
    if (object.transportDocument !== undefined && object.transportDocument !== null) {
      message.transportDocument = TransportDocument.fromJSON(object.transportDocument);
    } else {
      message.transportDocument = undefined;
    }
    return message;
  },

  toJSON(message: MsgCreateTransportDocumentResponse): unknown {
    const obj: any = {};
    message.transportDocument !== undefined &&
      (obj.transportDocument = message.transportDocument
        ? TransportDocument.toJSON(message.transportDocument)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgCreateTransportDocumentResponse>): MsgCreateTransportDocumentResponse {
    const message = {
      ...baseMsgCreateTransportDocumentResponse,
    } as MsgCreateTransportDocumentResponse;
    if (object.transportDocument !== undefined && object.transportDocument !== null) {
      message.transportDocument = TransportDocument.fromPartial(object.transportDocument);
    } else {
      message.transportDocument = undefined;
    }
    return message;
  },
};

const baseMsgUpdateTransportDocument: object = {
  creator: '',
  id: '',
  status: '',
  createdDate: 0,
  lastUpdate: 0,
};

export const MsgUpdateTransportDocument = {
  encode(message: MsgUpdateTransportDocument, writer: Writer = Writer.create()): Writer {
    if (message.creator !== '') {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== '') {
      writer.uint32(18).string(message.id);
    }
    if (message.sender !== undefined) {
      Company.encode(message.sender, writer.uint32(26).fork()).ldelim();
    }
    if (message.carrier !== undefined) {
      Carrier.encode(message.carrier, writer.uint32(34).fork()).ldelim();
    }
    if (message.freight !== undefined) {
      Freight.encode(message.freight, writer.uint32(42).fork()).ldelim();
    }
    for (const v of message.logEntries) {
      LogEntry.encode(v!, writer.uint32(50).fork()).ldelim();
    }
    if (message.status !== '') {
      writer.uint32(58).string(message.status);
    }
    if (message.createdDate !== 0) {
      writer.uint32(65).double(message.createdDate);
    }
    if (message.lastUpdate !== 0) {
      writer.uint32(73).double(message.lastUpdate);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgUpdateTransportDocument {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgUpdateTransportDocument,
    } as MsgUpdateTransportDocument;
    message.logEntries = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.sender = Company.decode(reader, reader.uint32());
          break;
        case 4:
          message.carrier = Carrier.decode(reader, reader.uint32());
          break;
        case 5:
          message.freight = Freight.decode(reader, reader.uint32());
          break;
        case 6:
          message.logEntries.push(LogEntry.decode(reader, reader.uint32()));
          break;
        case 7:
          message.status = reader.string();
          break;
        case 8:
          message.createdDate = reader.double();
          break;
        case 9:
          message.lastUpdate = reader.double();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateTransportDocument {
    const message = {
      ...baseMsgUpdateTransportDocument,
    } as MsgUpdateTransportDocument;
    message.logEntries = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = '';
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = '';
    }
    if (object.sender !== undefined && object.sender !== null) {
      message.sender = Company.fromJSON(object.sender);
    } else {
      message.sender = undefined;
    }
    if (object.carrier !== undefined && object.carrier !== null) {
      message.carrier = Carrier.fromJSON(object.carrier);
    } else {
      message.carrier = undefined;
    }
    if (object.freight !== undefined && object.freight !== null) {
      message.freight = Freight.fromJSON(object.freight);
    } else {
      message.freight = undefined;
    }
    if (object.logEntries !== undefined && object.logEntries !== null) {
      for (const e of object.logEntries) {
        message.logEntries.push(LogEntry.fromJSON(e));
      }
    }
    if (object.status !== undefined && object.status !== null) {
      message.status = String(object.status);
    } else {
      message.status = '';
    }
    if (object.createdDate !== undefined && object.createdDate !== null) {
      message.createdDate = Number(object.createdDate);
    } else {
      message.createdDate = 0;
    }
    if (object.lastUpdate !== undefined && object.lastUpdate !== null) {
      message.lastUpdate = Number(object.lastUpdate);
    } else {
      message.lastUpdate = 0;
    }
    return message;
  },

  toJSON(message: MsgUpdateTransportDocument): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.sender !== undefined && (obj.sender = message.sender ? Company.toJSON(message.sender) : undefined);
    message.carrier !== undefined && (obj.carrier = message.carrier ? Carrier.toJSON(message.carrier) : undefined);
    message.freight !== undefined && (obj.freight = message.freight ? Freight.toJSON(message.freight) : undefined);
    if (message.logEntries) {
      obj.logEntries = message.logEntries.map((e) => (e ? LogEntry.toJSON(e) : undefined));
    } else {
      obj.logEntries = [];
    }
    message.status !== undefined && (obj.status = message.status);
    message.createdDate !== undefined && (obj.createdDate = message.createdDate);
    message.lastUpdate !== undefined && (obj.lastUpdate = message.lastUpdate);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgUpdateTransportDocument>): MsgUpdateTransportDocument {
    const message = {
      ...baseMsgUpdateTransportDocument,
    } as MsgUpdateTransportDocument;
    message.logEntries = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = '';
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = '';
    }
    if (object.sender !== undefined && object.sender !== null) {
      message.sender = Company.fromPartial(object.sender);
    } else {
      message.sender = undefined;
    }
    if (object.carrier !== undefined && object.carrier !== null) {
      message.carrier = Carrier.fromPartial(object.carrier);
    } else {
      message.carrier = undefined;
    }
    if (object.freight !== undefined && object.freight !== null) {
      message.freight = Freight.fromPartial(object.freight);
    } else {
      message.freight = undefined;
    }
    if (object.logEntries !== undefined && object.logEntries !== null) {
      for (const e of object.logEntries) {
        message.logEntries.push(LogEntry.fromPartial(e));
      }
    }
    if (object.status !== undefined && object.status !== null) {
      message.status = object.status;
    } else {
      message.status = '';
    }
    if (object.createdDate !== undefined && object.createdDate !== null) {
      message.createdDate = object.createdDate;
    } else {
      message.createdDate = 0;
    }
    if (object.lastUpdate !== undefined && object.lastUpdate !== null) {
      message.lastUpdate = object.lastUpdate;
    } else {
      message.lastUpdate = 0;
    }
    return message;
  },
};

const baseMsgUpdateTransportDocumentResponse: object = {};

export const MsgUpdateTransportDocumentResponse = {
  encode(message: MsgUpdateTransportDocumentResponse, writer: Writer = Writer.create()): Writer {
    if (message.transportDocument !== undefined) {
      TransportDocument.encode(message.transportDocument, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgUpdateTransportDocumentResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgUpdateTransportDocumentResponse,
    } as MsgUpdateTransportDocumentResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.transportDocument = TransportDocument.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateTransportDocumentResponse {
    const message = {
      ...baseMsgUpdateTransportDocumentResponse,
    } as MsgUpdateTransportDocumentResponse;
    if (object.transportDocument !== undefined && object.transportDocument !== null) {
      message.transportDocument = TransportDocument.fromJSON(object.transportDocument);
    } else {
      message.transportDocument = undefined;
    }
    return message;
  },

  toJSON(message: MsgUpdateTransportDocumentResponse): unknown {
    const obj: any = {};
    message.transportDocument !== undefined &&
      (obj.transportDocument = message.transportDocument
        ? TransportDocument.toJSON(message.transportDocument)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgUpdateTransportDocumentResponse>): MsgUpdateTransportDocumentResponse {
    const message = {
      ...baseMsgUpdateTransportDocumentResponse,
    } as MsgUpdateTransportDocumentResponse;
    if (object.transportDocument !== undefined && object.transportDocument !== null) {
      message.transportDocument = TransportDocument.fromPartial(object.transportDocument);
    } else {
      message.transportDocument = undefined;
    }
    return message;
  },
};

const baseMsgDeleteTransportDocument: object = { creator: '', id: '' };

export const MsgDeleteTransportDocument = {
  encode(message: MsgDeleteTransportDocument, writer: Writer = Writer.create()): Writer {
    if (message.creator !== '') {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== '') {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgDeleteTransportDocument {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgDeleteTransportDocument,
    } as MsgDeleteTransportDocument;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgDeleteTransportDocument {
    const message = {
      ...baseMsgDeleteTransportDocument,
    } as MsgDeleteTransportDocument;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = '';
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = '';
    }
    return message;
  },

  toJSON(message: MsgDeleteTransportDocument): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgDeleteTransportDocument>): MsgDeleteTransportDocument {
    const message = {
      ...baseMsgDeleteTransportDocument,
    } as MsgDeleteTransportDocument;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = '';
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = '';
    }
    return message;
  },
};

const baseMsgDeleteTransportDocumentResponse: object = {};

export const MsgDeleteTransportDocumentResponse = {
  encode(_: MsgDeleteTransportDocumentResponse, writer: Writer = Writer.create()): Writer {
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgDeleteTransportDocumentResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgDeleteTransportDocumentResponse,
    } as MsgDeleteTransportDocumentResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgDeleteTransportDocumentResponse {
    const message = {
      ...baseMsgDeleteTransportDocumentResponse,
    } as MsgDeleteTransportDocumentResponse;
    return message;
  },

  toJSON(_: MsgDeleteTransportDocumentResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(_: DeepPartial<MsgDeleteTransportDocumentResponse>): MsgDeleteTransportDocumentResponse {
    const message = {
      ...baseMsgDeleteTransportDocumentResponse,
    } as MsgDeleteTransportDocumentResponse;
    return message;
  },
};

const baseMsgUploadDeviceData: object = {
  creator: '',
  jobId: 0,
  deviceId: 0,
  protocolVersion: 0,
  comTimestamp: 0,
  lastComCause: '',
};

export const MsgUploadDeviceData = {
  encode(message: MsgUploadDeviceData, writer: Writer = Writer.create()): Writer {
    if (message.creator !== '') {
      writer.uint32(10).string(message.creator);
    }
    if (message.jobId !== 0) {
      writer.uint32(16).uint64(message.jobId);
    }
    if (message.deviceId !== 0) {
      writer.uint32(24).uint64(message.deviceId);
    }
    if (message.protocolVersion !== 0) {
      writer.uint32(32).uint32(message.protocolVersion);
    }
    if (message.firmwareDescriptor !== undefined) {
      FirmwareDescriptor.encode(message.firmwareDescriptor, writer.uint32(42).fork()).ldelim();
    }
    if (message.comTimestamp !== 0) {
      writer.uint32(48).uint64(message.comTimestamp);
    }
    if (message.lastComCause !== '') {
      writer.uint32(58).string(message.lastComCause);
    }
    for (const v of message.data) {
      Data.encode(v!, writer.uint32(66).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgUploadDeviceData {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgUploadDeviceData } as MsgUploadDeviceData;
    message.data = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.jobId = longToNumber(reader.uint64() as Long);
          break;
        case 3:
          message.deviceId = longToNumber(reader.uint64() as Long);
          break;
        case 4:
          message.protocolVersion = reader.uint32();
          break;
        case 5:
          message.firmwareDescriptor = FirmwareDescriptor.decode(reader, reader.uint32());
          break;
        case 6:
          message.comTimestamp = longToNumber(reader.uint64() as Long);
          break;
        case 7:
          message.lastComCause = reader.string();
          break;
        case 8:
          message.data.push(Data.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUploadDeviceData {
    const message = { ...baseMsgUploadDeviceData } as MsgUploadDeviceData;
    message.data = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = '';
    }
    if (object.jobId !== undefined && object.jobId !== null) {
      message.jobId = Number(object.jobId);
    } else {
      message.jobId = 0;
    }
    if (object.deviceId !== undefined && object.deviceId !== null) {
      message.deviceId = Number(object.deviceId);
    } else {
      message.deviceId = 0;
    }
    if (object.protocolVersion !== undefined && object.protocolVersion !== null) {
      message.protocolVersion = Number(object.protocolVersion);
    } else {
      message.protocolVersion = 0;
    }
    if (object.firmwareDescriptor !== undefined && object.firmwareDescriptor !== null) {
      message.firmwareDescriptor = FirmwareDescriptor.fromJSON(object.firmwareDescriptor);
    } else {
      message.firmwareDescriptor = undefined;
    }
    if (object.comTimestamp !== undefined && object.comTimestamp !== null) {
      message.comTimestamp = Number(object.comTimestamp);
    } else {
      message.comTimestamp = 0;
    }
    if (object.lastComCause !== undefined && object.lastComCause !== null) {
      message.lastComCause = String(object.lastComCause);
    } else {
      message.lastComCause = '';
    }
    if (object.data !== undefined && object.data !== null) {
      for (const e of object.data) {
        message.data.push(Data.fromJSON(e));
      }
    }
    return message;
  },

  toJSON(message: MsgUploadDeviceData): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.jobId !== undefined && (obj.jobId = message.jobId);
    message.deviceId !== undefined && (obj.deviceId = message.deviceId);
    message.protocolVersion !== undefined && (obj.protocolVersion = message.protocolVersion);
    message.firmwareDescriptor !== undefined &&
      (obj.firmwareDescriptor = message.firmwareDescriptor
        ? FirmwareDescriptor.toJSON(message.firmwareDescriptor)
        : undefined);
    message.comTimestamp !== undefined && (obj.comTimestamp = message.comTimestamp);
    message.lastComCause !== undefined && (obj.lastComCause = message.lastComCause);
    if (message.data) {
      obj.data = message.data.map((e) => (e ? Data.toJSON(e) : undefined));
    } else {
      obj.data = [];
    }
    return obj;
  },

  fromPartial(object: DeepPartial<MsgUploadDeviceData>): MsgUploadDeviceData {
    const message = { ...baseMsgUploadDeviceData } as MsgUploadDeviceData;
    message.data = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = '';
    }
    if (object.jobId !== undefined && object.jobId !== null) {
      message.jobId = object.jobId;
    } else {
      message.jobId = 0;
    }
    if (object.deviceId !== undefined && object.deviceId !== null) {
      message.deviceId = object.deviceId;
    } else {
      message.deviceId = 0;
    }
    if (object.protocolVersion !== undefined && object.protocolVersion !== null) {
      message.protocolVersion = object.protocolVersion;
    } else {
      message.protocolVersion = 0;
    }
    if (object.firmwareDescriptor !== undefined && object.firmwareDescriptor !== null) {
      message.firmwareDescriptor = FirmwareDescriptor.fromPartial(object.firmwareDescriptor);
    } else {
      message.firmwareDescriptor = undefined;
    }
    if (object.comTimestamp !== undefined && object.comTimestamp !== null) {
      message.comTimestamp = object.comTimestamp;
    } else {
      message.comTimestamp = 0;
    }
    if (object.lastComCause !== undefined && object.lastComCause !== null) {
      message.lastComCause = object.lastComCause;
    } else {
      message.lastComCause = '';
    }
    if (object.data !== undefined && object.data !== null) {
      for (const e of object.data) {
        message.data.push(Data.fromPartial(e));
      }
    }
    return message;
  },
};

const baseMsgUploadDeviceDataResponse: object = {};

export const MsgUploadDeviceDataResponse = {
  encode(_: MsgUploadDeviceDataResponse, writer: Writer = Writer.create()): Writer {
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgUploadDeviceDataResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgUploadDeviceDataResponse,
    } as MsgUploadDeviceDataResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgUploadDeviceDataResponse {
    const message = {
      ...baseMsgUploadDeviceDataResponse,
    } as MsgUploadDeviceDataResponse;
    return message;
  },

  toJSON(_: MsgUploadDeviceDataResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(_: DeepPartial<MsgUploadDeviceDataResponse>): MsgUploadDeviceDataResponse {
    const message = {
      ...baseMsgUploadDeviceDataResponse,
    } as MsgUploadDeviceDataResponse;
    return message;
  },
};

const baseMsgUpdateVisualInspectionCarrier: object = {
  creator: '',
  transportDocumentId: '',
};

export const MsgUpdateVisualInspectionCarrier = {
  encode(message: MsgUpdateVisualInspectionCarrier, writer: Writer = Writer.create()): Writer {
    if (message.creator !== '') {
      writer.uint32(10).string(message.creator);
    }
    if (message.transportDocumentId !== '') {
      writer.uint32(18).string(message.transportDocumentId);
    }
    for (const v of message.orders) {
      MsgOrderVisualInspectionCarrierDto.encode(v!, writer.uint32(26).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgUpdateVisualInspectionCarrier {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgUpdateVisualInspectionCarrier,
    } as MsgUpdateVisualInspectionCarrier;
    message.orders = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.transportDocumentId = reader.string();
          break;
        case 3:
          message.orders.push(MsgOrderVisualInspectionCarrierDto.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateVisualInspectionCarrier {
    const message = {
      ...baseMsgUpdateVisualInspectionCarrier,
    } as MsgUpdateVisualInspectionCarrier;
    message.orders = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = '';
    }
    if (object.transportDocumentId !== undefined && object.transportDocumentId !== null) {
      message.transportDocumentId = String(object.transportDocumentId);
    } else {
      message.transportDocumentId = '';
    }
    if (object.orders !== undefined && object.orders !== null) {
      for (const e of object.orders) {
        message.orders.push(MsgOrderVisualInspectionCarrierDto.fromJSON(e));
      }
    }
    return message;
  },

  toJSON(message: MsgUpdateVisualInspectionCarrier): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.transportDocumentId !== undefined && (obj.transportDocumentId = message.transportDocumentId);
    if (message.orders) {
      obj.orders = message.orders.map((e) => (e ? MsgOrderVisualInspectionCarrierDto.toJSON(e) : undefined));
    } else {
      obj.orders = [];
    }
    return obj;
  },

  fromPartial(object: DeepPartial<MsgUpdateVisualInspectionCarrier>): MsgUpdateVisualInspectionCarrier {
    const message = {
      ...baseMsgUpdateVisualInspectionCarrier,
    } as MsgUpdateVisualInspectionCarrier;
    message.orders = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = '';
    }
    if (object.transportDocumentId !== undefined && object.transportDocumentId !== null) {
      message.transportDocumentId = object.transportDocumentId;
    } else {
      message.transportDocumentId = '';
    }
    if (object.orders !== undefined && object.orders !== null) {
      for (const e of object.orders) {
        message.orders.push(MsgOrderVisualInspectionCarrierDto.fromPartial(e));
      }
    }
    return message;
  },
};

const baseMsgUpdateVisualInspectionCarrierResponse: object = {};

export const MsgUpdateVisualInspectionCarrierResponse = {
  encode(message: MsgUpdateVisualInspectionCarrierResponse, writer: Writer = Writer.create()): Writer {
    if (message.transportDocument !== undefined) {
      TransportDocument.encode(message.transportDocument, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgUpdateVisualInspectionCarrierResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgUpdateVisualInspectionCarrierResponse,
    } as MsgUpdateVisualInspectionCarrierResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.transportDocument = TransportDocument.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateVisualInspectionCarrierResponse {
    const message = {
      ...baseMsgUpdateVisualInspectionCarrierResponse,
    } as MsgUpdateVisualInspectionCarrierResponse;
    if (object.transportDocument !== undefined && object.transportDocument !== null) {
      message.transportDocument = TransportDocument.fromJSON(object.transportDocument);
    } else {
      message.transportDocument = undefined;
    }
    return message;
  },

  toJSON(message: MsgUpdateVisualInspectionCarrierResponse): unknown {
    const obj: any = {};
    message.transportDocument !== undefined &&
      (obj.transportDocument = message.transportDocument
        ? TransportDocument.toJSON(message.transportDocument)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgUpdateVisualInspectionCarrierResponse>): MsgUpdateVisualInspectionCarrierResponse {
    const message = {
      ...baseMsgUpdateVisualInspectionCarrierResponse,
    } as MsgUpdateVisualInspectionCarrierResponse;
    if (object.transportDocument !== undefined && object.transportDocument !== null) {
      message.transportDocument = TransportDocument.fromPartial(object.transportDocument);
    } else {
      message.transportDocument = undefined;
    }
    return message;
  },
};

const baseMsgUpdateVisualInspectionConsignee: object = {
  creator: '',
  transportDocumentId: '',
};

export const MsgUpdateVisualInspectionConsignee = {
  encode(message: MsgUpdateVisualInspectionConsignee, writer: Writer = Writer.create()): Writer {
    if (message.creator !== '') {
      writer.uint32(10).string(message.creator);
    }
    if (message.transportDocumentId !== '') {
      writer.uint32(18).string(message.transportDocumentId);
    }
    if (message.order !== undefined) {
      MsgOrderVisualInspectionConsigneeDto.encode(message.order, writer.uint32(26).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgUpdateVisualInspectionConsignee {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgUpdateVisualInspectionConsignee,
    } as MsgUpdateVisualInspectionConsignee;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.transportDocumentId = reader.string();
          break;
        case 3:
          message.order = MsgOrderVisualInspectionConsigneeDto.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateVisualInspectionConsignee {
    const message = {
      ...baseMsgUpdateVisualInspectionConsignee,
    } as MsgUpdateVisualInspectionConsignee;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = '';
    }
    if (object.transportDocumentId !== undefined && object.transportDocumentId !== null) {
      message.transportDocumentId = String(object.transportDocumentId);
    } else {
      message.transportDocumentId = '';
    }
    if (object.order !== undefined && object.order !== null) {
      message.order = MsgOrderVisualInspectionConsigneeDto.fromJSON(object.order);
    } else {
      message.order = undefined;
    }
    return message;
  },

  toJSON(message: MsgUpdateVisualInspectionConsignee): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.transportDocumentId !== undefined && (obj.transportDocumentId = message.transportDocumentId);
    message.order !== undefined &&
      (obj.order = message.order ? MsgOrderVisualInspectionConsigneeDto.toJSON(message.order) : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgUpdateVisualInspectionConsignee>): MsgUpdateVisualInspectionConsignee {
    const message = {
      ...baseMsgUpdateVisualInspectionConsignee,
    } as MsgUpdateVisualInspectionConsignee;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = '';
    }
    if (object.transportDocumentId !== undefined && object.transportDocumentId !== null) {
      message.transportDocumentId = object.transportDocumentId;
    } else {
      message.transportDocumentId = '';
    }
    if (object.order !== undefined && object.order !== null) {
      message.order = MsgOrderVisualInspectionConsigneeDto.fromPartial(object.order);
    } else {
      message.order = undefined;
    }
    return message;
  },
};

const baseMsgUpdateVisualInspectionConsigneeResponse: object = {};

export const MsgUpdateVisualInspectionConsigneeResponse = {
  encode(message: MsgUpdateVisualInspectionConsigneeResponse, writer: Writer = Writer.create()): Writer {
    if (message.transportDocument !== undefined) {
      TransportDocument.encode(message.transportDocument, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgUpdateVisualInspectionConsigneeResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgUpdateVisualInspectionConsigneeResponse,
    } as MsgUpdateVisualInspectionConsigneeResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.transportDocument = TransportDocument.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateVisualInspectionConsigneeResponse {
    const message = {
      ...baseMsgUpdateVisualInspectionConsigneeResponse,
    } as MsgUpdateVisualInspectionConsigneeResponse;
    if (object.transportDocument !== undefined && object.transportDocument !== null) {
      message.transportDocument = TransportDocument.fromJSON(object.transportDocument);
    } else {
      message.transportDocument = undefined;
    }
    return message;
  },

  toJSON(message: MsgUpdateVisualInspectionConsigneeResponse): unknown {
    const obj: any = {};
    message.transportDocument !== undefined &&
      (obj.transportDocument = message.transportDocument
        ? TransportDocument.toJSON(message.transportDocument)
        : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgUpdateVisualInspectionConsigneeResponse>
  ): MsgUpdateVisualInspectionConsigneeResponse {
    const message = {
      ...baseMsgUpdateVisualInspectionConsigneeResponse,
    } as MsgUpdateVisualInspectionConsigneeResponse;
    if (object.transportDocument !== undefined && object.transportDocument !== null) {
      message.transportDocument = TransportDocument.fromPartial(object.transportDocument);
    } else {
      message.transportDocument = undefined;
    }
    return message;
  },
};

const baseMsgAddDeviceToOrderPosition: object = {
  creator: '',
  transportDocumentId: '',
  orderId: '',
  orderPositionId: 0,
  deviceId: 0,
};

export const MsgAddDeviceToOrderPosition = {
  encode(message: MsgAddDeviceToOrderPosition, writer: Writer = Writer.create()): Writer {
    if (message.creator !== '') {
      writer.uint32(10).string(message.creator);
    }
    if (message.transportDocumentId !== '') {
      writer.uint32(18).string(message.transportDocumentId);
    }
    if (message.orderId !== '') {
      writer.uint32(26).string(message.orderId);
    }
    if (message.orderPositionId !== 0) {
      writer.uint32(32).uint64(message.orderPositionId);
    }
    if (message.deviceId !== 0) {
      writer.uint32(40).uint64(message.deviceId);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgAddDeviceToOrderPosition {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgAddDeviceToOrderPosition,
    } as MsgAddDeviceToOrderPosition;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.transportDocumentId = reader.string();
          break;
        case 3:
          message.orderId = reader.string();
          break;
        case 4:
          message.orderPositionId = longToNumber(reader.uint64() as Long);
          break;
        case 5:
          message.deviceId = longToNumber(reader.uint64() as Long);
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgAddDeviceToOrderPosition {
    const message = {
      ...baseMsgAddDeviceToOrderPosition,
    } as MsgAddDeviceToOrderPosition;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = '';
    }
    if (object.transportDocumentId !== undefined && object.transportDocumentId !== null) {
      message.transportDocumentId = String(object.transportDocumentId);
    } else {
      message.transportDocumentId = '';
    }
    if (object.orderId !== undefined && object.orderId !== null) {
      message.orderId = String(object.orderId);
    } else {
      message.orderId = '';
    }
    if (object.orderPositionId !== undefined && object.orderPositionId !== null) {
      message.orderPositionId = Number(object.orderPositionId);
    } else {
      message.orderPositionId = 0;
    }
    if (object.deviceId !== undefined && object.deviceId !== null) {
      message.deviceId = Number(object.deviceId);
    } else {
      message.deviceId = 0;
    }
    return message;
  },

  toJSON(message: MsgAddDeviceToOrderPosition): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.transportDocumentId !== undefined && (obj.transportDocumentId = message.transportDocumentId);
    message.orderId !== undefined && (obj.orderId = message.orderId);
    message.orderPositionId !== undefined && (obj.orderPositionId = message.orderPositionId);
    message.deviceId !== undefined && (obj.deviceId = message.deviceId);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgAddDeviceToOrderPosition>): MsgAddDeviceToOrderPosition {
    const message = {
      ...baseMsgAddDeviceToOrderPosition,
    } as MsgAddDeviceToOrderPosition;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = '';
    }
    if (object.transportDocumentId !== undefined && object.transportDocumentId !== null) {
      message.transportDocumentId = object.transportDocumentId;
    } else {
      message.transportDocumentId = '';
    }
    if (object.orderId !== undefined && object.orderId !== null) {
      message.orderId = object.orderId;
    } else {
      message.orderId = '';
    }
    if (object.orderPositionId !== undefined && object.orderPositionId !== null) {
      message.orderPositionId = object.orderPositionId;
    } else {
      message.orderPositionId = 0;
    }
    if (object.deviceId !== undefined && object.deviceId !== null) {
      message.deviceId = object.deviceId;
    } else {
      message.deviceId = 0;
    }
    return message;
  },
};

const baseMsgAddDeviceToOrderPositionResponse: object = {};

export const MsgAddDeviceToOrderPositionResponse = {
  encode(message: MsgAddDeviceToOrderPositionResponse, writer: Writer = Writer.create()): Writer {
    if (message.transportDocument !== undefined) {
      TransportDocument.encode(message.transportDocument, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgAddDeviceToOrderPositionResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgAddDeviceToOrderPositionResponse,
    } as MsgAddDeviceToOrderPositionResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.transportDocument = TransportDocument.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgAddDeviceToOrderPositionResponse {
    const message = {
      ...baseMsgAddDeviceToOrderPositionResponse,
    } as MsgAddDeviceToOrderPositionResponse;
    if (object.transportDocument !== undefined && object.transportDocument !== null) {
      message.transportDocument = TransportDocument.fromJSON(object.transportDocument);
    } else {
      message.transportDocument = undefined;
    }
    return message;
  },

  toJSON(message: MsgAddDeviceToOrderPositionResponse): unknown {
    const obj: any = {};
    message.transportDocument !== undefined &&
      (obj.transportDocument = message.transportDocument
        ? TransportDocument.toJSON(message.transportDocument)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgAddDeviceToOrderPositionResponse>): MsgAddDeviceToOrderPositionResponse {
    const message = {
      ...baseMsgAddDeviceToOrderPositionResponse,
    } as MsgAddDeviceToOrderPositionResponse;
    if (object.transportDocument !== undefined && object.transportDocument !== null) {
      message.transportDocument = TransportDocument.fromPartial(object.transportDocument);
    } else {
      message.transportDocument = undefined;
    }
    return message;
  },
};

const baseMsgRevertToGenesis: object = { creator: '' };

export const MsgRevertToGenesis = {
  encode(message: MsgRevertToGenesis, writer: Writer = Writer.create()): Writer {
    if (message.creator !== '') {
      writer.uint32(10).string(message.creator);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgRevertToGenesis {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgRevertToGenesis } as MsgRevertToGenesis;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgRevertToGenesis {
    const message = { ...baseMsgRevertToGenesis } as MsgRevertToGenesis;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = '';
    }
    return message;
  },

  toJSON(message: MsgRevertToGenesis): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgRevertToGenesis>): MsgRevertToGenesis {
    const message = { ...baseMsgRevertToGenesis } as MsgRevertToGenesis;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = '';
    }
    return message;
  },
};

const baseMsgRevertToGenesisResponse: object = {};

export const MsgRevertToGenesisResponse = {
  encode(_: MsgRevertToGenesisResponse, writer: Writer = Writer.create()): Writer {
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgRevertToGenesisResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgRevertToGenesisResponse,
    } as MsgRevertToGenesisResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgRevertToGenesisResponse {
    const message = {
      ...baseMsgRevertToGenesisResponse,
    } as MsgRevertToGenesisResponse;
    return message;
  },

  toJSON(_: MsgRevertToGenesisResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(_: DeepPartial<MsgRevertToGenesisResponse>): MsgRevertToGenesisResponse {
    const message = {
      ...baseMsgRevertToGenesisResponse,
    } as MsgRevertToGenesisResponse;
    return message;
  },
};

const baseMsgFinishIoTBrokerJob: object = {
  creator: '',
  jobId: 0,
  deviceId: 0,
};

export const MsgFinishIoTBrokerJob = {
  encode(message: MsgFinishIoTBrokerJob, writer: Writer = Writer.create()): Writer {
    if (message.creator !== '') {
      writer.uint32(10).string(message.creator);
    }
    if (message.jobId !== 0) {
      writer.uint32(16).uint64(message.jobId);
    }
    if (message.deviceId !== 0) {
      writer.uint32(24).uint64(message.deviceId);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgFinishIoTBrokerJob {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgFinishIoTBrokerJob } as MsgFinishIoTBrokerJob;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.jobId = longToNumber(reader.uint64() as Long);
          break;
        case 3:
          message.deviceId = longToNumber(reader.uint64() as Long);
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFinishIoTBrokerJob {
    const message = { ...baseMsgFinishIoTBrokerJob } as MsgFinishIoTBrokerJob;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = '';
    }
    if (object.jobId !== undefined && object.jobId !== null) {
      message.jobId = Number(object.jobId);
    } else {
      message.jobId = 0;
    }
    if (object.deviceId !== undefined && object.deviceId !== null) {
      message.deviceId = Number(object.deviceId);
    } else {
      message.deviceId = 0;
    }
    return message;
  },

  toJSON(message: MsgFinishIoTBrokerJob): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.jobId !== undefined && (obj.jobId = message.jobId);
    message.deviceId !== undefined && (obj.deviceId = message.deviceId);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgFinishIoTBrokerJob>): MsgFinishIoTBrokerJob {
    const message = { ...baseMsgFinishIoTBrokerJob } as MsgFinishIoTBrokerJob;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = '';
    }
    if (object.jobId !== undefined && object.jobId !== null) {
      message.jobId = object.jobId;
    } else {
      message.jobId = 0;
    }
    if (object.deviceId !== undefined && object.deviceId !== null) {
      message.deviceId = object.deviceId;
    } else {
      message.deviceId = 0;
    }
    return message;
  },
};

const baseMsgFinishIoTBrokerJobResponse: object = {};

export const MsgFinishIoTBrokerJobResponse = {
  encode(_: MsgFinishIoTBrokerJobResponse, writer: Writer = Writer.create()): Writer {
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgFinishIoTBrokerJobResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgFinishIoTBrokerJobResponse,
    } as MsgFinishIoTBrokerJobResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgFinishIoTBrokerJobResponse {
    const message = {
      ...baseMsgFinishIoTBrokerJobResponse,
    } as MsgFinishIoTBrokerJobResponse;
    return message;
  },

  toJSON(_: MsgFinishIoTBrokerJobResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(_: DeepPartial<MsgFinishIoTBrokerJobResponse>): MsgFinishIoTBrokerJobResponse {
    const message = {
      ...baseMsgFinishIoTBrokerJobResponse,
    } as MsgFinishIoTBrokerJobResponse;
    return message;
  },
};

const baseMsgRemoveDeviceFromOrderPosition: object = {
  creator: '',
  orderPositionId: 0,
};

export const MsgRemoveDeviceFromOrderPosition = {
  encode(message: MsgRemoveDeviceFromOrderPosition, writer: Writer = Writer.create()): Writer {
    if (message.creator !== '') {
      writer.uint32(10).string(message.creator);
    }
    if (message.orderPositionId !== 0) {
      writer.uint32(16).uint64(message.orderPositionId);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgRemoveDeviceFromOrderPosition {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgRemoveDeviceFromOrderPosition,
    } as MsgRemoveDeviceFromOrderPosition;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.orderPositionId = longToNumber(reader.uint64() as Long);
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgRemoveDeviceFromOrderPosition {
    const message = {
      ...baseMsgRemoveDeviceFromOrderPosition,
    } as MsgRemoveDeviceFromOrderPosition;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = '';
    }
    if (object.orderPositionId !== undefined && object.orderPositionId !== null) {
      message.orderPositionId = Number(object.orderPositionId);
    } else {
      message.orderPositionId = 0;
    }
    return message;
  },

  toJSON(message: MsgRemoveDeviceFromOrderPosition): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.orderPositionId !== undefined && (obj.orderPositionId = message.orderPositionId);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgRemoveDeviceFromOrderPosition>): MsgRemoveDeviceFromOrderPosition {
    const message = {
      ...baseMsgRemoveDeviceFromOrderPosition,
    } as MsgRemoveDeviceFromOrderPosition;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = '';
    }
    if (object.orderPositionId !== undefined && object.orderPositionId !== null) {
      message.orderPositionId = object.orderPositionId;
    } else {
      message.orderPositionId = 0;
    }
    return message;
  },
};

const baseMsgRemoveDeviceFromOrderPositionResponse: object = {};

export const MsgRemoveDeviceFromOrderPositionResponse = {
  encode(_: MsgRemoveDeviceFromOrderPositionResponse, writer: Writer = Writer.create()): Writer {
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgRemoveDeviceFromOrderPositionResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgRemoveDeviceFromOrderPositionResponse,
    } as MsgRemoveDeviceFromOrderPositionResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgRemoveDeviceFromOrderPositionResponse {
    const message = {
      ...baseMsgRemoveDeviceFromOrderPositionResponse,
    } as MsgRemoveDeviceFromOrderPositionResponse;
    return message;
  },

  toJSON(_: MsgRemoveDeviceFromOrderPositionResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(_: DeepPartial<MsgRemoveDeviceFromOrderPositionResponse>): MsgRemoveDeviceFromOrderPositionResponse {
    const message = {
      ...baseMsgRemoveDeviceFromOrderPositionResponse,
    } as MsgRemoveDeviceFromOrderPositionResponse;
    return message;
  },
};

const baseMsgCreateDangerousGoodRegistration: object = {
  creator: '',
  createdDate: 0,
  lastUpdate: 0,
};

export const MsgCreateDangerousGoodRegistration = {
  encode(message: MsgCreateDangerousGoodRegistration, writer: Writer = Writer.create()): Writer {
    if (message.creator !== '') {
      writer.uint32(10).string(message.creator);
    }
    if (message.sender !== undefined) {
      Company.encode(message.sender, writer.uint32(18).fork()).ldelim();
    }
    if (message.freight !== undefined) {
      Freight.encode(message.freight, writer.uint32(26).fork()).ldelim();
    }
    if (message.createdDate !== 0) {
      writer.uint32(33).double(message.createdDate);
    }
    if (message.lastUpdate !== 0) {
      writer.uint32(41).double(message.lastUpdate);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgCreateDangerousGoodRegistration {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgCreateDangerousGoodRegistration,
    } as MsgCreateDangerousGoodRegistration;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.sender = Company.decode(reader, reader.uint32());
          break;
        case 3:
          message.freight = Freight.decode(reader, reader.uint32());
          break;
        case 4:
          message.createdDate = reader.double();
          break;
        case 5:
          message.lastUpdate = reader.double();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateDangerousGoodRegistration {
    const message = {
      ...baseMsgCreateDangerousGoodRegistration,
    } as MsgCreateDangerousGoodRegistration;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = '';
    }
    if (object.sender !== undefined && object.sender !== null) {
      message.sender = Company.fromJSON(object.sender);
    } else {
      message.sender = undefined;
    }
    if (object.freight !== undefined && object.freight !== null) {
      message.freight = Freight.fromJSON(object.freight);
    } else {
      message.freight = undefined;
    }
    if (object.createdDate !== undefined && object.createdDate !== null) {
      message.createdDate = Number(object.createdDate);
    } else {
      message.createdDate = 0;
    }
    if (object.lastUpdate !== undefined && object.lastUpdate !== null) {
      message.lastUpdate = Number(object.lastUpdate);
    } else {
      message.lastUpdate = 0;
    }
    return message;
  },

  toJSON(message: MsgCreateDangerousGoodRegistration): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.sender !== undefined && (obj.sender = message.sender ? Company.toJSON(message.sender) : undefined);
    message.freight !== undefined && (obj.freight = message.freight ? Freight.toJSON(message.freight) : undefined);
    message.createdDate !== undefined && (obj.createdDate = message.createdDate);
    message.lastUpdate !== undefined && (obj.lastUpdate = message.lastUpdate);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgCreateDangerousGoodRegistration>): MsgCreateDangerousGoodRegistration {
    const message = {
      ...baseMsgCreateDangerousGoodRegistration,
    } as MsgCreateDangerousGoodRegistration;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = '';
    }
    if (object.sender !== undefined && object.sender !== null) {
      message.sender = Company.fromPartial(object.sender);
    } else {
      message.sender = undefined;
    }
    if (object.freight !== undefined && object.freight !== null) {
      message.freight = Freight.fromPartial(object.freight);
    } else {
      message.freight = undefined;
    }
    if (object.createdDate !== undefined && object.createdDate !== null) {
      message.createdDate = object.createdDate;
    } else {
      message.createdDate = 0;
    }
    if (object.lastUpdate !== undefined && object.lastUpdate !== null) {
      message.lastUpdate = object.lastUpdate;
    } else {
      message.lastUpdate = 0;
    }
    return message;
  },
};

const baseMsgCreateDangerousGoodRegistrationResponse: object = {};

export const MsgCreateDangerousGoodRegistrationResponse = {
  encode(message: MsgCreateDangerousGoodRegistrationResponse, writer: Writer = Writer.create()): Writer {
    if (message.dangerousGoodRegistration !== undefined) {
      DangerousGoodRegistration.encode(message.dangerousGoodRegistration, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgCreateDangerousGoodRegistrationResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgCreateDangerousGoodRegistrationResponse,
    } as MsgCreateDangerousGoodRegistrationResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.dangerousGoodRegistration = DangerousGoodRegistration.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateDangerousGoodRegistrationResponse {
    const message = {
      ...baseMsgCreateDangerousGoodRegistrationResponse,
    } as MsgCreateDangerousGoodRegistrationResponse;
    if (object.dangerousGoodRegistration !== undefined && object.dangerousGoodRegistration !== null) {
      message.dangerousGoodRegistration = DangerousGoodRegistration.fromJSON(object.dangerousGoodRegistration);
    } else {
      message.dangerousGoodRegistration = undefined;
    }
    return message;
  },

  toJSON(message: MsgCreateDangerousGoodRegistrationResponse): unknown {
    const obj: any = {};
    message.dangerousGoodRegistration !== undefined &&
      (obj.dangerousGoodRegistration = message.dangerousGoodRegistration
        ? DangerousGoodRegistration.toJSON(message.dangerousGoodRegistration)
        : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgCreateDangerousGoodRegistrationResponse>
  ): MsgCreateDangerousGoodRegistrationResponse {
    const message = {
      ...baseMsgCreateDangerousGoodRegistrationResponse,
    } as MsgCreateDangerousGoodRegistrationResponse;
    if (object.dangerousGoodRegistration !== undefined && object.dangerousGoodRegistration !== null) {
      message.dangerousGoodRegistration = DangerousGoodRegistration.fromPartial(object.dangerousGoodRegistration);
    } else {
      message.dangerousGoodRegistration = undefined;
    }
    return message;
  },
};

const baseMsgUpdateDangerousGoodRegistration: object = {
  creator: '',
  id: '',
  createdDate: 0,
};

export const MsgUpdateDangerousGoodRegistration = {
  encode(message: MsgUpdateDangerousGoodRegistration, writer: Writer = Writer.create()): Writer {
    if (message.creator !== '') {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== '') {
      writer.uint32(18).string(message.id);
    }
    if (message.sender !== undefined) {
      Company.encode(message.sender, writer.uint32(26).fork()).ldelim();
    }
    if (message.freight !== undefined) {
      Freight.encode(message.freight, writer.uint32(34).fork()).ldelim();
    }
    if (message.createdDate !== 0) {
      writer.uint32(41).double(message.createdDate);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgUpdateDangerousGoodRegistration {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgUpdateDangerousGoodRegistration,
    } as MsgUpdateDangerousGoodRegistration;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.sender = Company.decode(reader, reader.uint32());
          break;
        case 4:
          message.freight = Freight.decode(reader, reader.uint32());
          break;
        case 5:
          message.createdDate = reader.double();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateDangerousGoodRegistration {
    const message = {
      ...baseMsgUpdateDangerousGoodRegistration,
    } as MsgUpdateDangerousGoodRegistration;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = '';
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = '';
    }
    if (object.sender !== undefined && object.sender !== null) {
      message.sender = Company.fromJSON(object.sender);
    } else {
      message.sender = undefined;
    }
    if (object.freight !== undefined && object.freight !== null) {
      message.freight = Freight.fromJSON(object.freight);
    } else {
      message.freight = undefined;
    }
    if (object.createdDate !== undefined && object.createdDate !== null) {
      message.createdDate = Number(object.createdDate);
    } else {
      message.createdDate = 0;
    }
    return message;
  },

  toJSON(message: MsgUpdateDangerousGoodRegistration): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.sender !== undefined && (obj.sender = message.sender ? Company.toJSON(message.sender) : undefined);
    message.freight !== undefined && (obj.freight = message.freight ? Freight.toJSON(message.freight) : undefined);
    message.createdDate !== undefined && (obj.createdDate = message.createdDate);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgUpdateDangerousGoodRegistration>): MsgUpdateDangerousGoodRegistration {
    const message = {
      ...baseMsgUpdateDangerousGoodRegistration,
    } as MsgUpdateDangerousGoodRegistration;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = '';
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = '';
    }
    if (object.sender !== undefined && object.sender !== null) {
      message.sender = Company.fromPartial(object.sender);
    } else {
      message.sender = undefined;
    }
    if (object.freight !== undefined && object.freight !== null) {
      message.freight = Freight.fromPartial(object.freight);
    } else {
      message.freight = undefined;
    }
    if (object.createdDate !== undefined && object.createdDate !== null) {
      message.createdDate = object.createdDate;
    } else {
      message.createdDate = 0;
    }
    return message;
  },
};

const baseMsgUpdateDangerousGoodRegistrationResponse: object = {};

export const MsgUpdateDangerousGoodRegistrationResponse = {
  encode(message: MsgUpdateDangerousGoodRegistrationResponse, writer: Writer = Writer.create()): Writer {
    if (message.dangerousGoodRegistration !== undefined) {
      DangerousGoodRegistration.encode(message.dangerousGoodRegistration, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgUpdateDangerousGoodRegistrationResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgUpdateDangerousGoodRegistrationResponse,
    } as MsgUpdateDangerousGoodRegistrationResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.dangerousGoodRegistration = DangerousGoodRegistration.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateDangerousGoodRegistrationResponse {
    const message = {
      ...baseMsgUpdateDangerousGoodRegistrationResponse,
    } as MsgUpdateDangerousGoodRegistrationResponse;
    if (object.dangerousGoodRegistration !== undefined && object.dangerousGoodRegistration !== null) {
      message.dangerousGoodRegistration = DangerousGoodRegistration.fromJSON(object.dangerousGoodRegistration);
    } else {
      message.dangerousGoodRegistration = undefined;
    }
    return message;
  },

  toJSON(message: MsgUpdateDangerousGoodRegistrationResponse): unknown {
    const obj: any = {};
    message.dangerousGoodRegistration !== undefined &&
      (obj.dangerousGoodRegistration = message.dangerousGoodRegistration
        ? DangerousGoodRegistration.toJSON(message.dangerousGoodRegistration)
        : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgUpdateDangerousGoodRegistrationResponse>
  ): MsgUpdateDangerousGoodRegistrationResponse {
    const message = {
      ...baseMsgUpdateDangerousGoodRegistrationResponse,
    } as MsgUpdateDangerousGoodRegistrationResponse;
    if (object.dangerousGoodRegistration !== undefined && object.dangerousGoodRegistration !== null) {
      message.dangerousGoodRegistration = DangerousGoodRegistration.fromPartial(object.dangerousGoodRegistration);
    } else {
      message.dangerousGoodRegistration = undefined;
    }
    return message;
  },
};

const baseMsgDeleteDangerousGoodRegistration: object = { creator: '', id: '' };

export const MsgDeleteDangerousGoodRegistration = {
  encode(message: MsgDeleteDangerousGoodRegistration, writer: Writer = Writer.create()): Writer {
    if (message.creator !== '') {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== '') {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgDeleteDangerousGoodRegistration {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgDeleteDangerousGoodRegistration,
    } as MsgDeleteDangerousGoodRegistration;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgDeleteDangerousGoodRegistration {
    const message = {
      ...baseMsgDeleteDangerousGoodRegistration,
    } as MsgDeleteDangerousGoodRegistration;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = '';
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = '';
    }
    return message;
  },

  toJSON(message: MsgDeleteDangerousGoodRegistration): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgDeleteDangerousGoodRegistration>): MsgDeleteDangerousGoodRegistration {
    const message = {
      ...baseMsgDeleteDangerousGoodRegistration,
    } as MsgDeleteDangerousGoodRegistration;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = '';
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = '';
    }
    return message;
  },
};

const baseMsgDeleteDangerousGoodRegistrationResponse: object = {};

export const MsgDeleteDangerousGoodRegistrationResponse = {
  encode(_: MsgDeleteDangerousGoodRegistrationResponse, writer: Writer = Writer.create()): Writer {
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgDeleteDangerousGoodRegistrationResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgDeleteDangerousGoodRegistrationResponse,
    } as MsgDeleteDangerousGoodRegistrationResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgDeleteDangerousGoodRegistrationResponse {
    const message = {
      ...baseMsgDeleteDangerousGoodRegistrationResponse,
    } as MsgDeleteDangerousGoodRegistrationResponse;
    return message;
  },

  toJSON(_: MsgDeleteDangerousGoodRegistrationResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(_: DeepPartial<MsgDeleteDangerousGoodRegistrationResponse>): MsgDeleteDangerousGoodRegistrationResponse {
    const message = {
      ...baseMsgDeleteDangerousGoodRegistrationResponse,
    } as MsgDeleteDangerousGoodRegistrationResponse;
    return message;
  },
};

const baseMsgAcceptDangerousGoodRegistration: object = { creator: '' };

export const MsgAcceptDangerousGoodRegistration = {
  encode(message: MsgAcceptDangerousGoodRegistration, writer: Writer = Writer.create()): Writer {
    if (message.creator !== '') {
      writer.uint32(10).string(message.creator);
    }
    if (message.acceptDangerousGoodRegistrationDto !== undefined) {
      AcceptDangerousGoodRegistrationDto.encode(
        message.acceptDangerousGoodRegistrationDto,
        writer.uint32(18).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgAcceptDangerousGoodRegistration {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgAcceptDangerousGoodRegistration,
    } as MsgAcceptDangerousGoodRegistration;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.acceptDangerousGoodRegistrationDto = AcceptDangerousGoodRegistrationDto.decode(
            reader,
            reader.uint32()
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgAcceptDangerousGoodRegistration {
    const message = {
      ...baseMsgAcceptDangerousGoodRegistration,
    } as MsgAcceptDangerousGoodRegistration;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = '';
    }
    if (object.acceptDangerousGoodRegistrationDto !== undefined && object.acceptDangerousGoodRegistrationDto !== null) {
      message.acceptDangerousGoodRegistrationDto = AcceptDangerousGoodRegistrationDto.fromJSON(
        object.acceptDangerousGoodRegistrationDto
      );
    } else {
      message.acceptDangerousGoodRegistrationDto = undefined;
    }
    return message;
  },

  toJSON(message: MsgAcceptDangerousGoodRegistration): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.acceptDangerousGoodRegistrationDto !== undefined &&
      (obj.acceptDangerousGoodRegistrationDto = message.acceptDangerousGoodRegistrationDto
        ? AcceptDangerousGoodRegistrationDto.toJSON(message.acceptDangerousGoodRegistrationDto)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgAcceptDangerousGoodRegistration>): MsgAcceptDangerousGoodRegistration {
    const message = {
      ...baseMsgAcceptDangerousGoodRegistration,
    } as MsgAcceptDangerousGoodRegistration;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = '';
    }
    if (object.acceptDangerousGoodRegistrationDto !== undefined && object.acceptDangerousGoodRegistrationDto !== null) {
      message.acceptDangerousGoodRegistrationDto = AcceptDangerousGoodRegistrationDto.fromPartial(
        object.acceptDangerousGoodRegistrationDto
      );
    } else {
      message.acceptDangerousGoodRegistrationDto = undefined;
    }
    return message;
  },
};

const baseMsgAcceptDangerousGoodRegistrationResponse: object = {};

export const MsgAcceptDangerousGoodRegistrationResponse = {
  encode(message: MsgAcceptDangerousGoodRegistrationResponse, writer: Writer = Writer.create()): Writer {
    if (message.transportDocument !== undefined) {
      TransportDocument.encode(message.transportDocument, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgAcceptDangerousGoodRegistrationResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgAcceptDangerousGoodRegistrationResponse,
    } as MsgAcceptDangerousGoodRegistrationResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.transportDocument = TransportDocument.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgAcceptDangerousGoodRegistrationResponse {
    const message = {
      ...baseMsgAcceptDangerousGoodRegistrationResponse,
    } as MsgAcceptDangerousGoodRegistrationResponse;
    if (object.transportDocument !== undefined && object.transportDocument !== null) {
      message.transportDocument = TransportDocument.fromJSON(object.transportDocument);
    } else {
      message.transportDocument = undefined;
    }
    return message;
  },

  toJSON(message: MsgAcceptDangerousGoodRegistrationResponse): unknown {
    const obj: any = {};
    message.transportDocument !== undefined &&
      (obj.transportDocument = message.transportDocument
        ? TransportDocument.toJSON(message.transportDocument)
        : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgAcceptDangerousGoodRegistrationResponse>
  ): MsgAcceptDangerousGoodRegistrationResponse {
    const message = {
      ...baseMsgAcceptDangerousGoodRegistrationResponse,
    } as MsgAcceptDangerousGoodRegistrationResponse;
    if (object.transportDocument !== undefined && object.transportDocument !== null) {
      message.transportDocument = TransportDocument.fromPartial(object.transportDocument);
    } else {
      message.transportDocument = undefined;
    }
    return message;
  },
};

/** Msg defines the Msg service. */
export interface Msg {
  CreateTransportDocument(request: MsgCreateTransportDocument): Promise<MsgCreateTransportDocumentResponse>;
  UpdateTransportDocument(request: MsgUpdateTransportDocument): Promise<MsgUpdateTransportDocumentResponse>;
  DeleteTransportDocument(request: MsgDeleteTransportDocument): Promise<MsgDeleteTransportDocumentResponse>;
  AddDeviceToOrderPosition(request: MsgAddDeviceToOrderPosition): Promise<MsgAddDeviceToOrderPositionResponse>;
  UpdateVisualInspectionCarrier(
    request: MsgUpdateVisualInspectionCarrier
  ): Promise<MsgUpdateVisualInspectionCarrierResponse>;
  UpdateVisualInspectionConsignee(
    request: MsgUpdateVisualInspectionConsignee
  ): Promise<MsgUpdateVisualInspectionConsigneeResponse>;
  UploadDeviceData(request: MsgUploadDeviceData): Promise<MsgUploadDeviceDataResponse>;
  RevertToGenesis(request: MsgRevertToGenesis): Promise<MsgRevertToGenesisResponse>;
  FinishIoTBrokerJob(request: MsgFinishIoTBrokerJob): Promise<MsgFinishIoTBrokerJobResponse>;
  RemoveDeviceFromOrderPosition(
    request: MsgRemoveDeviceFromOrderPosition
  ): Promise<MsgRemoveDeviceFromOrderPositionResponse>;
  CreateDangerousGoodRegistration(
    request: MsgCreateDangerousGoodRegistration
  ): Promise<MsgCreateDangerousGoodRegistrationResponse>;
  UpdateDangerousGoodRegistration(
    request: MsgUpdateDangerousGoodRegistration
  ): Promise<MsgUpdateDangerousGoodRegistrationResponse>;
  DeleteDangerousGoodRegistration(
    request: MsgDeleteDangerousGoodRegistration
  ): Promise<MsgDeleteDangerousGoodRegistrationResponse>;
  /** this line is used by starport scaffolding # proto/tx/rpc */
  AcceptDangerousGoodRegistration(
    request: MsgAcceptDangerousGoodRegistration
  ): Promise<MsgAcceptDangerousGoodRegistrationResponse>;
}

export class MsgClientImpl implements Msg {
  private readonly rpc: Rpc;
  constructor(rpc: Rpc) {
    this.rpc = rpc;
  }
  CreateTransportDocument(request: MsgCreateTransportDocument): Promise<MsgCreateTransportDocumentResponse> {
    const data = MsgCreateTransportDocument.encode(request).finish();
    const promise = this.rpc.request(
      'git.openlogisticsfoundation.org.silicon_economy.services.dangerous.core.apps.generated.businesslogic.Msg',
      'CreateTransportDocument',
      data
    );
    return promise.then((data) => MsgCreateTransportDocumentResponse.decode(new Reader(data)));
  }

  UpdateTransportDocument(request: MsgUpdateTransportDocument): Promise<MsgUpdateTransportDocumentResponse> {
    const data = MsgUpdateTransportDocument.encode(request).finish();
    const promise = this.rpc.request(
      'git.openlogisticsfoundation.org.silicon_economy.services.dangerous.core.apps.generated.businesslogic.Msg',
      'UpdateTransportDocument',
      data
    );
    return promise.then((data) => MsgUpdateTransportDocumentResponse.decode(new Reader(data)));
  }

  DeleteTransportDocument(request: MsgDeleteTransportDocument): Promise<MsgDeleteTransportDocumentResponse> {
    const data = MsgDeleteTransportDocument.encode(request).finish();
    const promise = this.rpc.request(
      'git.openlogisticsfoundation.org.silicon_economy.services.dangerous.core.apps.generated.businesslogic.Msg',
      'DeleteTransportDocument',
      data
    );
    return promise.then((data) => MsgDeleteTransportDocumentResponse.decode(new Reader(data)));
  }

  AddDeviceToOrderPosition(request: MsgAddDeviceToOrderPosition): Promise<MsgAddDeviceToOrderPositionResponse> {
    const data = MsgAddDeviceToOrderPosition.encode(request).finish();
    const promise = this.rpc.request(
      'git.openlogisticsfoundation.org.silicon_economy.services.dangerous.core.apps.generated.businesslogic.Msg',
      'AddDeviceToOrderPosition',
      data
    );
    return promise.then((data) => MsgAddDeviceToOrderPositionResponse.decode(new Reader(data)));
  }

  UpdateVisualInspectionCarrier(
    request: MsgUpdateVisualInspectionCarrier
  ): Promise<MsgUpdateVisualInspectionCarrierResponse> {
    const data = MsgUpdateVisualInspectionCarrier.encode(request).finish();
    const promise = this.rpc.request(
      'git.openlogisticsfoundation.org.silicon_economy.services.dangerous.core.apps.generated.businesslogic.Msg',
      'UpdateVisualInspectionCarrier',
      data
    );
    return promise.then((data) => MsgUpdateVisualInspectionCarrierResponse.decode(new Reader(data)));
  }

  UpdateVisualInspectionConsignee(
    request: MsgUpdateVisualInspectionConsignee
  ): Promise<MsgUpdateVisualInspectionConsigneeResponse> {
    const data = MsgUpdateVisualInspectionConsignee.encode(request).finish();
    const promise = this.rpc.request(
      'git.openlogisticsfoundation.org.silicon_economy.services.dangerous.core.apps.generated.businesslogic.Msg',
      'UpdateVisualInspectionConsignee',
      data
    );
    return promise.then((data) => MsgUpdateVisualInspectionConsigneeResponse.decode(new Reader(data)));
  }

  UploadDeviceData(request: MsgUploadDeviceData): Promise<MsgUploadDeviceDataResponse> {
    const data = MsgUploadDeviceData.encode(request).finish();
    const promise = this.rpc.request('git.openlogisticsfoundation.org.silicon_economy.services.dangerous.core.apps.generated.businesslogic.Msg', 'UploadDeviceData', data);
    return promise.then((data) => MsgUploadDeviceDataResponse.decode(new Reader(data)));
  }

  RevertToGenesis(request: MsgRevertToGenesis): Promise<MsgRevertToGenesisResponse> {
    const data = MsgRevertToGenesis.encode(request).finish();
    const promise = this.rpc.request('git.openlogisticsfoundation.org.silicon_economy.services.dangerous.core.apps.generated.businesslogic.Msg', 'RevertToGenesis', data);
    return promise.then((data) => MsgRevertToGenesisResponse.decode(new Reader(data)));
  }

  FinishIoTBrokerJob(request: MsgFinishIoTBrokerJob): Promise<MsgFinishIoTBrokerJobResponse> {
    const data = MsgFinishIoTBrokerJob.encode(request).finish();
    const promise = this.rpc.request('git.openlogisticsfoundation.org.silicon_economy.services.dangerous.core.apps.generated.businesslogic.Msg', 'FinishIoTBrokerJob', data);
    return promise.then((data) => MsgFinishIoTBrokerJobResponse.decode(new Reader(data)));
  }

  RemoveDeviceFromOrderPosition(
    request: MsgRemoveDeviceFromOrderPosition
  ): Promise<MsgRemoveDeviceFromOrderPositionResponse> {
    const data = MsgRemoveDeviceFromOrderPosition.encode(request).finish();
    const promise = this.rpc.request(
      'git.openlogisticsfoundation.org.silicon_economy.services.dangerous.core.apps.generated.businesslogic.Msg',
      'RemoveDeviceFromOrderPosition',
      data
    );
    return promise.then((data) => MsgRemoveDeviceFromOrderPositionResponse.decode(new Reader(data)));
  }

  CreateDangerousGoodRegistration(
    request: MsgCreateDangerousGoodRegistration
  ): Promise<MsgCreateDangerousGoodRegistrationResponse> {
    const data = MsgCreateDangerousGoodRegistration.encode(request).finish();
    const promise = this.rpc.request(
      'git.openlogisticsfoundation.org.silicon_economy.services.dangerous.core.apps.generated.businesslogic.Msg',
      'CreateDangerousGoodRegistration',
      data
    );
    return promise.then((data) => MsgCreateDangerousGoodRegistrationResponse.decode(new Reader(data)));
  }

  UpdateDangerousGoodRegistration(
    request: MsgUpdateDangerousGoodRegistration
  ): Promise<MsgUpdateDangerousGoodRegistrationResponse> {
    const data = MsgUpdateDangerousGoodRegistration.encode(request).finish();
    const promise = this.rpc.request(
      'git.openlogisticsfoundation.org.silicon_economy.services.dangerous.core.apps.generated.businesslogic.Msg',
      'UpdateDangerousGoodRegistration',
      data
    );
    return promise.then((data) => MsgUpdateDangerousGoodRegistrationResponse.decode(new Reader(data)));
  }

  DeleteDangerousGoodRegistration(
    request: MsgDeleteDangerousGoodRegistration
  ): Promise<MsgDeleteDangerousGoodRegistrationResponse> {
    const data = MsgDeleteDangerousGoodRegistration.encode(request).finish();
    const promise = this.rpc.request(
      'git.openlogisticsfoundation.org.silicon_economy.services.dangerous.core.apps.generated.businesslogic.Msg',
      'DeleteDangerousGoodRegistration',
      data
    );
    return promise.then((data) => MsgDeleteDangerousGoodRegistrationResponse.decode(new Reader(data)));
  }

  AcceptDangerousGoodRegistration(
    request: MsgAcceptDangerousGoodRegistration
  ): Promise<MsgAcceptDangerousGoodRegistrationResponse> {
    const data = MsgAcceptDangerousGoodRegistration.encode(request).finish();
    const promise = this.rpc.request(
      'git.openlogisticsfoundation.org.silicon_economy.services.dangerous.core.apps.generated.businesslogic.Msg',
      'AcceptDangerousGoodRegistration',
      data
    );
    return promise.then((data) => MsgAcceptDangerousGoodRegistrationResponse.decode(new Reader(data)));
  }
}

interface Rpc {
  request(service: string, method: string, data: Uint8Array): Promise<Uint8Array>;
}

declare var self: any | undefined;
declare var window: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== 'undefined') return globalThis;
  if (typeof self !== 'undefined') return self;
  if (typeof window !== 'undefined') return window;
  if (typeof global !== 'undefined') return global;
  throw 'Unable to locate global object';
})();

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error('Value is larger than ' + Number.MAX_SAFE_INTEGER);
  }
  return long.toNumber();
}

util.Long = Long as any;
configure();
