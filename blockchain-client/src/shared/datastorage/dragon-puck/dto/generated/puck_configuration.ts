/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/* eslint-disable */
import * as Long from 'long';
import { util, configure, Writer, Reader } from 'protobufjs/minimal';

export const protobufPackage =
  'git.openlogisticsfoundation.org.silicon_economy.services.dangerous.core.apps.generated.businesslogic';

export interface PuckConfiguration {
  protocolVersion: number;
  dataAmount: number;
  clearAlarm: boolean;
  ledTimings: LedTimings | undefined;
  measurementCycles: MeasurementCycles | undefined;
  jobState: string;
  humidityAlarm: HumidityAlarm | undefined;
  temperatureAlarm: TemperatureAlarm | undefined;
  deviceId: number;
  jobId: number;
}

export interface LedTimings {
  showId: number;
  showAlarm: number;
  showTemp: number;
}

export interface MeasurementCycles {
  storage: number;
  transport: number;
}

export interface HumidityAlarm {
  lowerBound: number;
  upperBound: number;
}

export interface TemperatureAlarm {
  lowerBound: number;
  upperBound: number;
}

const basePuckConfiguration: object = {
  protocolVersion: 0,
  dataAmount: 0,
  clearAlarm: false,
  jobState: '',
  deviceId: 0,
  jobId: 0,
};

export const PuckConfiguration = {
  encode(message: PuckConfiguration, writer: Writer = Writer.create()): Writer {
    if (message.protocolVersion !== 0) {
      writer.uint32(8).uint64(message.protocolVersion);
    }
    if (message.dataAmount !== 0) {
      writer.uint32(16).uint64(message.dataAmount);
    }
    if (message.clearAlarm === true) {
      writer.uint32(24).bool(message.clearAlarm);
    }
    if (message.ledTimings !== undefined) {
      LedTimings.encode(message.ledTimings, writer.uint32(34).fork()).ldelim();
    }
    if (message.measurementCycles !== undefined) {
      MeasurementCycles.encode(message.measurementCycles, writer.uint32(42).fork()).ldelim();
    }
    if (message.jobState !== '') {
      writer.uint32(50).string(message.jobState);
    }
    if (message.humidityAlarm !== undefined) {
      HumidityAlarm.encode(message.humidityAlarm, writer.uint32(58).fork()).ldelim();
    }
    if (message.temperatureAlarm !== undefined) {
      TemperatureAlarm.encode(message.temperatureAlarm, writer.uint32(66).fork()).ldelim();
    }
    if (message.deviceId !== 0) {
      writer.uint32(72).uint64(message.deviceId);
    }
    if (message.jobId !== 0) {
      writer.uint32(80).uint64(message.jobId);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): PuckConfiguration {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...basePuckConfiguration } as PuckConfiguration;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.protocolVersion = longToNumber(reader.uint64() as Long);
          break;
        case 2:
          message.dataAmount = longToNumber(reader.uint64() as Long);
          break;
        case 3:
          message.clearAlarm = reader.bool();
          break;
        case 4:
          message.ledTimings = LedTimings.decode(reader, reader.uint32());
          break;
        case 5:
          message.measurementCycles = MeasurementCycles.decode(reader, reader.uint32());
          break;
        case 6:
          message.jobState = reader.string();
          break;
        case 7:
          message.humidityAlarm = HumidityAlarm.decode(reader, reader.uint32());
          break;
        case 8:
          message.temperatureAlarm = TemperatureAlarm.decode(reader, reader.uint32());
          break;
        case 9:
          message.deviceId = longToNumber(reader.uint64() as Long);
          break;
        case 10:
          message.jobId = longToNumber(reader.uint64() as Long);
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): PuckConfiguration {
    const message = { ...basePuckConfiguration } as PuckConfiguration;
    if (object.protocolVersion !== undefined && object.protocolVersion !== null) {
      message.protocolVersion = Number(object.protocolVersion);
    } else {
      message.protocolVersion = 0;
    }
    if (object.dataAmount !== undefined && object.dataAmount !== null) {
      message.dataAmount = Number(object.dataAmount);
    } else {
      message.dataAmount = 0;
    }
    if (object.clearAlarm !== undefined && object.clearAlarm !== null) {
      message.clearAlarm = Boolean(object.clearAlarm);
    } else {
      message.clearAlarm = false;
    }
    if (object.ledTimings !== undefined && object.ledTimings !== null) {
      message.ledTimings = LedTimings.fromJSON(object.ledTimings);
    } else {
      message.ledTimings = undefined;
    }
    if (object.measurementCycles !== undefined && object.measurementCycles !== null) {
      message.measurementCycles = MeasurementCycles.fromJSON(object.measurementCycles);
    } else {
      message.measurementCycles = undefined;
    }
    if (object.jobState !== undefined && object.jobState !== null) {
      message.jobState = String(object.jobState);
    } else {
      message.jobState = '';
    }
    if (object.humidityAlarm !== undefined && object.humidityAlarm !== null) {
      message.humidityAlarm = HumidityAlarm.fromJSON(object.humidityAlarm);
    } else {
      message.humidityAlarm = undefined;
    }
    if (object.temperatureAlarm !== undefined && object.temperatureAlarm !== null) {
      message.temperatureAlarm = TemperatureAlarm.fromJSON(object.temperatureAlarm);
    } else {
      message.temperatureAlarm = undefined;
    }
    if (object.deviceId !== undefined && object.deviceId !== null) {
      message.deviceId = Number(object.deviceId);
    } else {
      message.deviceId = 0;
    }
    if (object.jobId !== undefined && object.jobId !== null) {
      message.jobId = Number(object.jobId);
    } else {
      message.jobId = 0;
    }
    return message;
  },

  toJSON(message: PuckConfiguration): unknown {
    const obj: any = {};
    message.protocolVersion !== undefined && (obj.protocolVersion = message.protocolVersion);
    message.dataAmount !== undefined && (obj.dataAmount = message.dataAmount);
    message.clearAlarm !== undefined && (obj.clearAlarm = message.clearAlarm);
    message.ledTimings !== undefined &&
      (obj.ledTimings = message.ledTimings ? LedTimings.toJSON(message.ledTimings) : undefined);
    message.measurementCycles !== undefined &&
      (obj.measurementCycles = message.measurementCycles
        ? MeasurementCycles.toJSON(message.measurementCycles)
        : undefined);
    message.jobState !== undefined && (obj.jobState = message.jobState);
    message.humidityAlarm !== undefined &&
      (obj.humidityAlarm = message.humidityAlarm ? HumidityAlarm.toJSON(message.humidityAlarm) : undefined);
    message.temperatureAlarm !== undefined &&
      (obj.temperatureAlarm = message.temperatureAlarm ? TemperatureAlarm.toJSON(message.temperatureAlarm) : undefined);
    message.deviceId !== undefined && (obj.deviceId = message.deviceId);
    message.jobId !== undefined && (obj.jobId = message.jobId);
    return obj;
  },

  fromPartial(object: DeepPartial<PuckConfiguration>): PuckConfiguration {
    const message = { ...basePuckConfiguration } as PuckConfiguration;
    if (object.protocolVersion !== undefined && object.protocolVersion !== null) {
      message.protocolVersion = object.protocolVersion;
    } else {
      message.protocolVersion = 0;
    }
    if (object.dataAmount !== undefined && object.dataAmount !== null) {
      message.dataAmount = object.dataAmount;
    } else {
      message.dataAmount = 0;
    }
    if (object.clearAlarm !== undefined && object.clearAlarm !== null) {
      message.clearAlarm = object.clearAlarm;
    } else {
      message.clearAlarm = false;
    }
    if (object.ledTimings !== undefined && object.ledTimings !== null) {
      message.ledTimings = LedTimings.fromPartial(object.ledTimings);
    } else {
      message.ledTimings = undefined;
    }
    if (object.measurementCycles !== undefined && object.measurementCycles !== null) {
      message.measurementCycles = MeasurementCycles.fromPartial(object.measurementCycles);
    } else {
      message.measurementCycles = undefined;
    }
    if (object.jobState !== undefined && object.jobState !== null) {
      message.jobState = object.jobState;
    } else {
      message.jobState = '';
    }
    if (object.humidityAlarm !== undefined && object.humidityAlarm !== null) {
      message.humidityAlarm = HumidityAlarm.fromPartial(object.humidityAlarm);
    } else {
      message.humidityAlarm = undefined;
    }
    if (object.temperatureAlarm !== undefined && object.temperatureAlarm !== null) {
      message.temperatureAlarm = TemperatureAlarm.fromPartial(object.temperatureAlarm);
    } else {
      message.temperatureAlarm = undefined;
    }
    if (object.deviceId !== undefined && object.deviceId !== null) {
      message.deviceId = object.deviceId;
    } else {
      message.deviceId = 0;
    }
    if (object.jobId !== undefined && object.jobId !== null) {
      message.jobId = object.jobId;
    } else {
      message.jobId = 0;
    }
    return message;
  },
};

const baseLedTimings: object = { showId: 0, showAlarm: 0, showTemp: 0 };

export const LedTimings = {
  encode(message: LedTimings, writer: Writer = Writer.create()): Writer {
    if (message.showId !== 0) {
      writer.uint32(8).uint64(message.showId);
    }
    if (message.showAlarm !== 0) {
      writer.uint32(16).uint64(message.showAlarm);
    }
    if (message.showTemp !== 0) {
      writer.uint32(24).uint64(message.showTemp);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): LedTimings {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseLedTimings } as LedTimings;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.showId = longToNumber(reader.uint64() as Long);
          break;
        case 2:
          message.showAlarm = longToNumber(reader.uint64() as Long);
          break;
        case 3:
          message.showTemp = longToNumber(reader.uint64() as Long);
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): LedTimings {
    const message = { ...baseLedTimings } as LedTimings;
    if (object.showId !== undefined && object.showId !== null) {
      message.showId = Number(object.showId);
    } else {
      message.showId = 0;
    }
    if (object.showAlarm !== undefined && object.showAlarm !== null) {
      message.showAlarm = Number(object.showAlarm);
    } else {
      message.showAlarm = 0;
    }
    if (object.showTemp !== undefined && object.showTemp !== null) {
      message.showTemp = Number(object.showTemp);
    } else {
      message.showTemp = 0;
    }
    return message;
  },

  toJSON(message: LedTimings): unknown {
    const obj: any = {};
    message.showId !== undefined && (obj.showId = message.showId);
    message.showAlarm !== undefined && (obj.showAlarm = message.showAlarm);
    message.showTemp !== undefined && (obj.showTemp = message.showTemp);
    return obj;
  },

  fromPartial(object: DeepPartial<LedTimings>): LedTimings {
    const message = { ...baseLedTimings } as LedTimings;
    if (object.showId !== undefined && object.showId !== null) {
      message.showId = object.showId;
    } else {
      message.showId = 0;
    }
    if (object.showAlarm !== undefined && object.showAlarm !== null) {
      message.showAlarm = object.showAlarm;
    } else {
      message.showAlarm = 0;
    }
    if (object.showTemp !== undefined && object.showTemp !== null) {
      message.showTemp = object.showTemp;
    } else {
      message.showTemp = 0;
    }
    return message;
  },
};

const baseMeasurementCycles: object = { storage: 0, transport: 0 };

export const MeasurementCycles = {
  encode(message: MeasurementCycles, writer: Writer = Writer.create()): Writer {
    if (message.storage !== 0) {
      writer.uint32(8).uint64(message.storage);
    }
    if (message.transport !== 0) {
      writer.uint32(16).uint64(message.transport);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MeasurementCycles {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMeasurementCycles } as MeasurementCycles;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.storage = longToNumber(reader.uint64() as Long);
          break;
        case 2:
          message.transport = longToNumber(reader.uint64() as Long);
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MeasurementCycles {
    const message = { ...baseMeasurementCycles } as MeasurementCycles;
    if (object.storage !== undefined && object.storage !== null) {
      message.storage = Number(object.storage);
    } else {
      message.storage = 0;
    }
    if (object.transport !== undefined && object.transport !== null) {
      message.transport = Number(object.transport);
    } else {
      message.transport = 0;
    }
    return message;
  },

  toJSON(message: MeasurementCycles): unknown {
    const obj: any = {};
    message.storage !== undefined && (obj.storage = message.storage);
    message.transport !== undefined && (obj.transport = message.transport);
    return obj;
  },

  fromPartial(object: DeepPartial<MeasurementCycles>): MeasurementCycles {
    const message = { ...baseMeasurementCycles } as MeasurementCycles;
    if (object.storage !== undefined && object.storage !== null) {
      message.storage = object.storage;
    } else {
      message.storage = 0;
    }
    if (object.transport !== undefined && object.transport !== null) {
      message.transport = object.transport;
    } else {
      message.transport = 0;
    }
    return message;
  },
};

const baseHumidityAlarm: object = { lowerBound: 0, upperBound: 0 };

export const HumidityAlarm = {
  encode(message: HumidityAlarm, writer: Writer = Writer.create()): Writer {
    if (message.lowerBound !== 0) {
      writer.uint32(13).float(message.lowerBound);
    }
    if (message.upperBound !== 0) {
      writer.uint32(21).float(message.upperBound);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): HumidityAlarm {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseHumidityAlarm } as HumidityAlarm;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.lowerBound = reader.float();
          break;
        case 2:
          message.upperBound = reader.float();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): HumidityAlarm {
    const message = { ...baseHumidityAlarm } as HumidityAlarm;
    if (object.lowerBound !== undefined && object.lowerBound !== null) {
      message.lowerBound = Number(object.lowerBound);
    } else {
      message.lowerBound = 0;
    }
    if (object.upperBound !== undefined && object.upperBound !== null) {
      message.upperBound = Number(object.upperBound);
    } else {
      message.upperBound = 0;
    }
    return message;
  },

  toJSON(message: HumidityAlarm): unknown {
    const obj: any = {};
    message.lowerBound !== undefined && (obj.lowerBound = message.lowerBound);
    message.upperBound !== undefined && (obj.upperBound = message.upperBound);
    return obj;
  },

  fromPartial(object: DeepPartial<HumidityAlarm>): HumidityAlarm {
    const message = { ...baseHumidityAlarm } as HumidityAlarm;
    if (object.lowerBound !== undefined && object.lowerBound !== null) {
      message.lowerBound = object.lowerBound;
    } else {
      message.lowerBound = 0;
    }
    if (object.upperBound !== undefined && object.upperBound !== null) {
      message.upperBound = object.upperBound;
    } else {
      message.upperBound = 0;
    }
    return message;
  },
};

const baseTemperatureAlarm: object = { lowerBound: 0, upperBound: 0 };

export const TemperatureAlarm = {
  encode(message: TemperatureAlarm, writer: Writer = Writer.create()): Writer {
    if (message.lowerBound !== 0) {
      writer.uint32(13).float(message.lowerBound);
    }
    if (message.upperBound !== 0) {
      writer.uint32(21).float(message.upperBound);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): TemperatureAlarm {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseTemperatureAlarm } as TemperatureAlarm;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.lowerBound = reader.float();
          break;
        case 2:
          message.upperBound = reader.float();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): TemperatureAlarm {
    const message = { ...baseTemperatureAlarm } as TemperatureAlarm;
    if (object.lowerBound !== undefined && object.lowerBound !== null) {
      message.lowerBound = Number(object.lowerBound);
    } else {
      message.lowerBound = 0;
    }
    if (object.upperBound !== undefined && object.upperBound !== null) {
      message.upperBound = Number(object.upperBound);
    } else {
      message.upperBound = 0;
    }
    return message;
  },

  toJSON(message: TemperatureAlarm): unknown {
    const obj: any = {};
    message.lowerBound !== undefined && (obj.lowerBound = message.lowerBound);
    message.upperBound !== undefined && (obj.upperBound = message.upperBound);
    return obj;
  },

  fromPartial(object: DeepPartial<TemperatureAlarm>): TemperatureAlarm {
    const message = { ...baseTemperatureAlarm } as TemperatureAlarm;
    if (object.lowerBound !== undefined && object.lowerBound !== null) {
      message.lowerBound = object.lowerBound;
    } else {
      message.lowerBound = 0;
    }
    if (object.upperBound !== undefined && object.upperBound !== null) {
      message.upperBound = object.upperBound;
    } else {
      message.upperBound = 0;
    }
    return message;
  },
};

declare var self: any | undefined;
declare var window: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== 'undefined') return globalThis;
  if (typeof self !== 'undefined') return self;
  if (typeof window !== 'undefined') return window;
  if (typeof global !== 'undefined') return global;
  throw 'Unable to locate global object';
})();

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error('Value is larger than Number.MAX_SAFE_INTEGER');
  }
  return long.toNumber();
}

if (true) {
  util.Long = Long as any;
  configure();
}
