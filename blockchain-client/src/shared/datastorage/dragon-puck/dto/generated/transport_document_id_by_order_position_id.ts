/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/* eslint-disable */
import { Writer, Reader } from 'protobufjs/minimal';

export const protobufPackage =
  'git.openlogisticsfoundation.org.silicon_economy.services.dangerous.core.apps.generated.businesslogic';

export interface TransportDocumentIdByOrderPositionId {
  creator: string;
  orderPositionId: string;
  transportDocumentId: string;
}

const baseTransportDocumentIdByOrderPositionId: object = { creator: '', orderPositionId: '', transportDocumentId: '' };

export const TransportDocumentIdByOrderPositionId = {
  encode(message: TransportDocumentIdByOrderPositionId, writer: Writer = Writer.create()): Writer {
    if (message.creator !== '') {
      writer.uint32(10).string(message.creator);
    }
    if (message.orderPositionId !== '') {
      writer.uint32(18).string(message.orderPositionId);
    }
    if (message.transportDocumentId !== '') {
      writer.uint32(26).string(message.transportDocumentId);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): TransportDocumentIdByOrderPositionId {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseTransportDocumentIdByOrderPositionId } as TransportDocumentIdByOrderPositionId;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.orderPositionId = reader.string();
          break;
        case 3:
          message.transportDocumentId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): TransportDocumentIdByOrderPositionId {
    const message = { ...baseTransportDocumentIdByOrderPositionId } as TransportDocumentIdByOrderPositionId;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = '';
    }
    if (object.orderPositionId !== undefined && object.orderPositionId !== null) {
      message.orderPositionId = String(object.orderPositionId);
    } else {
      message.orderPositionId = '';
    }
    if (object.transportDocumentId !== undefined && object.transportDocumentId !== null) {
      message.transportDocumentId = String(object.transportDocumentId);
    } else {
      message.transportDocumentId = '';
    }
    return message;
  },

  toJSON(message: TransportDocumentIdByOrderPositionId): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.orderPositionId !== undefined && (obj.orderPositionId = message.orderPositionId);
    message.transportDocumentId !== undefined && (obj.transportDocumentId = message.transportDocumentId);
    return obj;
  },

  fromPartial(object: DeepPartial<TransportDocumentIdByOrderPositionId>): TransportDocumentIdByOrderPositionId {
    const message = { ...baseTransportDocumentIdByOrderPositionId } as TransportDocumentIdByOrderPositionId;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = '';
    }
    if (object.orderPositionId !== undefined && object.orderPositionId !== null) {
      message.orderPositionId = object.orderPositionId;
    } else {
      message.orderPositionId = '';
    }
    if (object.transportDocumentId !== undefined && object.transportDocumentId !== null) {
      message.transportDocumentId = object.transportDocumentId;
    } else {
      message.transportDocumentId = '';
    }
    return message;
  },
};

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;
