/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/* eslint-disable */
import * as Long from 'long';
import { configure, Reader, util, Writer } from 'protobufjs/minimal';

export const protobufPackage = 'git.openlogisticsfoundation.org.silicon_economy.services.dangerous.core.apps.generated.businesslogic';

export interface TransportDocument {
  creator: string;
  id: string;
  sender: Company | undefined;
  freight: Freight | undefined;
  carrier: Carrier | undefined;
  logEntries: LogEntry[];
  status: string;
  createdDate: number;
  lastUpdate: number;
}

export interface Company {
  name: string;
  address: Address | undefined;
  contact: ContactPerson | undefined;
}

export interface Address {
  street: string;
  number: string;
  postalCode: string;
  city: string;
  country: string;
}

export interface ContactPerson {
  name: string;
  phone: string;
  mail: string;
  department: string;
}

export interface Freight {
  orders: Order[];
  load: string;
  additionalInformation: string;
  transportationInstructions: string;
  totalTransportPoints: number;
}

export interface Order {
  id: string;
  consignee: Company | undefined;
  logEntries: LogEntry[];
  status: string;
  orderPositions: OrderPosition[];
}

export interface OrderPosition {
  dangerousGood: DangerousGood | undefined;
  package: string;
  packagingCode: string;
  quantity: number;
  unit: string;
  individualAmount: number;
  polluting: boolean;
  transportPoints: number;
  totalAmount: number;
  id: number;
  deviceId: number;
  status: string;
  logEntries: LogEntry[];
}

export interface LogEntry {
  status: string;
  date: number;
  author: string;
  description: string;
  acceptanceCriteria: AcceptanceCriteria | undefined;
}

export interface DangerousGood {
  unNumber: string;
  casNumber: string;
  description: string;
  label1: string;
  label2: string;
  label3: string;
  packingGroup: string;
  tunnelRestrictionCode: string;
  transportCategory: string;
}

export interface Carrier {
  name: string;
  driver: string;
  licensePlate: string;
}

export interface AcceptanceCriteria {
  comment: string;
  carrierCheckCriteria: CarrierCheckCriteria | undefined;
  transportVehicleCriteria: TransportVehicleCriteria | undefined;
  visualInspectionCarrierCriteria: VisualInspectionCarrierCriteria | undefined;
  visualInspectionConsigneeCriteria: VisualInspectionConsigneeCriteria | undefined;
}

export interface CarrierCheckCriteria {
  acceptSender: boolean;
  acceptConsignees: boolean[];
  acceptFreight: boolean;
  acceptAdditionalInformation: boolean;
  acceptTransportationInstructions: boolean;
}

export interface TransportVehicleCriteria {
  licencePlate: string;
  acceptVehicleCondition: boolean;
  acceptVehicleSafetyEquipment: boolean;
  driverName: string;
  acceptCarrierInformation: boolean;
  acceptCarrierSafetyEquipment: boolean;
}

export interface VisualInspectionCarrierCriteria {
  acceptTransportability: boolean;
  acceptLabeling: boolean;
}

export interface VisualInspectionConsigneeCriteria {
  acceptQuantity: boolean;
  acceptIntactness: boolean;
}

const baseTransportDocument: object = {
  creator: '',
  id: '',
  status: '',
  createdDate: 0,
  lastUpdate: 0,
};

export const TransportDocument = {
  encode(message: TransportDocument, writer: Writer = Writer.create()): Writer {
    if (message.creator !== '') {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== '') {
      writer.uint32(18).string(message.id);
    }
    if (message.sender !== undefined) {
      Company.encode(message.sender, writer.uint32(26).fork()).ldelim();
    }
    if (message.freight !== undefined) {
      Freight.encode(message.freight, writer.uint32(34).fork()).ldelim();
    }
    if (message.carrier !== undefined) {
      Carrier.encode(message.carrier, writer.uint32(42).fork()).ldelim();
    }
    for (const v of message.logEntries) {
      LogEntry.encode(v!, writer.uint32(50).fork()).ldelim();
    }
    if (message.status !== '') {
      writer.uint32(58).string(message.status);
    }
    if (message.createdDate !== 0) {
      writer.uint32(65).double(message.createdDate);
    }
    if (message.lastUpdate !== 0) {
      writer.uint32(73).double(message.lastUpdate);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): TransportDocument {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseTransportDocument } as TransportDocument;
    message.logEntries = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.sender = Company.decode(reader, reader.uint32());
          break;
        case 4:
          message.freight = Freight.decode(reader, reader.uint32());
          break;
        case 5:
          message.carrier = Carrier.decode(reader, reader.uint32());
          break;
        case 6:
          message.logEntries.push(LogEntry.decode(reader, reader.uint32()));
          break;
        case 7:
          message.status = reader.string();
          break;
        case 8:
          message.createdDate = reader.double();
          break;
        case 9:
          message.lastUpdate = reader.double();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): TransportDocument {
    const message = { ...baseTransportDocument } as TransportDocument;
    message.logEntries = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = '';
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = '';
    }
    if (object.sender !== undefined && object.sender !== null) {
      message.sender = Company.fromJSON(object.sender);
    } else {
      message.sender = undefined;
    }
    if (object.freight !== undefined && object.freight !== null) {
      message.freight = Freight.fromJSON(object.freight);
    } else {
      message.freight = undefined;
    }
    if (object.carrier !== undefined && object.carrier !== null) {
      message.carrier = Carrier.fromJSON(object.carrier);
    } else {
      message.carrier = undefined;
    }
    if (object.logEntries !== undefined && object.logEntries !== null) {
      for (const e of object.logEntries) {
        message.logEntries.push(LogEntry.fromJSON(e));
      }
    }
    if (object.status !== undefined && object.status !== null) {
      message.status = String(object.status);
    } else {
      message.status = '';
    }
    if (object.createdDate !== undefined && object.createdDate !== null) {
      message.createdDate = Number(object.createdDate);
    } else {
      message.createdDate = 0;
    }
    if (object.lastUpdate !== undefined && object.lastUpdate !== null) {
      message.lastUpdate = Number(object.lastUpdate);
    } else {
      message.lastUpdate = 0;
    }
    return message;
  },

  toJSON(message: TransportDocument): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.sender !== undefined && (obj.sender = message.sender ? Company.toJSON(message.sender) : undefined);
    message.freight !== undefined && (obj.freight = message.freight ? Freight.toJSON(message.freight) : undefined);
    message.carrier !== undefined && (obj.carrier = message.carrier ? Carrier.toJSON(message.carrier) : undefined);
    if (message.logEntries) {
      obj.logEntries = message.logEntries.map((e) => (e ? LogEntry.toJSON(e) : undefined));
    } else {
      obj.logEntries = [];
    }
    message.status !== undefined && (obj.status = message.status);
    message.createdDate !== undefined && (obj.createdDate = message.createdDate);
    message.lastUpdate !== undefined && (obj.lastUpdate = message.lastUpdate);
    return obj;
  },

  fromPartial(object: DeepPartial<TransportDocument>): TransportDocument {
    const message = { ...baseTransportDocument } as TransportDocument;
    message.logEntries = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = '';
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = '';
    }
    if (object.sender !== undefined && object.sender !== null) {
      message.sender = Company.fromPartial(object.sender);
    } else {
      message.sender = undefined;
    }
    if (object.freight !== undefined && object.freight !== null) {
      message.freight = Freight.fromPartial(object.freight);
    } else {
      message.freight = undefined;
    }
    if (object.carrier !== undefined && object.carrier !== null) {
      message.carrier = Carrier.fromPartial(object.carrier);
    } else {
      message.carrier = undefined;
    }
    if (object.logEntries !== undefined && object.logEntries !== null) {
      for (const e of object.logEntries) {
        message.logEntries.push(LogEntry.fromPartial(e));
      }
    }
    if (object.status !== undefined && object.status !== null) {
      message.status = object.status;
    } else {
      message.status = '';
    }
    if (object.createdDate !== undefined && object.createdDate !== null) {
      message.createdDate = object.createdDate;
    } else {
      message.createdDate = 0;
    }
    if (object.lastUpdate !== undefined && object.lastUpdate !== null) {
      message.lastUpdate = object.lastUpdate;
    } else {
      message.lastUpdate = 0;
    }
    return message;
  },
};

const baseCompany: object = { name: '' };

export const Company = {
  encode(message: Company, writer: Writer = Writer.create()): Writer {
    if (message.name !== '') {
      writer.uint32(10).string(message.name);
    }
    if (message.address !== undefined) {
      Address.encode(message.address, writer.uint32(18).fork()).ldelim();
    }
    if (message.contact !== undefined) {
      ContactPerson.encode(message.contact, writer.uint32(26).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Company {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseCompany } as Company;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.name = reader.string();
          break;
        case 2:
          message.address = Address.decode(reader, reader.uint32());
          break;
        case 3:
          message.contact = ContactPerson.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Company {
    const message = { ...baseCompany } as Company;
    if (object.name !== undefined && object.name !== null) {
      message.name = String(object.name);
    } else {
      message.name = '';
    }
    if (object.address !== undefined && object.address !== null) {
      message.address = Address.fromJSON(object.address);
    } else {
      message.address = undefined;
    }
    if (object.contact !== undefined && object.contact !== null) {
      message.contact = ContactPerson.fromJSON(object.contact);
    } else {
      message.contact = undefined;
    }
    return message;
  },

  toJSON(message: Company): unknown {
    const obj: any = {};
    message.name !== undefined && (obj.name = message.name);
    message.address !== undefined && (obj.address = message.address ? Address.toJSON(message.address) : undefined);
    message.contact !== undefined &&
      (obj.contact = message.contact ? ContactPerson.toJSON(message.contact) : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<Company>): Company {
    const message = { ...baseCompany } as Company;
    if (object.name !== undefined && object.name !== null) {
      message.name = object.name;
    } else {
      message.name = '';
    }
    if (object.address !== undefined && object.address !== null) {
      message.address = Address.fromPartial(object.address);
    } else {
      message.address = undefined;
    }
    if (object.contact !== undefined && object.contact !== null) {
      message.contact = ContactPerson.fromPartial(object.contact);
    } else {
      message.contact = undefined;
    }
    return message;
  },
};

const baseAddress: object = {
  street: '',
  number: '',
  postalCode: '',
  city: '',
  country: '',
};

export const Address = {
  encode(message: Address, writer: Writer = Writer.create()): Writer {
    if (message.street !== '') {
      writer.uint32(10).string(message.street);
    }
    if (message.number !== '') {
      writer.uint32(18).string(message.number);
    }
    if (message.postalCode !== '') {
      writer.uint32(26).string(message.postalCode);
    }
    if (message.city !== '') {
      writer.uint32(34).string(message.city);
    }
    if (message.country !== '') {
      writer.uint32(42).string(message.country);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Address {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseAddress } as Address;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.street = reader.string();
          break;
        case 2:
          message.number = reader.string();
          break;
        case 3:
          message.postalCode = reader.string();
          break;
        case 4:
          message.city = reader.string();
          break;
        case 5:
          message.country = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Address {
    const message = { ...baseAddress } as Address;
    if (object.street !== undefined && object.street !== null) {
      message.street = String(object.street);
    } else {
      message.street = '';
    }
    if (object.number !== undefined && object.number !== null) {
      message.number = String(object.number);
    } else {
      message.number = '';
    }
    if (object.postalCode !== undefined && object.postalCode !== null) {
      message.postalCode = String(object.postalCode);
    } else {
      message.postalCode = '';
    }
    if (object.city !== undefined && object.city !== null) {
      message.city = String(object.city);
    } else {
      message.city = '';
    }
    if (object.country !== undefined && object.country !== null) {
      message.country = String(object.country);
    } else {
      message.country = '';
    }
    return message;
  },

  toJSON(message: Address): unknown {
    const obj: any = {};
    message.street !== undefined && (obj.street = message.street);
    message.number !== undefined && (obj.number = message.number);
    message.postalCode !== undefined && (obj.postalCode = message.postalCode);
    message.city !== undefined && (obj.city = message.city);
    message.country !== undefined && (obj.country = message.country);
    return obj;
  },

  fromPartial(object: DeepPartial<Address>): Address {
    const message = { ...baseAddress } as Address;
    if (object.street !== undefined && object.street !== null) {
      message.street = object.street;
    } else {
      message.street = '';
    }
    if (object.number !== undefined && object.number !== null) {
      message.number = object.number;
    } else {
      message.number = '';
    }
    if (object.postalCode !== undefined && object.postalCode !== null) {
      message.postalCode = object.postalCode;
    } else {
      message.postalCode = '';
    }
    if (object.city !== undefined && object.city !== null) {
      message.city = object.city;
    } else {
      message.city = '';
    }
    if (object.country !== undefined && object.country !== null) {
      message.country = object.country;
    } else {
      message.country = '';
    }
    return message;
  },
};

const baseContactPerson: object = {
  name: '',
  phone: '',
  mail: '',
  department: '',
};

export const ContactPerson = {
  encode(message: ContactPerson, writer: Writer = Writer.create()): Writer {
    if (message.name !== '') {
      writer.uint32(10).string(message.name);
    }
    if (message.phone !== '') {
      writer.uint32(18).string(message.phone);
    }
    if (message.mail !== '') {
      writer.uint32(26).string(message.mail);
    }
    if (message.department !== '') {
      writer.uint32(34).string(message.department);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): ContactPerson {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseContactPerson } as ContactPerson;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.name = reader.string();
          break;
        case 2:
          message.phone = reader.string();
          break;
        case 3:
          message.mail = reader.string();
          break;
        case 4:
          message.department = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ContactPerson {
    const message = { ...baseContactPerson } as ContactPerson;
    if (object.name !== undefined && object.name !== null) {
      message.name = String(object.name);
    } else {
      message.name = '';
    }
    if (object.phone !== undefined && object.phone !== null) {
      message.phone = String(object.phone);
    } else {
      message.phone = '';
    }
    if (object.mail !== undefined && object.mail !== null) {
      message.mail = String(object.mail);
    } else {
      message.mail = '';
    }
    if (object.department !== undefined && object.department !== null) {
      message.department = String(object.department);
    } else {
      message.department = '';
    }
    return message;
  },

  toJSON(message: ContactPerson): unknown {
    const obj: any = {};
    message.name !== undefined && (obj.name = message.name);
    message.phone !== undefined && (obj.phone = message.phone);
    message.mail !== undefined && (obj.mail = message.mail);
    message.department !== undefined && (obj.department = message.department);
    return obj;
  },

  fromPartial(object: DeepPartial<ContactPerson>): ContactPerson {
    const message = { ...baseContactPerson } as ContactPerson;
    if (object.name !== undefined && object.name !== null) {
      message.name = object.name;
    } else {
      message.name = '';
    }
    if (object.phone !== undefined && object.phone !== null) {
      message.phone = object.phone;
    } else {
      message.phone = '';
    }
    if (object.mail !== undefined && object.mail !== null) {
      message.mail = object.mail;
    } else {
      message.mail = '';
    }
    if (object.department !== undefined && object.department !== null) {
      message.department = object.department;
    } else {
      message.department = '';
    }
    return message;
  },
};

const baseFreight: object = {
  load: '',
  additionalInformation: '',
  transportationInstructions: '',
  totalTransportPoints: 0,
};

export const Freight = {
  encode(message: Freight, writer: Writer = Writer.create()): Writer {
    for (const v of message.orders) {
      Order.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.load !== '') {
      writer.uint32(18).string(message.load);
    }
    if (message.additionalInformation !== '') {
      writer.uint32(26).string(message.additionalInformation);
    }
    if (message.transportationInstructions !== '') {
      writer.uint32(34).string(message.transportationInstructions);
    }
    if (message.totalTransportPoints !== 0) {
      writer.uint32(41).double(message.totalTransportPoints);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Freight {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseFreight } as Freight;
    message.orders = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.orders.push(Order.decode(reader, reader.uint32()));
          break;
        case 2:
          message.load = reader.string();
          break;
        case 3:
          message.additionalInformation = reader.string();
          break;
        case 4:
          message.transportationInstructions = reader.string();
          break;
        case 5:
          message.totalTransportPoints = reader.double();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Freight {
    const message = { ...baseFreight } as Freight;
    message.orders = [];
    if (object.orders !== undefined && object.orders !== null) {
      for (const e of object.orders) {
        message.orders.push(Order.fromJSON(e));
      }
    }
    if (object.load !== undefined && object.load !== null) {
      message.load = String(object.load);
    } else {
      message.load = '';
    }
    if (object.additionalInformation !== undefined && object.additionalInformation !== null) {
      message.additionalInformation = String(object.additionalInformation);
    } else {
      message.additionalInformation = '';
    }
    if (object.transportationInstructions !== undefined && object.transportationInstructions !== null) {
      message.transportationInstructions = String(object.transportationInstructions);
    } else {
      message.transportationInstructions = '';
    }
    if (object.totalTransportPoints !== undefined && object.totalTransportPoints !== null) {
      message.totalTransportPoints = Number(object.totalTransportPoints);
    } else {
      message.totalTransportPoints = 0;
    }
    return message;
  },

  toJSON(message: Freight): unknown {
    const obj: any = {};
    if (message.orders) {
      obj.orders = message.orders.map((e) => (e ? Order.toJSON(e) : undefined));
    } else {
      obj.orders = [];
    }
    message.load !== undefined && (obj.load = message.load);
    message.additionalInformation !== undefined && (obj.additionalInformation = message.additionalInformation);
    message.transportationInstructions !== undefined &&
      (obj.transportationInstructions = message.transportationInstructions);
    message.totalTransportPoints !== undefined && (obj.totalTransportPoints = message.totalTransportPoints);
    return obj;
  },

  fromPartial(object: DeepPartial<Freight>): Freight {
    const message = { ...baseFreight } as Freight;
    message.orders = [];
    if (object.orders !== undefined && object.orders !== null) {
      for (const e of object.orders) {
        message.orders.push(Order.fromPartial(e));
      }
    }
    if (object.load !== undefined && object.load !== null) {
      message.load = object.load;
    } else {
      message.load = '';
    }
    if (object.additionalInformation !== undefined && object.additionalInformation !== null) {
      message.additionalInformation = object.additionalInformation;
    } else {
      message.additionalInformation = '';
    }
    if (object.transportationInstructions !== undefined && object.transportationInstructions !== null) {
      message.transportationInstructions = object.transportationInstructions;
    } else {
      message.transportationInstructions = '';
    }
    if (object.totalTransportPoints !== undefined && object.totalTransportPoints !== null) {
      message.totalTransportPoints = object.totalTransportPoints;
    } else {
      message.totalTransportPoints = 0;
    }
    return message;
  },
};

const baseOrder: object = { id: '', status: '' };

export const Order = {
  encode(message: Order, writer: Writer = Writer.create()): Writer {
    if (message.id !== '') {
      writer.uint32(10).string(message.id);
    }
    if (message.consignee !== undefined) {
      Company.encode(message.consignee, writer.uint32(18).fork()).ldelim();
    }
    for (const v of message.logEntries) {
      LogEntry.encode(v!, writer.uint32(26).fork()).ldelim();
    }
    if (message.status !== '') {
      writer.uint32(34).string(message.status);
    }
    for (const v of message.orderPositions) {
      OrderPosition.encode(v!, writer.uint32(42).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Order {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseOrder } as Order;
    message.logEntries = [];
    message.orderPositions = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        case 2:
          message.consignee = Company.decode(reader, reader.uint32());
          break;
        case 3:
          message.logEntries.push(LogEntry.decode(reader, reader.uint32()));
          break;
        case 4:
          message.status = reader.string();
          break;
        case 5:
          message.orderPositions.push(OrderPosition.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Order {
    const message = { ...baseOrder } as Order;
    message.logEntries = [];
    message.orderPositions = [];
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = '';
    }
    if (object.consignee !== undefined && object.consignee !== null) {
      message.consignee = Company.fromJSON(object.consignee);
    } else {
      message.consignee = undefined;
    }
    if (object.logEntries !== undefined && object.logEntries !== null) {
      for (const e of object.logEntries) {
        message.logEntries.push(LogEntry.fromJSON(e));
      }
    }
    if (object.status !== undefined && object.status !== null) {
      message.status = String(object.status);
    } else {
      message.status = '';
    }
    if (object.orderPositions !== undefined && object.orderPositions !== null) {
      for (const e of object.orderPositions) {
        message.orderPositions.push(OrderPosition.fromJSON(e));
      }
    }
    return message;
  },

  toJSON(message: Order): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.consignee !== undefined &&
      (obj.consignee = message.consignee ? Company.toJSON(message.consignee) : undefined);
    if (message.logEntries) {
      obj.logEntries = message.logEntries.map((e) => (e ? LogEntry.toJSON(e) : undefined));
    } else {
      obj.logEntries = [];
    }
    message.status !== undefined && (obj.status = message.status);
    if (message.orderPositions) {
      obj.orderPositions = message.orderPositions.map((e) => (e ? OrderPosition.toJSON(e) : undefined));
    } else {
      obj.orderPositions = [];
    }
    return obj;
  },

  fromPartial(object: DeepPartial<Order>): Order {
    const message = { ...baseOrder } as Order;
    message.logEntries = [];
    message.orderPositions = [];
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = '';
    }
    if (object.consignee !== undefined && object.consignee !== null) {
      message.consignee = Company.fromPartial(object.consignee);
    } else {
      message.consignee = undefined;
    }
    if (object.logEntries !== undefined && object.logEntries !== null) {
      for (const e of object.logEntries) {
        message.logEntries.push(LogEntry.fromPartial(e));
      }
    }
    if (object.status !== undefined && object.status !== null) {
      message.status = object.status;
    } else {
      message.status = '';
    }
    if (object.orderPositions !== undefined && object.orderPositions !== null) {
      for (const e of object.orderPositions) {
        message.orderPositions.push(OrderPosition.fromPartial(e));
      }
    }
    return message;
  },
};

const baseOrderPosition: object = {
  package: '',
  packagingCode: '',
  quantity: 0,
  unit: '',
  individualAmount: 0,
  polluting: false,
  transportPoints: 0,
  totalAmount: 0,
  id: 0,
  deviceId: 0,
  status: '',
};

export const OrderPosition = {
  encode(message: OrderPosition, writer: Writer = Writer.create()): Writer {
    if (message.dangerousGood !== undefined) {
      DangerousGood.encode(message.dangerousGood, writer.uint32(10).fork()).ldelim();
    }
    if (message.package !== '') {
      writer.uint32(18).string(message.package);
    }
    if (message.packagingCode !== '') {
      writer.uint32(26).string(message.packagingCode);
    }
    if (message.quantity !== 0) {
      writer.uint32(33).double(message.quantity);
    }
    if (message.unit !== '') {
      writer.uint32(42).string(message.unit);
    }
    if (message.individualAmount !== 0) {
      writer.uint32(49).double(message.individualAmount);
    }
    if (message.polluting === true) {
      writer.uint32(56).bool(message.polluting);
    }
    if (message.transportPoints !== 0) {
      writer.uint32(65).double(message.transportPoints);
    }
    if (message.totalAmount !== 0) {
      writer.uint32(73).double(message.totalAmount);
    }
    if (message.id !== 0) {
      writer.uint32(80).uint64(message.id);
    }
    if (message.deviceId !== 0) {
      writer.uint32(88).uint64(message.deviceId);
    }
    if (message.status !== '') {
      writer.uint32(98).string(message.status);
    }
    for (const v of message.logEntries) {
      LogEntry.encode(v!, writer.uint32(106).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): OrderPosition {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseOrderPosition } as OrderPosition;
    message.logEntries = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.dangerousGood = DangerousGood.decode(reader, reader.uint32());
          break;
        case 2:
          message.package = reader.string();
          break;
        case 3:
          message.packagingCode = reader.string();
          break;
        case 4:
          message.quantity = reader.double();
          break;
        case 5:
          message.unit = reader.string();
          break;
        case 6:
          message.individualAmount = reader.double();
          break;
        case 7:
          message.polluting = reader.bool();
          break;
        case 8:
          message.transportPoints = reader.double();
          break;
        case 9:
          message.totalAmount = reader.double();
          break;
        case 10:
          message.id = longToNumber(reader.uint64() as Long);
          break;
        case 11:
          message.deviceId = longToNumber(reader.uint64() as Long);
          break;
        case 12:
          message.status = reader.string();
          break;
        case 13:
          message.logEntries.push(LogEntry.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): OrderPosition {
    const message = { ...baseOrderPosition } as OrderPosition;
    message.logEntries = [];
    if (object.dangerousGood !== undefined && object.dangerousGood !== null) {
      message.dangerousGood = DangerousGood.fromJSON(object.dangerousGood);
    } else {
      message.dangerousGood = undefined;
    }
    if (object.package !== undefined && object.package !== null) {
      message.package = String(object.package);
    } else {
      message.package = '';
    }
    if (object.packagingCode !== undefined && object.packagingCode !== null) {
      message.packagingCode = String(object.packagingCode);
    } else {
      message.packagingCode = '';
    }
    if (object.quantity !== undefined && object.quantity !== null) {
      message.quantity = Number(object.quantity);
    } else {
      message.quantity = 0;
    }
    if (object.unit !== undefined && object.unit !== null) {
      message.unit = String(object.unit);
    } else {
      message.unit = '';
    }
    if (object.individualAmount !== undefined && object.individualAmount !== null) {
      message.individualAmount = Number(object.individualAmount);
    } else {
      message.individualAmount = 0;
    }
    if (object.polluting !== undefined && object.polluting !== null) {
      message.polluting = Boolean(object.polluting);
    } else {
      message.polluting = false;
    }
    if (object.transportPoints !== undefined && object.transportPoints !== null) {
      message.transportPoints = Number(object.transportPoints);
    } else {
      message.transportPoints = 0;
    }
    if (object.totalAmount !== undefined && object.totalAmount !== null) {
      message.totalAmount = Number(object.totalAmount);
    } else {
      message.totalAmount = 0;
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = Number(object.id);
    } else {
      message.id = 0;
    }
    if (object.deviceId !== undefined && object.deviceId !== null) {
      message.deviceId = Number(object.deviceId);
    } else {
      message.deviceId = 0;
    }
    if (object.status !== undefined && object.status !== null) {
      message.status = String(object.status);
    } else {
      message.status = '';
    }
    if (object.logEntries !== undefined && object.logEntries !== null) {
      for (const e of object.logEntries) {
        message.logEntries.push(LogEntry.fromJSON(e));
      }
    }
    return message;
  },

  toJSON(message: OrderPosition): unknown {
    const obj: any = {};
    message.dangerousGood !== undefined &&
      (obj.dangerousGood = message.dangerousGood ? DangerousGood.toJSON(message.dangerousGood) : undefined);
    message.package !== undefined && (obj.package = message.package);
    message.packagingCode !== undefined && (obj.packagingCode = message.packagingCode);
    message.quantity !== undefined && (obj.quantity = message.quantity);
    message.unit !== undefined && (obj.unit = message.unit);
    message.individualAmount !== undefined && (obj.individualAmount = message.individualAmount);
    message.polluting !== undefined && (obj.polluting = message.polluting);
    message.transportPoints !== undefined && (obj.transportPoints = message.transportPoints);
    message.totalAmount !== undefined && (obj.totalAmount = message.totalAmount);
    message.id !== undefined && (obj.id = message.id);
    message.deviceId !== undefined && (obj.deviceId = message.deviceId);
    message.status !== undefined && (obj.status = message.status);
    if (message.logEntries) {
      obj.logEntries = message.logEntries.map((e) => (e ? LogEntry.toJSON(e) : undefined));
    } else {
      obj.logEntries = [];
    }
    return obj;
  },

  fromPartial(object: DeepPartial<OrderPosition>): OrderPosition {
    const message = { ...baseOrderPosition } as OrderPosition;
    message.logEntries = [];
    if (object.dangerousGood !== undefined && object.dangerousGood !== null) {
      message.dangerousGood = DangerousGood.fromPartial(object.dangerousGood);
    } else {
      message.dangerousGood = undefined;
    }
    if (object.package !== undefined && object.package !== null) {
      message.package = object.package;
    } else {
      message.package = '';
    }
    if (object.packagingCode !== undefined && object.packagingCode !== null) {
      message.packagingCode = object.packagingCode;
    } else {
      message.packagingCode = '';
    }
    if (object.quantity !== undefined && object.quantity !== null) {
      message.quantity = object.quantity;
    } else {
      message.quantity = 0;
    }
    if (object.unit !== undefined && object.unit !== null) {
      message.unit = object.unit;
    } else {
      message.unit = '';
    }
    if (object.individualAmount !== undefined && object.individualAmount !== null) {
      message.individualAmount = object.individualAmount;
    } else {
      message.individualAmount = 0;
    }
    if (object.polluting !== undefined && object.polluting !== null) {
      message.polluting = object.polluting;
    } else {
      message.polluting = false;
    }
    if (object.transportPoints !== undefined && object.transportPoints !== null) {
      message.transportPoints = object.transportPoints;
    } else {
      message.transportPoints = 0;
    }
    if (object.totalAmount !== undefined && object.totalAmount !== null) {
      message.totalAmount = object.totalAmount;
    } else {
      message.totalAmount = 0;
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = 0;
    }
    if (object.deviceId !== undefined && object.deviceId !== null) {
      message.deviceId = object.deviceId;
    } else {
      message.deviceId = 0;
    }
    if (object.status !== undefined && object.status !== null) {
      message.status = object.status;
    } else {
      message.status = '';
    }
    if (object.logEntries !== undefined && object.logEntries !== null) {
      for (const e of object.logEntries) {
        message.logEntries.push(LogEntry.fromPartial(e));
      }
    }
    return message;
  },
};

const baseLogEntry: object = {
  status: '',
  date: 0,
  author: '',
  description: '',
};

export const LogEntry = {
  encode(message: LogEntry, writer: Writer = Writer.create()): Writer {
    if (message.status !== '') {
      writer.uint32(10).string(message.status);
    }
    if (message.date !== 0) {
      writer.uint32(17).double(message.date);
    }
    if (message.author !== '') {
      writer.uint32(26).string(message.author);
    }
    if (message.description !== '') {
      writer.uint32(34).string(message.description);
    }
    if (message.acceptanceCriteria !== undefined) {
      AcceptanceCriteria.encode(message.acceptanceCriteria, writer.uint32(42).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): LogEntry {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseLogEntry } as LogEntry;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.status = reader.string();
          break;
        case 2:
          message.date = reader.double();
          break;
        case 3:
          message.author = reader.string();
          break;
        case 4:
          message.description = reader.string();
          break;
        case 5:
          message.acceptanceCriteria = AcceptanceCriteria.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): LogEntry {
    const message = { ...baseLogEntry } as LogEntry;
    if (object.status !== undefined && object.status !== null) {
      message.status = String(object.status);
    } else {
      message.status = '';
    }
    if (object.date !== undefined && object.date !== null) {
      message.date = Number(object.date);
    } else {
      message.date = 0;
    }
    if (object.author !== undefined && object.author !== null) {
      message.author = String(object.author);
    } else {
      message.author = '';
    }
    if (object.description !== undefined && object.description !== null) {
      message.description = String(object.description);
    } else {
      message.description = '';
    }
    if (object.acceptanceCriteria !== undefined && object.acceptanceCriteria !== null) {
      message.acceptanceCriteria = AcceptanceCriteria.fromJSON(object.acceptanceCriteria);
    } else {
      message.acceptanceCriteria = undefined;
    }
    return message;
  },

  toJSON(message: LogEntry): unknown {
    const obj: any = {};
    message.status !== undefined && (obj.status = message.status);
    message.date !== undefined && (obj.date = message.date);
    message.author !== undefined && (obj.author = message.author);
    message.description !== undefined && (obj.description = message.description);
    message.acceptanceCriteria !== undefined &&
      (obj.acceptanceCriteria = message.acceptanceCriteria
        ? AcceptanceCriteria.toJSON(message.acceptanceCriteria)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<LogEntry>): LogEntry {
    const message = { ...baseLogEntry } as LogEntry;
    if (object.status !== undefined && object.status !== null) {
      message.status = object.status;
    } else {
      message.status = '';
    }
    if (object.date !== undefined && object.date !== null) {
      message.date = object.date;
    } else {
      message.date = 0;
    }
    if (object.author !== undefined && object.author !== null) {
      message.author = object.author;
    } else {
      message.author = '';
    }
    if (object.description !== undefined && object.description !== null) {
      message.description = object.description;
    } else {
      message.description = '';
    }
    if (object.acceptanceCriteria !== undefined && object.acceptanceCriteria !== null) {
      message.acceptanceCriteria = AcceptanceCriteria.fromPartial(object.acceptanceCriteria);
    } else {
      message.acceptanceCriteria = undefined;
    }
    return message;
  },
};

const baseDangerousGood: object = {
  unNumber: '',
  casNumber: '',
  description: '',
  label1: '',
  label2: '',
  label3: '',
  packingGroup: '',
  tunnelRestrictionCode: '',
  transportCategory: '',
};

export const DangerousGood = {
  encode(message: DangerousGood, writer: Writer = Writer.create()): Writer {
    if (message.unNumber !== '') {
      writer.uint32(10).string(message.unNumber);
    }
    if (message.casNumber !== '') {
      writer.uint32(18).string(message.casNumber);
    }
    if (message.description !== '') {
      writer.uint32(26).string(message.description);
    }
    if (message.label1 !== '') {
      writer.uint32(34).string(message.label1);
    }
    if (message.label2 !== '') {
      writer.uint32(42).string(message.label2);
    }
    if (message.label3 !== '') {
      writer.uint32(50).string(message.label3);
    }
    if (message.packingGroup !== '') {
      writer.uint32(58).string(message.packingGroup);
    }
    if (message.tunnelRestrictionCode !== '') {
      writer.uint32(66).string(message.tunnelRestrictionCode);
    }
    if (message.transportCategory !== '') {
      writer.uint32(74).string(message.transportCategory);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): DangerousGood {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseDangerousGood } as DangerousGood;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.unNumber = reader.string();
          break;
        case 2:
          message.casNumber = reader.string();
          break;
        case 3:
          message.description = reader.string();
          break;
        case 4:
          message.label1 = reader.string();
          break;
        case 5:
          message.label2 = reader.string();
          break;
        case 6:
          message.label3 = reader.string();
          break;
        case 7:
          message.packingGroup = reader.string();
          break;
        case 8:
          message.tunnelRestrictionCode = reader.string();
          break;
        case 9:
          message.transportCategory = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): DangerousGood {
    const message = { ...baseDangerousGood } as DangerousGood;
    if (object.unNumber !== undefined && object.unNumber !== null) {
      message.unNumber = String(object.unNumber);
    } else {
      message.unNumber = '';
    }
    if (object.casNumber !== undefined && object.casNumber !== null) {
      message.casNumber = String(object.casNumber);
    } else {
      message.casNumber = '';
    }
    if (object.description !== undefined && object.description !== null) {
      message.description = String(object.description);
    } else {
      message.description = '';
    }
    if (object.label1 !== undefined && object.label1 !== null) {
      message.label1 = String(object.label1);
    } else {
      message.label1 = '';
    }
    if (object.label2 !== undefined && object.label2 !== null) {
      message.label2 = String(object.label2);
    } else {
      message.label2 = '';
    }
    if (object.label3 !== undefined && object.label3 !== null) {
      message.label3 = String(object.label3);
    } else {
      message.label3 = '';
    }
    if (object.packingGroup !== undefined && object.packingGroup !== null) {
      message.packingGroup = String(object.packingGroup);
    } else {
      message.packingGroup = '';
    }
    if (object.tunnelRestrictionCode !== undefined && object.tunnelRestrictionCode !== null) {
      message.tunnelRestrictionCode = String(object.tunnelRestrictionCode);
    } else {
      message.tunnelRestrictionCode = '';
    }
    if (object.transportCategory !== undefined && object.transportCategory !== null) {
      message.transportCategory = String(object.transportCategory);
    } else {
      message.transportCategory = '';
    }
    return message;
  },

  toJSON(message: DangerousGood): unknown {
    const obj: any = {};
    message.unNumber !== undefined && (obj.unNumber = message.unNumber);
    message.casNumber !== undefined && (obj.casNumber = message.casNumber);
    message.description !== undefined && (obj.description = message.description);
    message.label1 !== undefined && (obj.label1 = message.label1);
    message.label2 !== undefined && (obj.label2 = message.label2);
    message.label3 !== undefined && (obj.label3 = message.label3);
    message.packingGroup !== undefined && (obj.packingGroup = message.packingGroup);
    message.tunnelRestrictionCode !== undefined && (obj.tunnelRestrictionCode = message.tunnelRestrictionCode);
    message.transportCategory !== undefined && (obj.transportCategory = message.transportCategory);
    return obj;
  },

  fromPartial(object: DeepPartial<DangerousGood>): DangerousGood {
    const message = { ...baseDangerousGood } as DangerousGood;
    if (object.unNumber !== undefined && object.unNumber !== null) {
      message.unNumber = object.unNumber;
    } else {
      message.unNumber = '';
    }
    if (object.casNumber !== undefined && object.casNumber !== null) {
      message.casNumber = object.casNumber;
    } else {
      message.casNumber = '';
    }
    if (object.description !== undefined && object.description !== null) {
      message.description = object.description;
    } else {
      message.description = '';
    }
    if (object.label1 !== undefined && object.label1 !== null) {
      message.label1 = object.label1;
    } else {
      message.label1 = '';
    }
    if (object.label2 !== undefined && object.label2 !== null) {
      message.label2 = object.label2;
    } else {
      message.label2 = '';
    }
    if (object.label3 !== undefined && object.label3 !== null) {
      message.label3 = object.label3;
    } else {
      message.label3 = '';
    }
    if (object.packingGroup !== undefined && object.packingGroup !== null) {
      message.packingGroup = object.packingGroup;
    } else {
      message.packingGroup = '';
    }
    if (object.tunnelRestrictionCode !== undefined && object.tunnelRestrictionCode !== null) {
      message.tunnelRestrictionCode = object.tunnelRestrictionCode;
    } else {
      message.tunnelRestrictionCode = '';
    }
    if (object.transportCategory !== undefined && object.transportCategory !== null) {
      message.transportCategory = object.transportCategory;
    } else {
      message.transportCategory = '';
    }
    return message;
  },
};

const baseCarrier: object = { name: '', driver: '', licensePlate: '' };

export const Carrier = {
  encode(message: Carrier, writer: Writer = Writer.create()): Writer {
    if (message.name !== '') {
      writer.uint32(10).string(message.name);
    }
    if (message.driver !== '') {
      writer.uint32(18).string(message.driver);
    }
    if (message.licensePlate !== '') {
      writer.uint32(26).string(message.licensePlate);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Carrier {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseCarrier } as Carrier;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.name = reader.string();
          break;
        case 2:
          message.driver = reader.string();
          break;
        case 3:
          message.licensePlate = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Carrier {
    const message = { ...baseCarrier } as Carrier;
    if (object.name !== undefined && object.name !== null) {
      message.name = String(object.name);
    } else {
      message.name = '';
    }
    if (object.driver !== undefined && object.driver !== null) {
      message.driver = String(object.driver);
    } else {
      message.driver = '';
    }
    if (object.licensePlate !== undefined && object.licensePlate !== null) {
      message.licensePlate = String(object.licensePlate);
    } else {
      message.licensePlate = '';
    }
    return message;
  },

  toJSON(message: Carrier): unknown {
    const obj: any = {};
    message.name !== undefined && (obj.name = message.name);
    message.driver !== undefined && (obj.driver = message.driver);
    message.licensePlate !== undefined && (obj.licensePlate = message.licensePlate);
    return obj;
  },

  fromPartial(object: DeepPartial<Carrier>): Carrier {
    const message = { ...baseCarrier } as Carrier;
    if (object.name !== undefined && object.name !== null) {
      message.name = object.name;
    } else {
      message.name = '';
    }
    if (object.driver !== undefined && object.driver !== null) {
      message.driver = object.driver;
    } else {
      message.driver = '';
    }
    if (object.licensePlate !== undefined && object.licensePlate !== null) {
      message.licensePlate = object.licensePlate;
    } else {
      message.licensePlate = '';
    }
    return message;
  },
};

const baseAcceptanceCriteria: object = { comment: '' };

export const AcceptanceCriteria = {
  encode(message: AcceptanceCriteria, writer: Writer = Writer.create()): Writer {
    if (message.comment !== '') {
      writer.uint32(10).string(message.comment);
    }
    if (message.carrierCheckCriteria !== undefined) {
      CarrierCheckCriteria.encode(message.carrierCheckCriteria, writer.uint32(18).fork()).ldelim();
    }
    if (message.transportVehicleCriteria !== undefined) {
      TransportVehicleCriteria.encode(message.transportVehicleCriteria, writer.uint32(26).fork()).ldelim();
    }
    if (message.visualInspectionCarrierCriteria !== undefined) {
      VisualInspectionCarrierCriteria.encode(
        message.visualInspectionCarrierCriteria,
        writer.uint32(34).fork()
      ).ldelim();
    }
    if (message.visualInspectionConsigneeCriteria !== undefined) {
      VisualInspectionConsigneeCriteria.encode(
        message.visualInspectionConsigneeCriteria,
        writer.uint32(42).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): AcceptanceCriteria {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseAcceptanceCriteria } as AcceptanceCriteria;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.comment = reader.string();
          break;
        case 2:
          message.carrierCheckCriteria = CarrierCheckCriteria.decode(reader, reader.uint32());
          break;
        case 3:
          message.transportVehicleCriteria = TransportVehicleCriteria.decode(reader, reader.uint32());
          break;
        case 4:
          message.visualInspectionCarrierCriteria = VisualInspectionCarrierCriteria.decode(reader, reader.uint32());
          break;
        case 5:
          message.visualInspectionConsigneeCriteria = VisualInspectionConsigneeCriteria.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): AcceptanceCriteria {
    const message = { ...baseAcceptanceCriteria } as AcceptanceCriteria;
    if (object.comment !== undefined && object.comment !== null) {
      message.comment = String(object.comment);
    } else {
      message.comment = '';
    }
    if (object.carrierCheckCriteria !== undefined && object.carrierCheckCriteria !== null) {
      message.carrierCheckCriteria = CarrierCheckCriteria.fromJSON(object.carrierCheckCriteria);
    } else {
      message.carrierCheckCriteria = undefined;
    }
    if (object.transportVehicleCriteria !== undefined && object.transportVehicleCriteria !== null) {
      message.transportVehicleCriteria = TransportVehicleCriteria.fromJSON(object.transportVehicleCriteria);
    } else {
      message.transportVehicleCriteria = undefined;
    }
    if (object.visualInspectionCarrierCriteria !== undefined && object.visualInspectionCarrierCriteria !== null) {
      message.visualInspectionCarrierCriteria = VisualInspectionCarrierCriteria.fromJSON(
        object.visualInspectionCarrierCriteria
      );
    } else {
      message.visualInspectionCarrierCriteria = undefined;
    }
    if (object.visualInspectionConsigneeCriteria !== undefined && object.visualInspectionConsigneeCriteria !== null) {
      message.visualInspectionConsigneeCriteria = VisualInspectionConsigneeCriteria.fromJSON(
        object.visualInspectionConsigneeCriteria
      );
    } else {
      message.visualInspectionConsigneeCriteria = undefined;
    }
    return message;
  },

  toJSON(message: AcceptanceCriteria): unknown {
    const obj: any = {};
    message.comment !== undefined && (obj.comment = message.comment);
    message.carrierCheckCriteria !== undefined &&
      (obj.carrierCheckCriteria = message.carrierCheckCriteria
        ? CarrierCheckCriteria.toJSON(message.carrierCheckCriteria)
        : undefined);
    message.transportVehicleCriteria !== undefined &&
      (obj.transportVehicleCriteria = message.transportVehicleCriteria
        ? TransportVehicleCriteria.toJSON(message.transportVehicleCriteria)
        : undefined);
    message.visualInspectionCarrierCriteria !== undefined &&
      (obj.visualInspectionCarrierCriteria = message.visualInspectionCarrierCriteria
        ? VisualInspectionCarrierCriteria.toJSON(message.visualInspectionCarrierCriteria)
        : undefined);
    message.visualInspectionConsigneeCriteria !== undefined &&
      (obj.visualInspectionConsigneeCriteria = message.visualInspectionConsigneeCriteria
        ? VisualInspectionConsigneeCriteria.toJSON(message.visualInspectionConsigneeCriteria)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<AcceptanceCriteria>): AcceptanceCriteria {
    const message = { ...baseAcceptanceCriteria } as AcceptanceCriteria;
    if (object.comment !== undefined && object.comment !== null) {
      message.comment = object.comment;
    } else {
      message.comment = '';
    }
    if (object.carrierCheckCriteria !== undefined && object.carrierCheckCriteria !== null) {
      message.carrierCheckCriteria = CarrierCheckCriteria.fromPartial(object.carrierCheckCriteria);
    } else {
      message.carrierCheckCriteria = undefined;
    }
    if (object.transportVehicleCriteria !== undefined && object.transportVehicleCriteria !== null) {
      message.transportVehicleCriteria = TransportVehicleCriteria.fromPartial(object.transportVehicleCriteria);
    } else {
      message.transportVehicleCriteria = undefined;
    }
    if (object.visualInspectionCarrierCriteria !== undefined && object.visualInspectionCarrierCriteria !== null) {
      message.visualInspectionCarrierCriteria = VisualInspectionCarrierCriteria.fromPartial(
        object.visualInspectionCarrierCriteria
      );
    } else {
      message.visualInspectionCarrierCriteria = undefined;
    }
    if (object.visualInspectionConsigneeCriteria !== undefined && object.visualInspectionConsigneeCriteria !== null) {
      message.visualInspectionConsigneeCriteria = VisualInspectionConsigneeCriteria.fromPartial(
        object.visualInspectionConsigneeCriteria
      );
    } else {
      message.visualInspectionConsigneeCriteria = undefined;
    }
    return message;
  },
};

const baseCarrierCheckCriteria: object = {
  acceptSender: false,
  acceptConsignees: false,
  acceptFreight: false,
  acceptAdditionalInformation: false,
  acceptTransportationInstructions: false,
};

export const CarrierCheckCriteria = {
  encode(message: CarrierCheckCriteria, writer: Writer = Writer.create()): Writer {
    if (message.acceptSender === true) {
      writer.uint32(8).bool(message.acceptSender);
    }
    writer.uint32(18).fork();
    for (const v of message.acceptConsignees) {
      writer.bool(v);
    }
    writer.ldelim();
    if (message.acceptFreight === true) {
      writer.uint32(24).bool(message.acceptFreight);
    }
    if (message.acceptAdditionalInformation === true) {
      writer.uint32(32).bool(message.acceptAdditionalInformation);
    }
    if (message.acceptTransportationInstructions === true) {
      writer.uint32(40).bool(message.acceptTransportationInstructions);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): CarrierCheckCriteria {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseCarrierCheckCriteria } as CarrierCheckCriteria;
    message.acceptConsignees = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.acceptSender = reader.bool();
          break;
        case 2:
          if ((tag & 7) === 2) {
            const end2 = reader.uint32() + reader.pos;
            while (reader.pos < end2) {
              message.acceptConsignees.push(reader.bool());
            }
          } else {
            message.acceptConsignees.push(reader.bool());
          }
          break;
        case 3:
          message.acceptFreight = reader.bool();
          break;
        case 4:
          message.acceptAdditionalInformation = reader.bool();
          break;
        case 5:
          message.acceptTransportationInstructions = reader.bool();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CarrierCheckCriteria {
    const message = { ...baseCarrierCheckCriteria } as CarrierCheckCriteria;
    message.acceptConsignees = [];
    if (object.acceptSender !== undefined && object.acceptSender !== null) {
      message.acceptSender = Boolean(object.acceptSender);
    } else {
      message.acceptSender = false;
    }
    if (object.acceptConsignees !== undefined && object.acceptConsignees !== null) {
      for (const e of object.acceptConsignees) {
        message.acceptConsignees.push(Boolean(e));
      }
    }
    if (object.acceptFreight !== undefined && object.acceptFreight !== null) {
      message.acceptFreight = Boolean(object.acceptFreight);
    } else {
      message.acceptFreight = false;
    }
    if (object.acceptAdditionalInformation !== undefined && object.acceptAdditionalInformation !== null) {
      message.acceptAdditionalInformation = Boolean(object.acceptAdditionalInformation);
    } else {
      message.acceptAdditionalInformation = false;
    }
    if (object.acceptTransportationInstructions !== undefined && object.acceptTransportationInstructions !== null) {
      message.acceptTransportationInstructions = Boolean(object.acceptTransportationInstructions);
    } else {
      message.acceptTransportationInstructions = false;
    }
    return message;
  },

  toJSON(message: CarrierCheckCriteria): unknown {
    const obj: any = {};
    message.acceptSender !== undefined && (obj.acceptSender = message.acceptSender);
    if (message.acceptConsignees) {
      obj.acceptConsignees = message.acceptConsignees.map((e) => e);
    } else {
      obj.acceptConsignees = [];
    }
    message.acceptFreight !== undefined && (obj.acceptFreight = message.acceptFreight);
    message.acceptAdditionalInformation !== undefined &&
      (obj.acceptAdditionalInformation = message.acceptAdditionalInformation);
    message.acceptTransportationInstructions !== undefined &&
      (obj.acceptTransportationInstructions = message.acceptTransportationInstructions);
    return obj;
  },

  fromPartial(object: DeepPartial<CarrierCheckCriteria>): CarrierCheckCriteria {
    const message = { ...baseCarrierCheckCriteria } as CarrierCheckCriteria;
    message.acceptConsignees = [];
    if (object.acceptSender !== undefined && object.acceptSender !== null) {
      message.acceptSender = object.acceptSender;
    } else {
      message.acceptSender = false;
    }
    if (object.acceptConsignees !== undefined && object.acceptConsignees !== null) {
      for (const e of object.acceptConsignees) {
        message.acceptConsignees.push(e);
      }
    }
    if (object.acceptFreight !== undefined && object.acceptFreight !== null) {
      message.acceptFreight = object.acceptFreight;
    } else {
      message.acceptFreight = false;
    }
    if (object.acceptAdditionalInformation !== undefined && object.acceptAdditionalInformation !== null) {
      message.acceptAdditionalInformation = object.acceptAdditionalInformation;
    } else {
      message.acceptAdditionalInformation = false;
    }
    if (object.acceptTransportationInstructions !== undefined && object.acceptTransportationInstructions !== null) {
      message.acceptTransportationInstructions = object.acceptTransportationInstructions;
    } else {
      message.acceptTransportationInstructions = false;
    }
    return message;
  },
};

const baseTransportVehicleCriteria: object = {
  licencePlate: '',
  acceptVehicleCondition: false,
  acceptVehicleSafetyEquipment: false,
  driverName: '',
  acceptCarrierInformation: false,
  acceptCarrierSafetyEquipment: false,
};

export const TransportVehicleCriteria = {
  encode(message: TransportVehicleCriteria, writer: Writer = Writer.create()): Writer {
    if (message.licencePlate !== '') {
      writer.uint32(10).string(message.licencePlate);
    }
    if (message.acceptVehicleCondition === true) {
      writer.uint32(16).bool(message.acceptVehicleCondition);
    }
    if (message.acceptVehicleSafetyEquipment === true) {
      writer.uint32(24).bool(message.acceptVehicleSafetyEquipment);
    }
    if (message.driverName !== '') {
      writer.uint32(34).string(message.driverName);
    }
    if (message.acceptCarrierInformation === true) {
      writer.uint32(40).bool(message.acceptCarrierInformation);
    }
    if (message.acceptCarrierSafetyEquipment === true) {
      writer.uint32(48).bool(message.acceptCarrierSafetyEquipment);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): TransportVehicleCriteria {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseTransportVehicleCriteria,
    } as TransportVehicleCriteria;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.licencePlate = reader.string();
          break;
        case 2:
          message.acceptVehicleCondition = reader.bool();
          break;
        case 3:
          message.acceptVehicleSafetyEquipment = reader.bool();
          break;
        case 4:
          message.driverName = reader.string();
          break;
        case 5:
          message.acceptCarrierInformation = reader.bool();
          break;
        case 6:
          message.acceptCarrierSafetyEquipment = reader.bool();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): TransportVehicleCriteria {
    const message = {
      ...baseTransportVehicleCriteria,
    } as TransportVehicleCriteria;
    if (object.licencePlate !== undefined && object.licencePlate !== null) {
      message.licencePlate = String(object.licencePlate);
    } else {
      message.licencePlate = '';
    }
    if (object.acceptVehicleCondition !== undefined && object.acceptVehicleCondition !== null) {
      message.acceptVehicleCondition = Boolean(object.acceptVehicleCondition);
    } else {
      message.acceptVehicleCondition = false;
    }
    if (object.acceptVehicleSafetyEquipment !== undefined && object.acceptVehicleSafetyEquipment !== null) {
      message.acceptVehicleSafetyEquipment = Boolean(object.acceptVehicleSafetyEquipment);
    } else {
      message.acceptVehicleSafetyEquipment = false;
    }
    if (object.driverName !== undefined && object.driverName !== null) {
      message.driverName = String(object.driverName);
    } else {
      message.driverName = '';
    }
    if (object.acceptCarrierInformation !== undefined && object.acceptCarrierInformation !== null) {
      message.acceptCarrierInformation = Boolean(object.acceptCarrierInformation);
    } else {
      message.acceptCarrierInformation = false;
    }
    if (object.acceptCarrierSafetyEquipment !== undefined && object.acceptCarrierSafetyEquipment !== null) {
      message.acceptCarrierSafetyEquipment = Boolean(object.acceptCarrierSafetyEquipment);
    } else {
      message.acceptCarrierSafetyEquipment = false;
    }
    return message;
  },

  toJSON(message: TransportVehicleCriteria): unknown {
    const obj: any = {};
    message.licencePlate !== undefined && (obj.licencePlate = message.licencePlate);
    message.acceptVehicleCondition !== undefined && (obj.acceptVehicleCondition = message.acceptVehicleCondition);
    message.acceptVehicleSafetyEquipment !== undefined &&
      (obj.acceptVehicleSafetyEquipment = message.acceptVehicleSafetyEquipment);
    message.driverName !== undefined && (obj.driverName = message.driverName);
    message.acceptCarrierInformation !== undefined && (obj.acceptCarrierInformation = message.acceptCarrierInformation);
    message.acceptCarrierSafetyEquipment !== undefined &&
      (obj.acceptCarrierSafetyEquipment = message.acceptCarrierSafetyEquipment);
    return obj;
  },

  fromPartial(object: DeepPartial<TransportVehicleCriteria>): TransportVehicleCriteria {
    const message = {
      ...baseTransportVehicleCriteria,
    } as TransportVehicleCriteria;
    if (object.licencePlate !== undefined && object.licencePlate !== null) {
      message.licencePlate = object.licencePlate;
    } else {
      message.licencePlate = '';
    }
    if (object.acceptVehicleCondition !== undefined && object.acceptVehicleCondition !== null) {
      message.acceptVehicleCondition = object.acceptVehicleCondition;
    } else {
      message.acceptVehicleCondition = false;
    }
    if (object.acceptVehicleSafetyEquipment !== undefined && object.acceptVehicleSafetyEquipment !== null) {
      message.acceptVehicleSafetyEquipment = object.acceptVehicleSafetyEquipment;
    } else {
      message.acceptVehicleSafetyEquipment = false;
    }
    if (object.driverName !== undefined && object.driverName !== null) {
      message.driverName = object.driverName;
    } else {
      message.driverName = '';
    }
    if (object.acceptCarrierInformation !== undefined && object.acceptCarrierInformation !== null) {
      message.acceptCarrierInformation = object.acceptCarrierInformation;
    } else {
      message.acceptCarrierInformation = false;
    }
    if (object.acceptCarrierSafetyEquipment !== undefined && object.acceptCarrierSafetyEquipment !== null) {
      message.acceptCarrierSafetyEquipment = object.acceptCarrierSafetyEquipment;
    } else {
      message.acceptCarrierSafetyEquipment = false;
    }
    return message;
  },
};

const baseVisualInspectionCarrierCriteria: object = {
  acceptTransportability: false,
  acceptLabeling: false,
};

export const VisualInspectionCarrierCriteria = {
  encode(message: VisualInspectionCarrierCriteria, writer: Writer = Writer.create()): Writer {
    if (message.acceptTransportability === true) {
      writer.uint32(8).bool(message.acceptTransportability);
    }
    if (message.acceptLabeling === true) {
      writer.uint32(16).bool(message.acceptLabeling);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): VisualInspectionCarrierCriteria {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseVisualInspectionCarrierCriteria,
    } as VisualInspectionCarrierCriteria;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.acceptTransportability = reader.bool();
          break;
        case 2:
          message.acceptLabeling = reader.bool();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): VisualInspectionCarrierCriteria {
    const message = {
      ...baseVisualInspectionCarrierCriteria,
    } as VisualInspectionCarrierCriteria;
    if (object.acceptTransportability !== undefined && object.acceptTransportability !== null) {
      message.acceptTransportability = Boolean(object.acceptTransportability);
    } else {
      message.acceptTransportability = false;
    }
    if (object.acceptLabeling !== undefined && object.acceptLabeling !== null) {
      message.acceptLabeling = Boolean(object.acceptLabeling);
    } else {
      message.acceptLabeling = false;
    }
    return message;
  },

  toJSON(message: VisualInspectionCarrierCriteria): unknown {
    const obj: any = {};
    message.acceptTransportability !== undefined && (obj.acceptTransportability = message.acceptTransportability);
    message.acceptLabeling !== undefined && (obj.acceptLabeling = message.acceptLabeling);
    return obj;
  },

  fromPartial(object: DeepPartial<VisualInspectionCarrierCriteria>): VisualInspectionCarrierCriteria {
    const message = {
      ...baseVisualInspectionCarrierCriteria,
    } as VisualInspectionCarrierCriteria;
    if (object.acceptTransportability !== undefined && object.acceptTransportability !== null) {
      message.acceptTransportability = object.acceptTransportability;
    } else {
      message.acceptTransportability = false;
    }
    if (object.acceptLabeling !== undefined && object.acceptLabeling !== null) {
      message.acceptLabeling = object.acceptLabeling;
    } else {
      message.acceptLabeling = false;
    }
    return message;
  },
};

const baseVisualInspectionConsigneeCriteria: object = {
  acceptQuantity: false,
  acceptIntactness: false,
};

export const VisualInspectionConsigneeCriteria = {
  encode(message: VisualInspectionConsigneeCriteria, writer: Writer = Writer.create()): Writer {
    if (message.acceptQuantity === true) {
      writer.uint32(8).bool(message.acceptQuantity);
    }
    if (message.acceptIntactness === true) {
      writer.uint32(16).bool(message.acceptIntactness);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): VisualInspectionConsigneeCriteria {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseVisualInspectionConsigneeCriteria,
    } as VisualInspectionConsigneeCriteria;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.acceptQuantity = reader.bool();
          break;
        case 2:
          message.acceptIntactness = reader.bool();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): VisualInspectionConsigneeCriteria {
    const message = {
      ...baseVisualInspectionConsigneeCriteria,
    } as VisualInspectionConsigneeCriteria;
    if (object.acceptQuantity !== undefined && object.acceptQuantity !== null) {
      message.acceptQuantity = Boolean(object.acceptQuantity);
    } else {
      message.acceptQuantity = false;
    }
    if (object.acceptIntactness !== undefined && object.acceptIntactness !== null) {
      message.acceptIntactness = Boolean(object.acceptIntactness);
    } else {
      message.acceptIntactness = false;
    }
    return message;
  },

  toJSON(message: VisualInspectionConsigneeCriteria): unknown {
    const obj: any = {};
    message.acceptQuantity !== undefined && (obj.acceptQuantity = message.acceptQuantity);
    message.acceptIntactness !== undefined && (obj.acceptIntactness = message.acceptIntactness);
    return obj;
  },

  fromPartial(object: DeepPartial<VisualInspectionConsigneeCriteria>): VisualInspectionConsigneeCriteria {
    const message = {
      ...baseVisualInspectionConsigneeCriteria,
    } as VisualInspectionConsigneeCriteria;
    if (object.acceptQuantity !== undefined && object.acceptQuantity !== null) {
      message.acceptQuantity = object.acceptQuantity;
    } else {
      message.acceptQuantity = false;
    }
    if (object.acceptIntactness !== undefined && object.acceptIntactness !== null) {
      message.acceptIntactness = object.acceptIntactness;
    } else {
      message.acceptIntactness = false;
    }
    return message;
  },
};

declare var self: any | undefined;
declare var window: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== 'undefined') return globalThis;
  if (typeof self !== 'undefined') return self;
  if (typeof window !== 'undefined') return window;
  if (typeof global !== 'undefined') return global;
  throw 'Unable to locate global object';
})();

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error('Value is larger than ' + Number.MAX_SAFE_INTEGER);
  }
  return long.toNumber();
}

util.Long = Long as any;
configure();
