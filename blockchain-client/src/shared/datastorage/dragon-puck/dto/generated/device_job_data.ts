/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/* eslint-disable */
import * as Long from 'long';
import { util, configure, Writer, Reader } from 'protobufjs/minimal';

export const protobufPackage =
  'git.openlogisticsfoundation.org.silicon_economy.services.dangerous.core.apps.generated.businesslogic';

export interface DeviceJobData {
  creator: string;
  id: number;
  deviceMessages: MsgDeviceData[];
}

export interface MsgDeviceData {
  creator: string;
  /** jobId is the same as the orderPositionId */
  jobId: number;
  deviceId: number;
  protocolVersion: number;
  firmwareDescriptor: FirmwareDescriptor | undefined;
  comTimestamp: number;
  lastComCause: string;
  data: Data[];
}

export interface FirmwareDescriptor {
  versionNumber: VersionNumber | undefined;
  commitHash: string;
  dirtyFlad: boolean;
}

export interface VersionNumber {
  major: number;
  minor: number;
  patch: number;
  commitCounter: number;
  releaseFlag: boolean;
}

export interface Data {
  temperature: number;
  humidity: number;
  timestamp: string;
  motionState: string;
}

const baseDeviceJobData: object = { creator: '', id: 0 };

export const DeviceJobData = {
  encode(message: DeviceJobData, writer: Writer = Writer.create()): Writer {
    if (message.creator !== '') {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== 0) {
      writer.uint32(16).uint64(message.id);
    }
    for (const v of message.deviceMessages) {
      MsgDeviceData.encode(v!, writer.uint32(26).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): DeviceJobData {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseDeviceJobData } as DeviceJobData;
    message.deviceMessages = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = longToNumber(reader.uint64() as Long);
          break;
        case 3:
          message.deviceMessages.push(MsgDeviceData.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): DeviceJobData {
    const message = { ...baseDeviceJobData } as DeviceJobData;
    message.deviceMessages = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = '';
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = Number(object.id);
    } else {
      message.id = 0;
    }
    if (object.deviceMessages !== undefined && object.deviceMessages !== null) {
      for (const e of object.deviceMessages) {
        message.deviceMessages.push(MsgDeviceData.fromJSON(e));
      }
    }
    return message;
  },

  toJSON(message: DeviceJobData): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    if (message.deviceMessages) {
      obj.deviceMessages = message.deviceMessages.map((e) => (e ? MsgDeviceData.toJSON(e) : undefined));
    } else {
      obj.deviceMessages = [];
    }
    return obj;
  },

  fromPartial(object: DeepPartial<DeviceJobData>): DeviceJobData {
    const message = { ...baseDeviceJobData } as DeviceJobData;
    message.deviceMessages = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = '';
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = 0;
    }
    if (object.deviceMessages !== undefined && object.deviceMessages !== null) {
      for (const e of object.deviceMessages) {
        message.deviceMessages.push(MsgDeviceData.fromPartial(e));
      }
    }
    return message;
  },
};

const baseMsgDeviceData: object = {
  creator: '',
  jobId: 0,
  deviceId: 0,
  protocolVersion: 0,
  comTimestamp: 0,
  lastComCause: '',
};

export const MsgDeviceData = {
  encode(message: MsgDeviceData, writer: Writer = Writer.create()): Writer {
    if (message.creator !== '') {
      writer.uint32(10).string(message.creator);
    }
    if (message.jobId !== 0) {
      writer.uint32(16).uint64(message.jobId);
    }
    if (message.deviceId !== 0) {
      writer.uint32(24).uint64(message.deviceId);
    }
    if (message.protocolVersion !== 0) {
      writer.uint32(32).uint32(message.protocolVersion);
    }
    if (message.firmwareDescriptor !== undefined) {
      FirmwareDescriptor.encode(message.firmwareDescriptor, writer.uint32(42).fork()).ldelim();
    }
    if (message.comTimestamp !== 0) {
      writer.uint32(48).uint64(message.comTimestamp);
    }
    if (message.lastComCause !== '') {
      writer.uint32(58).string(message.lastComCause);
    }
    for (const v of message.data) {
      Data.encode(v!, writer.uint32(66).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgDeviceData {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgDeviceData } as MsgDeviceData;
    message.data = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.jobId = longToNumber(reader.uint64() as Long);
          break;
        case 3:
          message.deviceId = longToNumber(reader.uint64() as Long);
          break;
        case 4:
          message.protocolVersion = reader.uint32();
          break;
        case 5:
          message.firmwareDescriptor = FirmwareDescriptor.decode(reader, reader.uint32());
          break;
        case 6:
          message.comTimestamp = longToNumber(reader.uint64() as Long);
          break;
        case 7:
          message.lastComCause = reader.string();
          break;
        case 8:
          message.data.push(Data.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgDeviceData {
    const message = { ...baseMsgDeviceData } as MsgDeviceData;
    message.data = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = '';
    }
    if (object.jobId !== undefined && object.jobId !== null) {
      message.jobId = Number(object.jobId);
    } else {
      message.jobId = 0;
    }
    if (object.deviceId !== undefined && object.deviceId !== null) {
      message.deviceId = Number(object.deviceId);
    } else {
      message.deviceId = 0;
    }
    if (object.protocolVersion !== undefined && object.protocolVersion !== null) {
      message.protocolVersion = Number(object.protocolVersion);
    } else {
      message.protocolVersion = 0;
    }
    if (object.firmwareDescriptor !== undefined && object.firmwareDescriptor !== null) {
      message.firmwareDescriptor = FirmwareDescriptor.fromJSON(object.firmwareDescriptor);
    } else {
      message.firmwareDescriptor = undefined;
    }
    if (object.comTimestamp !== undefined && object.comTimestamp !== null) {
      message.comTimestamp = Number(object.comTimestamp);
    } else {
      message.comTimestamp = 0;
    }
    if (object.lastComCause !== undefined && object.lastComCause !== null) {
      message.lastComCause = String(object.lastComCause);
    } else {
      message.lastComCause = '';
    }
    if (object.data !== undefined && object.data !== null) {
      for (const e of object.data) {
        message.data.push(Data.fromJSON(e));
      }
    }
    return message;
  },

  toJSON(message: MsgDeviceData): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.jobId !== undefined && (obj.jobId = message.jobId);
    message.deviceId !== undefined && (obj.deviceId = message.deviceId);
    message.protocolVersion !== undefined && (obj.protocolVersion = message.protocolVersion);
    message.firmwareDescriptor !== undefined &&
      (obj.firmwareDescriptor = message.firmwareDescriptor
        ? FirmwareDescriptor.toJSON(message.firmwareDescriptor)
        : undefined);
    message.comTimestamp !== undefined && (obj.comTimestamp = message.comTimestamp);
    message.lastComCause !== undefined && (obj.lastComCause = message.lastComCause);
    if (message.data) {
      obj.data = message.data.map((e) => (e ? Data.toJSON(e) : undefined));
    } else {
      obj.data = [];
    }
    return obj;
  },

  fromPartial(object: DeepPartial<MsgDeviceData>): MsgDeviceData {
    const message = { ...baseMsgDeviceData } as MsgDeviceData;
    message.data = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = '';
    }
    if (object.jobId !== undefined && object.jobId !== null) {
      message.jobId = object.jobId;
    } else {
      message.jobId = 0;
    }
    if (object.deviceId !== undefined && object.deviceId !== null) {
      message.deviceId = object.deviceId;
    } else {
      message.deviceId = 0;
    }
    if (object.protocolVersion !== undefined && object.protocolVersion !== null) {
      message.protocolVersion = object.protocolVersion;
    } else {
      message.protocolVersion = 0;
    }
    if (object.firmwareDescriptor !== undefined && object.firmwareDescriptor !== null) {
      message.firmwareDescriptor = FirmwareDescriptor.fromPartial(object.firmwareDescriptor);
    } else {
      message.firmwareDescriptor = undefined;
    }
    if (object.comTimestamp !== undefined && object.comTimestamp !== null) {
      message.comTimestamp = object.comTimestamp;
    } else {
      message.comTimestamp = 0;
    }
    if (object.lastComCause !== undefined && object.lastComCause !== null) {
      message.lastComCause = object.lastComCause;
    } else {
      message.lastComCause = '';
    }
    if (object.data !== undefined && object.data !== null) {
      for (const e of object.data) {
        message.data.push(Data.fromPartial(e));
      }
    }
    return message;
  },
};

const baseFirmwareDescriptor: object = { commitHash: '', dirtyFlad: false };

export const FirmwareDescriptor = {
  encode(message: FirmwareDescriptor, writer: Writer = Writer.create()): Writer {
    if (message.versionNumber !== undefined) {
      VersionNumber.encode(message.versionNumber, writer.uint32(10).fork()).ldelim();
    }
    if (message.commitHash !== '') {
      writer.uint32(18).string(message.commitHash);
    }
    if (message.dirtyFlad === true) {
      writer.uint32(24).bool(message.dirtyFlad);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): FirmwareDescriptor {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseFirmwareDescriptor } as FirmwareDescriptor;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.versionNumber = VersionNumber.decode(reader, reader.uint32());
          break;
        case 2:
          message.commitHash = reader.string();
          break;
        case 3:
          message.dirtyFlad = reader.bool();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): FirmwareDescriptor {
    const message = { ...baseFirmwareDescriptor } as FirmwareDescriptor;
    if (object.versionNumber !== undefined && object.versionNumber !== null) {
      message.versionNumber = VersionNumber.fromJSON(object.versionNumber);
    } else {
      message.versionNumber = undefined;
    }
    if (object.commitHash !== undefined && object.commitHash !== null) {
      message.commitHash = String(object.commitHash);
    } else {
      message.commitHash = '';
    }
    if (object.dirtyFlad !== undefined && object.dirtyFlad !== null) {
      message.dirtyFlad = Boolean(object.dirtyFlad);
    } else {
      message.dirtyFlad = false;
    }
    return message;
  },

  toJSON(message: FirmwareDescriptor): unknown {
    const obj: any = {};
    message.versionNumber !== undefined &&
      (obj.versionNumber = message.versionNumber ? VersionNumber.toJSON(message.versionNumber) : undefined);
    message.commitHash !== undefined && (obj.commitHash = message.commitHash);
    message.dirtyFlad !== undefined && (obj.dirtyFlad = message.dirtyFlad);
    return obj;
  },

  fromPartial(object: DeepPartial<FirmwareDescriptor>): FirmwareDescriptor {
    const message = { ...baseFirmwareDescriptor } as FirmwareDescriptor;
    if (object.versionNumber !== undefined && object.versionNumber !== null) {
      message.versionNumber = VersionNumber.fromPartial(object.versionNumber);
    } else {
      message.versionNumber = undefined;
    }
    if (object.commitHash !== undefined && object.commitHash !== null) {
      message.commitHash = object.commitHash;
    } else {
      message.commitHash = '';
    }
    if (object.dirtyFlad !== undefined && object.dirtyFlad !== null) {
      message.dirtyFlad = object.dirtyFlad;
    } else {
      message.dirtyFlad = false;
    }
    return message;
  },
};

const baseVersionNumber: object = { major: 0, minor: 0, patch: 0, commitCounter: 0, releaseFlag: false };

export const VersionNumber = {
  encode(message: VersionNumber, writer: Writer = Writer.create()): Writer {
    if (message.major !== 0) {
      writer.uint32(8).uint32(message.major);
    }
    if (message.minor !== 0) {
      writer.uint32(16).uint32(message.minor);
    }
    if (message.patch !== 0) {
      writer.uint32(24).uint32(message.patch);
    }
    if (message.commitCounter !== 0) {
      writer.uint32(32).uint32(message.commitCounter);
    }
    if (message.releaseFlag === true) {
      writer.uint32(40).bool(message.releaseFlag);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): VersionNumber {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseVersionNumber } as VersionNumber;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.major = reader.uint32();
          break;
        case 2:
          message.minor = reader.uint32();
          break;
        case 3:
          message.patch = reader.uint32();
          break;
        case 4:
          message.commitCounter = reader.uint32();
          break;
        case 5:
          message.releaseFlag = reader.bool();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): VersionNumber {
    const message = { ...baseVersionNumber } as VersionNumber;
    if (object.major !== undefined && object.major !== null) {
      message.major = Number(object.major);
    } else {
      message.major = 0;
    }
    if (object.minor !== undefined && object.minor !== null) {
      message.minor = Number(object.minor);
    } else {
      message.minor = 0;
    }
    if (object.patch !== undefined && object.patch !== null) {
      message.patch = Number(object.patch);
    } else {
      message.patch = 0;
    }
    if (object.commitCounter !== undefined && object.commitCounter !== null) {
      message.commitCounter = Number(object.commitCounter);
    } else {
      message.commitCounter = 0;
    }
    if (object.releaseFlag !== undefined && object.releaseFlag !== null) {
      message.releaseFlag = Boolean(object.releaseFlag);
    } else {
      message.releaseFlag = false;
    }
    return message;
  },

  toJSON(message: VersionNumber): unknown {
    const obj: any = {};
    message.major !== undefined && (obj.major = message.major);
    message.minor !== undefined && (obj.minor = message.minor);
    message.patch !== undefined && (obj.patch = message.patch);
    message.commitCounter !== undefined && (obj.commitCounter = message.commitCounter);
    message.releaseFlag !== undefined && (obj.releaseFlag = message.releaseFlag);
    return obj;
  },

  fromPartial(object: DeepPartial<VersionNumber>): VersionNumber {
    const message = { ...baseVersionNumber } as VersionNumber;
    if (object.major !== undefined && object.major !== null) {
      message.major = object.major;
    } else {
      message.major = 0;
    }
    if (object.minor !== undefined && object.minor !== null) {
      message.minor = object.minor;
    } else {
      message.minor = 0;
    }
    if (object.patch !== undefined && object.patch !== null) {
      message.patch = object.patch;
    } else {
      message.patch = 0;
    }
    if (object.commitCounter !== undefined && object.commitCounter !== null) {
      message.commitCounter = object.commitCounter;
    } else {
      message.commitCounter = 0;
    }
    if (object.releaseFlag !== undefined && object.releaseFlag !== null) {
      message.releaseFlag = object.releaseFlag;
    } else {
      message.releaseFlag = false;
    }
    return message;
  },
};

const baseData: object = { temperature: 0, humidity: 0, timestamp: '', motionState: '' };

export const Data = {
  encode(message: Data, writer: Writer = Writer.create()): Writer {
    if (message.temperature !== 0) {
      writer.uint32(13).float(message.temperature);
    }
    if (message.humidity !== 0) {
      writer.uint32(21).float(message.humidity);
    }
    if (message.timestamp !== '') {
      writer.uint32(26).string(message.timestamp);
    }
    if (message.motionState !== '') {
      writer.uint32(34).string(message.motionState);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Data {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseData } as Data;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.temperature = reader.float();
          break;
        case 2:
          message.humidity = reader.float();
          break;
        case 3:
          message.timestamp = reader.string();
          break;
        case 4:
          message.motionState = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Data {
    const message = { ...baseData } as Data;
    if (object.temperature !== undefined && object.temperature !== null) {
      message.temperature = Number(object.temperature);
    } else {
      message.temperature = 0;
    }
    if (object.humidity !== undefined && object.humidity !== null) {
      message.humidity = Number(object.humidity);
    } else {
      message.humidity = 0;
    }
    if (object.timestamp !== undefined && object.timestamp !== null) {
      message.timestamp = String(object.timestamp);
    } else {
      message.timestamp = '';
    }
    if (object.motionState !== undefined && object.motionState !== null) {
      message.motionState = String(object.motionState);
    } else {
      message.motionState = '';
    }
    return message;
  },

  toJSON(message: Data): unknown {
    const obj: any = {};
    message.temperature !== undefined && (obj.temperature = message.temperature);
    message.humidity !== undefined && (obj.humidity = message.humidity);
    message.timestamp !== undefined && (obj.timestamp = message.timestamp);
    message.motionState !== undefined && (obj.motionState = message.motionState);
    return obj;
  },

  fromPartial(object: DeepPartial<Data>): Data {
    const message = { ...baseData } as Data;
    if (object.temperature !== undefined && object.temperature !== null) {
      message.temperature = object.temperature;
    } else {
      message.temperature = 0;
    }
    if (object.humidity !== undefined && object.humidity !== null) {
      message.humidity = object.humidity;
    } else {
      message.humidity = 0;
    }
    if (object.timestamp !== undefined && object.timestamp !== null) {
      message.timestamp = object.timestamp;
    } else {
      message.timestamp = '';
    }
    if (object.motionState !== undefined && object.motionState !== null) {
      message.motionState = object.motionState;
    } else {
      message.motionState = '';
    }
    return message;
  },
};

declare var self: any | undefined;
declare var window: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== 'undefined') return globalThis;
  if (typeof self !== 'undefined') return self;
  if (typeof window !== 'undefined') return window;
  if (typeof global !== 'undefined') return global;
  throw 'Unable to locate global object';
})();

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error('Value is larger than Number.MAX_SAFE_INTEGER');
  }
  return long.toNumber();
}

if (true) {
  util.Long = Long as any;
  configure();
}
