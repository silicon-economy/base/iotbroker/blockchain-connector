/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/* eslint-disable */
import * as Long from 'long';
import { configure, Reader, util, Writer } from 'protobufjs/minimal';
import { Carrier } from './transportDocument.dto';

export const protobufPackage = 'git.openlogisticsfoundation.org.silicon_economy.services.dangerous.core.apps.generated.businesslogic';

export interface AcceptDangerousGoodRegistrationDto {
  creator: string;
  transportDocumentId: string;
  dangerousGoodRegistrationId: string;
  acceptedOrderPositionIds: number[];
  transportationInstructions: string;
  carrier: Carrier | undefined;
  newExemptionValues: BusinessLogicCalculateExemptionResponseDto | undefined;
}

export interface BusinessLogicCalculateExemptionResponseDto {
  consignees: ConsigneeResponseDto[];
  totalTransportCategoryPoints: number;
  exemptionStatus: string;
}

export interface ConsigneeResponseDto {
  responses: SingleThousandPointValueDto[];
}

export interface SingleThousandPointValueDto {
  unNumber: string;
  totalAmount: number;
  transportCategoryPoints: number;
  exemptionStatus: string;
}

const baseAcceptDangerousGoodRegistrationDto: object = {
  creator: '',
  transportDocumentId: '',
  dangerousGoodRegistrationId: '',
  acceptedOrderPositionIds: 0,
  transportationInstructions: '',
};

export const AcceptDangerousGoodRegistrationDto = {
  encode(message: AcceptDangerousGoodRegistrationDto, writer: Writer = Writer.create()): Writer {
    if (message.creator !== '') {
      writer.uint32(10).string(message.creator);
    }
    if (message.transportDocumentId !== '') {
      writer.uint32(18).string(message.transportDocumentId);
    }
    if (message.dangerousGoodRegistrationId !== '') {
      writer.uint32(26).string(message.dangerousGoodRegistrationId);
    }
    writer.uint32(42).fork();
    for (const v of message.acceptedOrderPositionIds) {
      writer.uint64(v);
    }
    writer.ldelim();
    if (message.transportationInstructions !== '') {
      writer.uint32(50).string(message.transportationInstructions);
    }
    if (message.carrier !== undefined) {
      Carrier.encode(message.carrier, writer.uint32(58).fork()).ldelim();
    }
    if (message.newExemptionValues !== undefined) {
      BusinessLogicCalculateExemptionResponseDto.encode(message.newExemptionValues, writer.uint32(66).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): AcceptDangerousGoodRegistrationDto {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseAcceptDangerousGoodRegistrationDto,
    } as AcceptDangerousGoodRegistrationDto;
    message.acceptedOrderPositionIds = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.transportDocumentId = reader.string();
          break;
        case 3:
          message.dangerousGoodRegistrationId = reader.string();
          break;
        case 5:
          if ((tag & 7) === 2) {
            const end2 = reader.uint32() + reader.pos;
            while (reader.pos < end2) {
              message.acceptedOrderPositionIds.push(longToNumber(reader.uint64() as Long));
            }
          } else {
            message.acceptedOrderPositionIds.push(longToNumber(reader.uint64() as Long));
          }
          break;
        case 6:
          message.transportationInstructions = reader.string();
          break;
        case 7:
          message.carrier = Carrier.decode(reader, reader.uint32());
          break;
        case 8:
          message.newExemptionValues = BusinessLogicCalculateExemptionResponseDto.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): AcceptDangerousGoodRegistrationDto {
    const message = {
      ...baseAcceptDangerousGoodRegistrationDto,
    } as AcceptDangerousGoodRegistrationDto;
    message.acceptedOrderPositionIds = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = '';
    }
    if (object.transportDocumentId !== undefined && object.transportDocumentId !== null) {
      message.transportDocumentId = String(object.transportDocumentId);
    } else {
      message.transportDocumentId = '';
    }
    if (object.dangerousGoodRegistrationId !== undefined && object.dangerousGoodRegistrationId !== null) {
      message.dangerousGoodRegistrationId = String(object.dangerousGoodRegistrationId);
    } else {
      message.dangerousGoodRegistrationId = '';
    }
    if (object.acceptedOrderPositionIds !== undefined && object.acceptedOrderPositionIds !== null) {
      for (const e of object.acceptedOrderPositionIds) {
        message.acceptedOrderPositionIds.push(Number(e));
      }
    }
    if (object.transportationInstructions !== undefined && object.transportationInstructions !== null) {
      message.transportationInstructions = String(object.transportationInstructions);
    } else {
      message.transportationInstructions = '';
    }
    if (object.carrier !== undefined && object.carrier !== null) {
      message.carrier = Carrier.fromJSON(object.carrier);
    } else {
      message.carrier = undefined;
    }
    if (object.newExemptionValues !== undefined && object.newExemptionValues !== null) {
      message.newExemptionValues = BusinessLogicCalculateExemptionResponseDto.fromJSON(object.newExemptionValues);
    } else {
      message.newExemptionValues = undefined;
    }
    return message;
  },

  toJSON(message: AcceptDangerousGoodRegistrationDto): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.transportDocumentId !== undefined && (obj.transportDocumentId = message.transportDocumentId);
    message.dangerousGoodRegistrationId !== undefined &&
      (obj.dangerousGoodRegistrationId = message.dangerousGoodRegistrationId);
    if (message.acceptedOrderPositionIds) {
      obj.acceptedOrderPositionIds = message.acceptedOrderPositionIds.map((e) => e);
    } else {
      obj.acceptedOrderPositionIds = [];
    }
    message.transportationInstructions !== undefined &&
      (obj.transportationInstructions = message.transportationInstructions);
    message.carrier !== undefined && (obj.carrier = message.carrier ? Carrier.toJSON(message.carrier) : undefined);
    message.newExemptionValues !== undefined &&
      (obj.newExemptionValues = message.newExemptionValues
        ? BusinessLogicCalculateExemptionResponseDto.toJSON(message.newExemptionValues)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<AcceptDangerousGoodRegistrationDto>): AcceptDangerousGoodRegistrationDto {
    const message = {
      ...baseAcceptDangerousGoodRegistrationDto,
    } as AcceptDangerousGoodRegistrationDto;
    message.acceptedOrderPositionIds = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = '';
    }
    if (object.transportDocumentId !== undefined && object.transportDocumentId !== null) {
      message.transportDocumentId = object.transportDocumentId;
    } else {
      message.transportDocumentId = '';
    }
    if (object.dangerousGoodRegistrationId !== undefined && object.dangerousGoodRegistrationId !== null) {
      message.dangerousGoodRegistrationId = object.dangerousGoodRegistrationId;
    } else {
      message.dangerousGoodRegistrationId = '';
    }
    if (object.acceptedOrderPositionIds !== undefined && object.acceptedOrderPositionIds !== null) {
      for (const e of object.acceptedOrderPositionIds) {
        message.acceptedOrderPositionIds.push(e);
      }
    }
    if (object.transportationInstructions !== undefined && object.transportationInstructions !== null) {
      message.transportationInstructions = object.transportationInstructions;
    } else {
      message.transportationInstructions = '';
    }
    if (object.carrier !== undefined && object.carrier !== null) {
      message.carrier = Carrier.fromPartial(object.carrier);
    } else {
      message.carrier = undefined;
    }
    if (object.newExemptionValues !== undefined && object.newExemptionValues !== null) {
      message.newExemptionValues = BusinessLogicCalculateExemptionResponseDto.fromPartial(object.newExemptionValues);
    } else {
      message.newExemptionValues = undefined;
    }
    return message;
  },
};

const baseBusinessLogicCalculateExemptionResponseDto: object = {
  totalTransportCategoryPoints: 0,
  exemptionStatus: '',
};

export const BusinessLogicCalculateExemptionResponseDto = {
  encode(message: BusinessLogicCalculateExemptionResponseDto, writer: Writer = Writer.create()): Writer {
    for (const v of message.consignees) {
      ConsigneeResponseDto.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.totalTransportCategoryPoints !== 0) {
      writer.uint32(17).double(message.totalTransportCategoryPoints);
    }
    if (message.exemptionStatus !== '') {
      writer.uint32(26).string(message.exemptionStatus);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): BusinessLogicCalculateExemptionResponseDto {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseBusinessLogicCalculateExemptionResponseDto,
    } as BusinessLogicCalculateExemptionResponseDto;
    message.consignees = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.consignees.push(ConsigneeResponseDto.decode(reader, reader.uint32()));
          break;
        case 2:
          message.totalTransportCategoryPoints = reader.double();
          break;
        case 3:
          message.exemptionStatus = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): BusinessLogicCalculateExemptionResponseDto {
    const message = {
      ...baseBusinessLogicCalculateExemptionResponseDto,
    } as BusinessLogicCalculateExemptionResponseDto;
    message.consignees = [];
    if (object.consignees !== undefined && object.consignees !== null) {
      for (const e of object.consignees) {
        message.consignees.push(ConsigneeResponseDto.fromJSON(e));
      }
    }
    if (object.totalTransportCategoryPoints !== undefined && object.totalTransportCategoryPoints !== null) {
      message.totalTransportCategoryPoints = Number(object.totalTransportCategoryPoints);
    } else {
      message.totalTransportCategoryPoints = 0;
    }
    if (object.exemptionStatus !== undefined && object.exemptionStatus !== null) {
      message.exemptionStatus = String(object.exemptionStatus);
    } else {
      message.exemptionStatus = '';
    }
    return message;
  },

  toJSON(message: BusinessLogicCalculateExemptionResponseDto): unknown {
    const obj: any = {};
    if (message.consignees) {
      obj.consignees = message.consignees.map((e) => (e ? ConsigneeResponseDto.toJSON(e) : undefined));
    } else {
      obj.consignees = [];
    }
    message.totalTransportCategoryPoints !== undefined &&
      (obj.totalTransportCategoryPoints = message.totalTransportCategoryPoints);
    message.exemptionStatus !== undefined && (obj.exemptionStatus = message.exemptionStatus);
    return obj;
  },

  fromPartial(
    object: DeepPartial<BusinessLogicCalculateExemptionResponseDto>
  ): BusinessLogicCalculateExemptionResponseDto {
    const message = {
      ...baseBusinessLogicCalculateExemptionResponseDto,
    } as BusinessLogicCalculateExemptionResponseDto;
    message.consignees = [];
    if (object.consignees !== undefined && object.consignees !== null) {
      for (const e of object.consignees) {
        message.consignees.push(ConsigneeResponseDto.fromPartial(e));
      }
    }
    if (object.totalTransportCategoryPoints !== undefined && object.totalTransportCategoryPoints !== null) {
      message.totalTransportCategoryPoints = object.totalTransportCategoryPoints;
    } else {
      message.totalTransportCategoryPoints = 0;
    }
    if (object.exemptionStatus !== undefined && object.exemptionStatus !== null) {
      message.exemptionStatus = object.exemptionStatus;
    } else {
      message.exemptionStatus = '';
    }
    return message;
  },
};

const baseConsigneeResponseDto: object = {};

export const ConsigneeResponseDto = {
  encode(message: ConsigneeResponseDto, writer: Writer = Writer.create()): Writer {
    for (const v of message.responses) {
      SingleThousandPointValueDto.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): ConsigneeResponseDto {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseConsigneeResponseDto } as ConsigneeResponseDto;
    message.responses = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.responses.push(SingleThousandPointValueDto.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ConsigneeResponseDto {
    const message = { ...baseConsigneeResponseDto } as ConsigneeResponseDto;
    message.responses = [];
    if (object.responses !== undefined && object.responses !== null) {
      for (const e of object.responses) {
        message.responses.push(SingleThousandPointValueDto.fromJSON(e));
      }
    }
    return message;
  },

  toJSON(message: ConsigneeResponseDto): unknown {
    const obj: any = {};
    if (message.responses) {
      obj.responses = message.responses.map((e) => (e ? SingleThousandPointValueDto.toJSON(e) : undefined));
    } else {
      obj.responses = [];
    }
    return obj;
  },

  fromPartial(object: DeepPartial<ConsigneeResponseDto>): ConsigneeResponseDto {
    const message = { ...baseConsigneeResponseDto } as ConsigneeResponseDto;
    message.responses = [];
    if (object.responses !== undefined && object.responses !== null) {
      for (const e of object.responses) {
        message.responses.push(SingleThousandPointValueDto.fromPartial(e));
      }
    }
    return message;
  },
};

const baseSingleThousandPointValueDto: object = {
  unNumber: '',
  totalAmount: 0,
  transportCategoryPoints: 0,
  exemptionStatus: '',
};

export const SingleThousandPointValueDto = {
  encode(message: SingleThousandPointValueDto, writer: Writer = Writer.create()): Writer {
    if (message.unNumber !== '') {
      writer.uint32(10).string(message.unNumber);
    }
    if (message.totalAmount !== 0) {
      writer.uint32(17).double(message.totalAmount);
    }
    if (message.transportCategoryPoints !== 0) {
      writer.uint32(25).double(message.transportCategoryPoints);
    }
    if (message.exemptionStatus !== '') {
      writer.uint32(34).string(message.exemptionStatus);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): SingleThousandPointValueDto {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseSingleThousandPointValueDto,
    } as SingleThousandPointValueDto;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.unNumber = reader.string();
          break;
        case 2:
          message.totalAmount = reader.double();
          break;
        case 3:
          message.transportCategoryPoints = reader.double();
          break;
        case 4:
          message.exemptionStatus = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): SingleThousandPointValueDto {
    const message = {
      ...baseSingleThousandPointValueDto,
    } as SingleThousandPointValueDto;
    if (object.unNumber !== undefined && object.unNumber !== null) {
      message.unNumber = String(object.unNumber);
    } else {
      message.unNumber = '';
    }
    if (object.totalAmount !== undefined && object.totalAmount !== null) {
      message.totalAmount = Number(object.totalAmount);
    } else {
      message.totalAmount = 0;
    }
    if (object.transportCategoryPoints !== undefined && object.transportCategoryPoints !== null) {
      message.transportCategoryPoints = Number(object.transportCategoryPoints);
    } else {
      message.transportCategoryPoints = 0;
    }
    if (object.exemptionStatus !== undefined && object.exemptionStatus !== null) {
      message.exemptionStatus = String(object.exemptionStatus);
    } else {
      message.exemptionStatus = '';
    }
    return message;
  },

  toJSON(message: SingleThousandPointValueDto): unknown {
    const obj: any = {};
    message.unNumber !== undefined && (obj.unNumber = message.unNumber);
    message.totalAmount !== undefined && (obj.totalAmount = message.totalAmount);
    message.transportCategoryPoints !== undefined && (obj.transportCategoryPoints = message.transportCategoryPoints);
    message.exemptionStatus !== undefined && (obj.exemptionStatus = message.exemptionStatus);
    return obj;
  },

  fromPartial(object: DeepPartial<SingleThousandPointValueDto>): SingleThousandPointValueDto {
    const message = {
      ...baseSingleThousandPointValueDto,
    } as SingleThousandPointValueDto;
    if (object.unNumber !== undefined && object.unNumber !== null) {
      message.unNumber = object.unNumber;
    } else {
      message.unNumber = '';
    }
    if (object.totalAmount !== undefined && object.totalAmount !== null) {
      message.totalAmount = object.totalAmount;
    } else {
      message.totalAmount = 0;
    }
    if (object.transportCategoryPoints !== undefined && object.transportCategoryPoints !== null) {
      message.transportCategoryPoints = object.transportCategoryPoints;
    } else {
      message.transportCategoryPoints = 0;
    }
    if (object.exemptionStatus !== undefined && object.exemptionStatus !== null) {
      message.exemptionStatus = object.exemptionStatus;
    } else {
      message.exemptionStatus = '';
    }
    return message;
  },
};

declare var self: any | undefined;
declare var window: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== 'undefined') return globalThis;
  if (typeof self !== 'undefined') return self;
  if (typeof window !== 'undefined') return window;
  if (typeof global !== 'undefined') return global;
  throw 'Unable to locate global object';
})();

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error('Value is larger than ' + Number.MAX_SAFE_INTEGER);
  }
  return long.toNumber();
}

util.Long = Long as any;
configure();
