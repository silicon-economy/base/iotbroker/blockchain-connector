/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/* eslint-disable */
import * as Long from 'long';
import { util, configure, Writer, Reader } from 'protobufjs/minimal';

export const protobufPackage =
  'git.openlogisticsfoundation.org.silicon_economy.services.dangerous.core.apps.generated.businesslogic';

export interface MsgOrderVisualInspectionConsigneeDto {
  orderId: string;
  orderPositions: MsgOrderPositionVisualInspectionConsigneeDto[];
}

export interface MsgOrderPositionVisualInspectionConsigneeDto {
  orderPositionId: number;
  acceptQuantity: boolean;
  acceptIntactness: boolean;
  comment: string;
}

const baseMsgOrderVisualInspectionConsigneeDto: object = { orderId: '' };

export const MsgOrderVisualInspectionConsigneeDto = {
  encode(message: MsgOrderVisualInspectionConsigneeDto, writer: Writer = Writer.create()): Writer {
    if (message.orderId !== '') {
      writer.uint32(10).string(message.orderId);
    }
    for (const v of message.orderPositions) {
      MsgOrderPositionVisualInspectionConsigneeDto.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgOrderVisualInspectionConsigneeDto {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgOrderVisualInspectionConsigneeDto } as MsgOrderVisualInspectionConsigneeDto;
    message.orderPositions = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.orderId = reader.string();
          break;
        case 2:
          message.orderPositions.push(MsgOrderPositionVisualInspectionConsigneeDto.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgOrderVisualInspectionConsigneeDto {
    const message = { ...baseMsgOrderVisualInspectionConsigneeDto } as MsgOrderVisualInspectionConsigneeDto;
    message.orderPositions = [];
    if (object.orderId !== undefined && object.orderId !== null) {
      message.orderId = String(object.orderId);
    } else {
      message.orderId = '';
    }
    if (object.orderPositions !== undefined && object.orderPositions !== null) {
      for (const e of object.orderPositions) {
        message.orderPositions.push(MsgOrderPositionVisualInspectionConsigneeDto.fromJSON(e));
      }
    }
    return message;
  },

  toJSON(message: MsgOrderVisualInspectionConsigneeDto): unknown {
    const obj: any = {};
    message.orderId !== undefined && (obj.orderId = message.orderId);
    if (message.orderPositions) {
      obj.orderPositions = message.orderPositions.map((e) =>
        e ? MsgOrderPositionVisualInspectionConsigneeDto.toJSON(e) : undefined
      );
    } else {
      obj.orderPositions = [];
    }
    return obj;
  },

  fromPartial(object: DeepPartial<MsgOrderVisualInspectionConsigneeDto>): MsgOrderVisualInspectionConsigneeDto {
    const message = { ...baseMsgOrderVisualInspectionConsigneeDto } as MsgOrderVisualInspectionConsigneeDto;
    message.orderPositions = [];
    if (object.orderId !== undefined && object.orderId !== null) {
      message.orderId = object.orderId;
    } else {
      message.orderId = '';
    }
    if (object.orderPositions !== undefined && object.orderPositions !== null) {
      for (const e of object.orderPositions) {
        message.orderPositions.push(MsgOrderPositionVisualInspectionConsigneeDto.fromPartial(e));
      }
    }
    return message;
  },
};

const baseMsgOrderPositionVisualInspectionConsigneeDto: object = {
  orderPositionId: 0,
  acceptQuantity: false,
  acceptIntactness: false,
  comment: '',
};

export const MsgOrderPositionVisualInspectionConsigneeDto = {
  encode(message: MsgOrderPositionVisualInspectionConsigneeDto, writer: Writer = Writer.create()): Writer {
    if (message.orderPositionId !== 0) {
      writer.uint32(8).uint64(message.orderPositionId);
    }
    if (message.acceptQuantity === true) {
      writer.uint32(16).bool(message.acceptQuantity);
    }
    if (message.acceptIntactness === true) {
      writer.uint32(24).bool(message.acceptIntactness);
    }
    if (message.comment !== '') {
      writer.uint32(34).string(message.comment);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgOrderPositionVisualInspectionConsigneeDto {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgOrderPositionVisualInspectionConsigneeDto,
    } as MsgOrderPositionVisualInspectionConsigneeDto;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.orderPositionId = longToNumber(reader.uint64() as Long);
          break;
        case 2:
          message.acceptQuantity = reader.bool();
          break;
        case 3:
          message.acceptIntactness = reader.bool();
          break;
        case 4:
          message.comment = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgOrderPositionVisualInspectionConsigneeDto {
    const message = {
      ...baseMsgOrderPositionVisualInspectionConsigneeDto,
    } as MsgOrderPositionVisualInspectionConsigneeDto;
    if (object.orderPositionId !== undefined && object.orderPositionId !== null) {
      message.orderPositionId = Number(object.orderPositionId);
    } else {
      message.orderPositionId = 0;
    }
    if (object.acceptQuantity !== undefined && object.acceptQuantity !== null) {
      message.acceptQuantity = Boolean(object.acceptQuantity);
    } else {
      message.acceptQuantity = false;
    }
    if (object.acceptIntactness !== undefined && object.acceptIntactness !== null) {
      message.acceptIntactness = Boolean(object.acceptIntactness);
    } else {
      message.acceptIntactness = false;
    }
    if (object.comment !== undefined && object.comment !== null) {
      message.comment = String(object.comment);
    } else {
      message.comment = '';
    }
    return message;
  },

  toJSON(message: MsgOrderPositionVisualInspectionConsigneeDto): unknown {
    const obj: any = {};
    message.orderPositionId !== undefined && (obj.orderPositionId = message.orderPositionId);
    message.acceptQuantity !== undefined && (obj.acceptQuantity = message.acceptQuantity);
    message.acceptIntactness !== undefined && (obj.acceptIntactness = message.acceptIntactness);
    message.comment !== undefined && (obj.comment = message.comment);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgOrderPositionVisualInspectionConsigneeDto>
  ): MsgOrderPositionVisualInspectionConsigneeDto {
    const message = {
      ...baseMsgOrderPositionVisualInspectionConsigneeDto,
    } as MsgOrderPositionVisualInspectionConsigneeDto;
    if (object.orderPositionId !== undefined && object.orderPositionId !== null) {
      message.orderPositionId = object.orderPositionId;
    } else {
      message.orderPositionId = 0;
    }
    if (object.acceptQuantity !== undefined && object.acceptQuantity !== null) {
      message.acceptQuantity = object.acceptQuantity;
    } else {
      message.acceptQuantity = false;
    }
    if (object.acceptIntactness !== undefined && object.acceptIntactness !== null) {
      message.acceptIntactness = object.acceptIntactness;
    } else {
      message.acceptIntactness = false;
    }
    if (object.comment !== undefined && object.comment !== null) {
      message.comment = object.comment;
    } else {
      message.comment = '';
    }
    return message;
  },
};

declare var self: any | undefined;
declare var window: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== 'undefined') return globalThis;
  if (typeof self !== 'undefined') return self;
  if (typeof window !== 'undefined') return window;
  if (typeof global !== 'undefined') return global;
  throw 'Unable to locate global object';
})();

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error('Value is larger than Number.MAX_SAFE_INTEGER');
  }
  return long.toNumber();
}

if (true) {
  util.Long = Long as any;
  configure();
}
