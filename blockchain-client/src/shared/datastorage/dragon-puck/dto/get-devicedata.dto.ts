/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
/* istanbul ignore file */
import { DragonFirmwareDescriptor } from './upload-devicedata.dto';

export class GetDragonJobDto {
  deviceData: JobDto[];
}

export class JobDto {
  creator: string;
  jobId: number;
  deviceId: string;
  protocolVersion: number;
  firmwareDescriptor: DragonFirmwareDescriptor | undefined;
  comTimestamp: string;
  lastComCause: string;
  data: any[];
}
