/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
/* istanbul ignore file */
import { DeviceData } from 'src/dto/publish.dto';
import { MsgUploadDeviceData } from "./generated/tx";

export class DragonDeviceData implements MsgUploadDeviceData {
  creator: string;
  jobId: number;
  deviceId: number;
  protocolVersion: number;
  firmwareDescriptor: DragonFirmwareDescriptor | undefined;
  comTimestamp: number;
  lastComCause: string;
  data: Data[];

  constructor(
    job: DeviceData,
    firmwareDescriptor: DragonFirmwareDescriptor | undefined,
  ) {
    this.jobId = job.jobId;
    this.deviceId = job.deviceId;
    this.protocolVersion = job.protocolVersion;
    this.firmwareDescriptor = firmwareDescriptor;
    this.comTimestamp = Date.parse(job.comTimestamp);
    this.lastComCause = job.lastComCause;
    this.data = new Array<Data>();
  }
}

class VersionNumber {
  major: number;
  minor: number;
  patch: number;
  commitCounter: number;
  releaseFlag: boolean;

  constructor(job: DeviceData) {
    let versionSplitted = job.firmwareDescriptor.versionNumber.split('.');
    this.major = Number(versionSplitted[0]);
    this.minor = Number(versionSplitted[1]);
    this.patch = Number(versionSplitted[2]);
    this.commitCounter = job.firmwareDescriptor.commitCounter;
    this.releaseFlag = job.firmwareDescriptor.releaseFlag;
  }
}

export class DragonFirmwareDescriptor {
  versionNumber: VersionNumber | undefined;
  commitHash: string;
  dirtyFlad: boolean;

  constructor(job: DeviceData) {
    this.versionNumber = new VersionNumber(job);
    this.commitHash = String(job.firmwareDescriptor.commitHash);
    this.dirtyFlad = job.firmwareDescriptor.dirtyFlag;
  }
}

export class Data {
  temperature: number;
  humidity: number;
  timestamp: string;
  motionState: string;

  constructor(
    temperature: number,
    humidity: number,
    timestamp: string,
    motionState: string,
  ) {
    this.temperature = temperature;
    this.humidity = humidity;
    this.timestamp = timestamp;
    this.motionState = motionState;
  }
}
