/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
import { Job } from '../../dto/job.dto';
import { DeviceData } from '../../dto/publish.dto';

export interface BCConnector {
  publishDeviceData: (job: DeviceData) => Promise<void>;
  readJob: (jobId: number) => Promise<Job>;
}
