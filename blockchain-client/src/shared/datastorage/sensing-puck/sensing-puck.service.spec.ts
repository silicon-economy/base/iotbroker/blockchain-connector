/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
import { BroadcastTxSuccess } from '@cosmjs/stargate';
import { HttpModule } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';

import { Job, Payload } from '../../../dto/job.dto';
import { DeviceData } from '../../../dto/publish.dto';
import tendermintConfig from '../../../settings/tendermint.config';
import { TendermintService } from '../../tendermint/tendermint.service';
import { GetSensingJobDto } from './dto/get-tendermint.dto';
import { SensingPuckService } from './sensing-puck.service';

describe('SensingPuckService', () => {
  let service: SensingPuckService;
  let configService: ConfigService;
  let devicedata: DeviceData;
  let jobChainDto: GetSensingJobDto;
  let jobRetVal: Job;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        HttpModule,
        ConfigModule.forRoot({
          isGlobal: true,
          envFilePath: ['.env', '.env.tendermint'],
          load: [tendermintConfig],
        }),
      ],
      providers: [SensingPuckService, TendermintService],
    }).compile();

    devicedata = new DeviceData(
      12,
      0,
      42,
      undefined,
      '2020-08-27 12:27:13',
      'Info',
      [{ String: 'data' }],
    );
    jobRetVal = new Job(0, '42', [
      new Payload('2020-08-27 12:27:13', 'Info', [{ String: 'data' }]),
    ]);
    jobChainDto = {
      Job: {
        id: 0,
        creator: 'A3425FE234',
        jobId: 0,
        deviceId: '42',
        payloads: [
          {
            creator: 'A3425FE234',
            comTimestamp: '2020-08-27 12:27:13',
            lastComCause: 'Info',
            data: [{ String: 'data' }],
          },
        ],
      },
    };
    configService = module.get<ConfigService>(ConfigService);
    service = module.get<SensingPuckService>(SensingPuckService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('publishJob', () => {
    it('should publish job-object to blockchain', async () => {
      let cosmosSucc: BroadcastTxSuccess = {
        height: 3,
        transactionHash: '5AFE54632',
        gasUsed: 500,
        gasWanted: 300,
      };
      jest
        .spyOn(TendermintService.prototype as any, 'writeToChain')
        .mockResolvedValue(cosmosSucc);

      await expect(
        service.publishDeviceData(devicedata),
      ).resolves.not.toThrow();
    });
  });

  describe('readJob', () => {
    it('should return job-object from blockchain', async () => {
      jest
        .spyOn(TendermintService.prototype as any, 'readFromChain')
        .mockResolvedValue(jobChainDto);

      expect(await service.readJob(devicedata.jobId)).toStrictEqual(jobRetVal);
    });
  });
});
