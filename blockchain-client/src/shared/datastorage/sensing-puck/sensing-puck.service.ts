/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

import { Job, Payload } from '../../../dto/job.dto';
import { DeviceData } from '../../../dto/publish.dto';
import {
  Sensing_Identifier,
  TendermintConfig,
} from '../../../settings/tendermint.config';
import { TendermintService } from '../../tendermint/tendermint.service';
import { BCConnector } from '../bc-connector';
import { SensingDeviceData } from './dto/create-job.dto';
import { MsgCreateJob } from './dto/generated/sensing.tx.dto';
import { GetSensingJobDto } from './dto/get-tendermint.dto';

@Injectable()
export class SensingPuckService implements BCConnector {
  private logger = new Logger(SensingPuckService.name);
  private sensingConfig: TendermintConfig;

  constructor(
    private tendermint: TendermintService,
    private configService: ConfigService,
  ) {
    this.sensingConfig =
      this.configService.get<TendermintConfig>(Sensing_Identifier);
  }

  async publishDeviceData(job: DeviceData): Promise<void> {
    try {
      const msgType: string = this.sensingConfig.createJobMsgType;
      const sensingPublishJob: SensingDeviceData = this.createMsgJobEvent(job);

      await this.tendermint.writeToChain(
        msgType,
        sensingPublishJob,
        this.sensingConfig,
        this.createCosmosTypes(),
      );
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }

  async readJob(jobId: number): Promise<Job> {
    try {
      const responseDto: GetSensingJobDto = await this.tendermint.readFromChain(
        `${this.sensingConfig.restGetJob}${jobId}`,
        this.sensingConfig,
      );

      const payloads: Payload[] = this.createPayloadFromBCResponse(responseDto);
      return new Job(responseDto.Job.jobId, responseDto.Job.deviceId, payloads);
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }

  private createMsgJobEvent(job: DeviceData): SensingDeviceData {
    const sensingPublishJob: SensingDeviceData = new SensingDeviceData(
      job.jobId,
      String(job.deviceId),
      job.comTimestamp,
      job.lastComCause,
      [],
    );
    job.data.forEach((element, index: number) => {
      sensingPublishJob.data[index] = JSON.stringify(element);
    });

    return sensingPublishJob;
  }

  private createPayloadFromBCResponse(
    responseDto: GetSensingJobDto,
  ): Payload[] {
    const payloads: Payload[] = [];
    responseDto.Job.payloads.forEach((payload) => {
      payload.data.forEach((element, index: number) => {
        try {
          payload.data[index] = JSON.parse(element);
        } catch (e) {
          payload.data[index] = element;
        }
      });
      payloads.push(
        new Payload(payload.comTimestamp, payload.lastComCause, payload.data),
      );
    });
    return payloads;
  }

  private createCosmosTypes(): any[][] {
    return [
      [
        `${this.sensingConfig.chainType}.${this.sensingConfig.createJobMsgType}`,
        MsgCreateJob,
      ],
    ];
  }
}
