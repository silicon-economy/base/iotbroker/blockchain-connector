/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

import { TendermintModule } from '../../tendermint/tendermint.module';
import { SensingPuckService } from './sensing-puck.service';

@Module({
  imports: [TendermintModule],
  controllers: [],
  providers: [SensingPuckService, ConfigService],
  exports: [SensingPuckService],
})
export class SensingPuckModule {}
