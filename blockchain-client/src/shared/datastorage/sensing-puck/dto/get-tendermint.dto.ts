/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
/* istanbul ignore file */
export class GetSensingJobDto {
  Job: JobDto;
}
export class JobDto {
  id: number;
  creator: string;
  jobId: number;
  deviceId: string;
  payloads: PayloadDto[];
}

export class PayloadDto {
  creator: string;
  comTimestamp: string;
  lastComCause: string;
  data: any[];
}
