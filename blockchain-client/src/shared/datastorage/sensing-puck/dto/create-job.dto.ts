/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
/* istanbul ignore file */
import { MsgCreateJob } from './generated/sensing.tx.dto';

export class SensingDeviceData implements MsgCreateJob {
  creator: string;
  jobId: number;
  deviceId: string;
  comTimestamp: string;
  lastComCause: string;
  data: string[];

  constructor(
    jobId: number,
    deviceId: string,
    comTimeStamp: string,
    lastComCause: string,
    data: string[],
  ) {
    this.jobId = jobId;
    this.deviceId = deviceId;
    this.comTimestamp = comTimeStamp;
    this.lastComCause = lastComCause;
    this.data = data;
  }
}
