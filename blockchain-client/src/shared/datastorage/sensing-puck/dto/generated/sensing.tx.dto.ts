/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
/* eslint-disable */
/* istanbul ignore file */
import { Reader, util, configure, Writer } from 'protobufjs/minimal';
import * as Long from 'long';

export const protobufPackage = 'iotbroker.devicechain.devicechain';

/** this line is used by starport scaffolding # proto/tx/message */
export interface MsgCreateJob {
  creator: string;
  jobId: number;
  deviceId: string;
  comTimestamp: string;
  lastComCause: string;
  data: string[];
}

export interface MsgCreateJobResponse {
  id: number;
}

export interface MsgUpdateJob {
  creator: string;
  id: number;
  jobId: number;
  deviceId: string;
  comTimestamp: string;
  lastComCause: string;
  data: string[];
}

export interface MsgUpdateJobResponse {}

export interface MsgDeleteJob {
  creator: string;
  id: number;
}

export interface MsgDeleteJobResponse {}

const baseMsgCreateJob: object = {
  creator: '',
  jobId: 0,
  deviceId: '',
  comTimestamp: '',
  lastComCause: '',
  data: '',
};

export const MsgCreateJob = {
  encode(message: MsgCreateJob, writer: Writer = Writer.create()): Writer {
    if (message.creator !== '') {
      writer.uint32(10).string(message.creator);
    }
    if (message.jobId !== 0) {
      writer.uint32(16).uint64(message.jobId);
    }
    if (message.deviceId !== '') {
      writer.uint32(26).string(message.deviceId);
    }
    if (message.comTimestamp !== '') {
      writer.uint32(34).string(message.comTimestamp);
    }
    if (message.lastComCause !== '') {
      writer.uint32(42).string(message.lastComCause);
    }
    for (const v of message.data) {
      writer.uint32(50).string(v!);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgCreateJob {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgCreateJob } as MsgCreateJob;
    message.data = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.jobId = longToNumber(reader.uint64() as Long);
          break;
        case 3:
          message.deviceId = reader.string();
          break;
        case 4:
          message.comTimestamp = reader.string();
          break;
        case 5:
          message.lastComCause = reader.string();
          break;
        case 6:
          message.data.push(reader.string());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateJob {
    const message = { ...baseMsgCreateJob } as MsgCreateJob;
    message.data = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = '';
    }
    if (object.jobId !== undefined && object.jobId !== null) {
      message.jobId = Number(object.jobId);
    } else {
      message.jobId = 0;
    }
    if (object.deviceId !== undefined && object.deviceId !== null) {
      message.deviceId = String(object.deviceId);
    } else {
      message.deviceId = '';
    }
    if (object.comTimestamp !== undefined && object.comTimestamp !== null) {
      message.comTimestamp = String(object.comTimestamp);
    } else {
      message.comTimestamp = '';
    }
    if (object.lastComCause !== undefined && object.lastComCause !== null) {
      message.lastComCause = String(object.lastComCause);
    } else {
      message.lastComCause = '';
    }
    if (object.data !== undefined && object.data !== null) {
      for (const e of object.data) {
        message.data.push(String(e));
      }
    }
    return message;
  },

  toJSON(message: MsgCreateJob): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.jobId !== undefined && (obj.jobId = message.jobId);
    message.deviceId !== undefined && (obj.deviceId = message.deviceId);
    message.comTimestamp !== undefined &&
      (obj.comTimestamp = message.comTimestamp);
    message.lastComCause !== undefined &&
      (obj.lastComCause = message.lastComCause);
    if (message.data) {
      obj.data = message.data.map((e) => e);
    } else {
      obj.data = [];
    }
    return obj;
  },

  fromPartial(object: DeepPartial<MsgCreateJob>): MsgCreateJob {
    const message = { ...baseMsgCreateJob } as MsgCreateJob;
    message.data = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = '';
    }
    if (object.jobId !== undefined && object.jobId !== null) {
      message.jobId = object.jobId;
    } else {
      message.jobId = 0;
    }
    if (object.deviceId !== undefined && object.deviceId !== null) {
      message.deviceId = object.deviceId;
    } else {
      message.deviceId = '';
    }
    if (object.comTimestamp !== undefined && object.comTimestamp !== null) {
      message.comTimestamp = object.comTimestamp;
    } else {
      message.comTimestamp = '';
    }
    if (object.lastComCause !== undefined && object.lastComCause !== null) {
      message.lastComCause = object.lastComCause;
    } else {
      message.lastComCause = '';
    }
    if (object.data !== undefined && object.data !== null) {
      for (const e of object.data) {
        message.data.push(e);
      }
    }
    return message;
  },
};

const baseMsgCreateJobResponse: object = { id: 0 };

export const MsgCreateJobResponse = {
  encode(
    message: MsgCreateJobResponse,
    writer: Writer = Writer.create(),
  ): Writer {
    if (message.id !== 0) {
      writer.uint32(8).uint64(message.id);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgCreateJobResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgCreateJobResponse } as MsgCreateJobResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = longToNumber(reader.uint64() as Long);
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateJobResponse {
    const message = { ...baseMsgCreateJobResponse } as MsgCreateJobResponse;
    if (object.id !== undefined && object.id !== null) {
      message.id = Number(object.id);
    } else {
      message.id = 0;
    }
    return message;
  },

  toJSON(message: MsgCreateJobResponse): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgCreateJobResponse>): MsgCreateJobResponse {
    const message = { ...baseMsgCreateJobResponse } as MsgCreateJobResponse;
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = 0;
    }
    return message;
  },
};

const baseMsgUpdateJob: object = {
  creator: '',
  id: 0,
  jobId: 0,
  deviceId: '',
  comTimestamp: '',
  lastComCause: '',
  data: '',
};

export const MsgUpdateJob = {
  encode(message: MsgUpdateJob, writer: Writer = Writer.create()): Writer {
    if (message.creator !== '') {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== 0) {
      writer.uint32(16).uint64(message.id);
    }
    if (message.jobId !== 0) {
      writer.uint32(24).uint64(message.jobId);
    }
    if (message.deviceId !== '') {
      writer.uint32(34).string(message.deviceId);
    }
    if (message.comTimestamp !== '') {
      writer.uint32(42).string(message.comTimestamp);
    }
    if (message.lastComCause !== '') {
      writer.uint32(50).string(message.lastComCause);
    }
    for (const v of message.data) {
      writer.uint32(58).string(v!);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgUpdateJob {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgUpdateJob } as MsgUpdateJob;
    message.data = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = longToNumber(reader.uint64() as Long);
          break;
        case 3:
          message.jobId = longToNumber(reader.uint64() as Long);
          break;
        case 4:
          message.deviceId = reader.string();
          break;
        case 5:
          message.comTimestamp = reader.string();
          break;
        case 6:
          message.lastComCause = reader.string();
          break;
        case 7:
          message.data.push(reader.string());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateJob {
    const message = { ...baseMsgUpdateJob } as MsgUpdateJob;
    message.data = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = '';
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = Number(object.id);
    } else {
      message.id = 0;
    }
    if (object.jobId !== undefined && object.jobId !== null) {
      message.jobId = Number(object.jobId);
    } else {
      message.jobId = 0;
    }
    if (object.deviceId !== undefined && object.deviceId !== null) {
      message.deviceId = String(object.deviceId);
    } else {
      message.deviceId = '';
    }
    if (object.comTimestamp !== undefined && object.comTimestamp !== null) {
      message.comTimestamp = String(object.comTimestamp);
    } else {
      message.comTimestamp = '';
    }
    if (object.lastComCause !== undefined && object.lastComCause !== null) {
      message.lastComCause = String(object.lastComCause);
    } else {
      message.lastComCause = '';
    }
    if (object.data !== undefined && object.data !== null) {
      for (const e of object.data) {
        message.data.push(String(e));
      }
    }
    return message;
  },

  toJSON(message: MsgUpdateJob): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.jobId !== undefined && (obj.jobId = message.jobId);
    message.deviceId !== undefined && (obj.deviceId = message.deviceId);
    message.comTimestamp !== undefined &&
      (obj.comTimestamp = message.comTimestamp);
    message.lastComCause !== undefined &&
      (obj.lastComCause = message.lastComCause);
    if (message.data) {
      obj.data = message.data.map((e) => e);
    } else {
      obj.data = [];
    }
    return obj;
  },

  fromPartial(object: DeepPartial<MsgUpdateJob>): MsgUpdateJob {
    const message = { ...baseMsgUpdateJob } as MsgUpdateJob;
    message.data = [];
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = '';
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = 0;
    }
    if (object.jobId !== undefined && object.jobId !== null) {
      message.jobId = object.jobId;
    } else {
      message.jobId = 0;
    }
    if (object.deviceId !== undefined && object.deviceId !== null) {
      message.deviceId = object.deviceId;
    } else {
      message.deviceId = '';
    }
    if (object.comTimestamp !== undefined && object.comTimestamp !== null) {
      message.comTimestamp = object.comTimestamp;
    } else {
      message.comTimestamp = '';
    }
    if (object.lastComCause !== undefined && object.lastComCause !== null) {
      message.lastComCause = object.lastComCause;
    } else {
      message.lastComCause = '';
    }
    if (object.data !== undefined && object.data !== null) {
      for (const e of object.data) {
        message.data.push(e);
      }
    }
    return message;
  },
};

const baseMsgUpdateJobResponse: object = {};

export const MsgUpdateJobResponse = {
  encode(_: MsgUpdateJobResponse, writer: Writer = Writer.create()): Writer {
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgUpdateJobResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgUpdateJobResponse } as MsgUpdateJobResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgUpdateJobResponse {
    const message = { ...baseMsgUpdateJobResponse } as MsgUpdateJobResponse;
    return message;
  },

  toJSON(_: MsgUpdateJobResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(_: DeepPartial<MsgUpdateJobResponse>): MsgUpdateJobResponse {
    const message = { ...baseMsgUpdateJobResponse } as MsgUpdateJobResponse;
    return message;
  },
};

const baseMsgDeleteJob: object = { creator: '', id: 0 };

export const MsgDeleteJob = {
  encode(message: MsgDeleteJob, writer: Writer = Writer.create()): Writer {
    if (message.creator !== '') {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== 0) {
      writer.uint32(16).uint64(message.id);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgDeleteJob {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgDeleteJob } as MsgDeleteJob;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = longToNumber(reader.uint64() as Long);
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgDeleteJob {
    const message = { ...baseMsgDeleteJob } as MsgDeleteJob;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = '';
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = Number(object.id);
    } else {
      message.id = 0;
    }
    return message;
  },

  toJSON(message: MsgDeleteJob): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgDeleteJob>): MsgDeleteJob {
    const message = { ...baseMsgDeleteJob } as MsgDeleteJob;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = '';
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = 0;
    }
    return message;
  },
};

const baseMsgDeleteJobResponse: object = {};

export const MsgDeleteJobResponse = {
  encode(_: MsgDeleteJobResponse, writer: Writer = Writer.create()): Writer {
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgDeleteJobResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgDeleteJobResponse } as MsgDeleteJobResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgDeleteJobResponse {
    const message = { ...baseMsgDeleteJobResponse } as MsgDeleteJobResponse;
    return message;
  },

  toJSON(_: MsgDeleteJobResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(_: DeepPartial<MsgDeleteJobResponse>): MsgDeleteJobResponse {
    const message = { ...baseMsgDeleteJobResponse } as MsgDeleteJobResponse;
    return message;
  },
};

/** Msg defines the Msg service. */
export interface Msg {
  /** this line is used by starport scaffolding # proto/tx/rpc */
  CreateJob(request: MsgCreateJob): Promise<MsgCreateJobResponse>;
  UpdateJob(request: MsgUpdateJob): Promise<MsgUpdateJobResponse>;
  DeleteJob(request: MsgDeleteJob): Promise<MsgDeleteJobResponse>;
}

export class MsgClientImpl implements Msg {
  private readonly rpc: Rpc;
  constructor(rpc: Rpc) {
    this.rpc = rpc;
  }
  CreateJob(request: MsgCreateJob): Promise<MsgCreateJobResponse> {
    const data = MsgCreateJob.encode(request).finish();
    const promise = this.rpc.request(
      'iotbroker.devicechain.devicechain.Msg',
      'CreateJob',
      data,
    );
    return promise.then((data) =>
      MsgCreateJobResponse.decode(new Reader(data)),
    );
  }

  UpdateJob(request: MsgUpdateJob): Promise<MsgUpdateJobResponse> {
    const data = MsgUpdateJob.encode(request).finish();
    const promise = this.rpc.request(
      'iotbroker.devicechain.devicechain.Msg',
      'UpdateJob',
      data,
    );
    return promise.then((data) =>
      MsgUpdateJobResponse.decode(new Reader(data)),
    );
  }

  DeleteJob(request: MsgDeleteJob): Promise<MsgDeleteJobResponse> {
    const data = MsgDeleteJob.encode(request).finish();
    const promise = this.rpc.request(
      'iotbroker.devicechain.devicechain.Msg',
      'DeleteJob',
      data,
    );
    return promise.then((data) =>
      MsgDeleteJobResponse.decode(new Reader(data)),
    );
  }
}

interface Rpc {
  request(
    service: string,
    method: string,
    data: Uint8Array,
  ): Promise<Uint8Array>;
}

declare var self: any | undefined;
declare var window: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== 'undefined') return globalThis;
  if (typeof self !== 'undefined') return self;
  if (typeof window !== 'undefined') return window;
  if (typeof global !== 'undefined') return global;
  throw 'Unable to locate global object';
})();

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error('Value is larger than Number.MAX_SAFE_INTEGER');
  }
  return long.toNumber();
}

if (util.Long !== Long) {
  util.Long = Long as any;
  configure();
}
