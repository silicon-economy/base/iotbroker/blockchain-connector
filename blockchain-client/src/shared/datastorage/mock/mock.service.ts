/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
import { Injectable, NotFoundException } from '@nestjs/common';

import { Job, Payload } from '../../../dto/job.dto';
import { DeviceData } from '../../../dto/publish.dto';
import { BCConnector } from '../bc-connector';

@Injectable()
export class MockService implements BCConnector {
  private storage: Job[] = new Array<Job>();

  async publishDeviceData(deviceData: DeviceData): Promise<void> {
    let job: Job = this.storage.find(
      (storageEntry) => storageEntry.jobId == deviceData.jobId,
    );
    let payload: Payload = new Payload(
      deviceData.comTimestamp,
      deviceData.lastComCause,
      deviceData.data,
    );
    if (job == undefined) {
      job = new Job(deviceData.jobId, String(deviceData.deviceId), [payload]);
    } else {
      job.comObj.push(payload);
    }

    this.storage.push(job);
  }

  async readJob(jobId: number): Promise<Job> {
    let job: Job = this.storage.find((data) => data.jobId == jobId);
    if (job == undefined) {
      throw new NotFoundException(`Could not find device job with id ${jobId}`);
    }
    return job;
  }
}
