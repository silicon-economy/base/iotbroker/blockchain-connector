/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
import { Test, TestingModule } from '@nestjs/testing';

import { Job, Payload } from '../../../dto/job.dto';
import { DeviceData } from '../../../dto/publish.dto';
import { MockService } from './mock.service';

describe('MockService', () => {
  let service: MockService;
  let data: DeviceData;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MockService],
    }).compile();

    data = new DeviceData(12, 5, 4711, undefined, '2020-08-13', '2020-08-11', [
      'data',
    ]);
    service = module.get<MockService>(MockService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should persist device data', async () => {
    await expect(service.publishDeviceData(data)).resolves.not.toThrow();
    await expect(service.publishDeviceData(data)).resolves.not.toThrow();
  });

  it('should throw an exception', async () => {
    await expect(service.readJob(5)).rejects.toThrow();
  });

  it('should return device data by jobId', async () => {
    await service.publishDeviceData(data);
    let payload: Payload[] = [
      new Payload(data.comTimestamp, data.lastComCause, data.data),
    ];
    let job: Job = new Job(data.jobId, String(data.deviceId), payload);
    expect(await service.readJob(5)).toStrictEqual(job);
  });
});
