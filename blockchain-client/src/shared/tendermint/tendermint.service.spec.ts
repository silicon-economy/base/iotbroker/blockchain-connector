/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
import { SignerData, SigningStargateClient } from '@cosmjs/stargate';
import { HttpModule } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import sensingConfig, {
  Sensing_Identifier,
  TendermintConfig,
} from '../../settings/tendermint.config';
import { GetSensingJobDto } from '../datastorage/sensing-puck/dto/get-tendermint.dto';
import { TendermintService } from './tendermint.service';

jest.mock('@cosmjs/stargate');

describe('TendermintService', () => {
  let service: TendermintService;
  let configService: ConfigService;
  let signerData: SignerData;
  let jobChainDto: GetSensingJobDto;
  let tendermintConfig: TendermintConfig;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        HttpModule,
        ConfigModule.forRoot({
          isGlobal: true,
          envFilePath: ['.env', '.env.tendermint'],
          load: [sensingConfig],
        }),
      ],
      providers: [TendermintService],
    }).compile();

    configService = module.get<ConfigService>(ConfigService);
    service = module.get<TendermintService>(TendermintService);

    tendermintConfig = configService.get<TendermintConfig>(Sensing_Identifier);

    jest.useRealTimers();

    jobChainDto = {
      Job: {
        id: 0,
        creator: 'A3425FE234',
        jobId: 0,
        deviceId: '42',
        payloads: [
          {
            creator: 'A3425FE234',
            comTimestamp: '2020-08-27 12:27:13',
            lastComCause: 'Info',
            data: [{ String: 'data' }],
          },
        ],
      },
    };
    signerData = {
      accountNumber: 1,
      sequence: 2,
      chainId: 'devicechain',
    };
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create signer information', async () => {
    const connect = SigningStargateClient.prototype.getSequence as jest.Mock;
    const chainId = SigningStargateClient.prototype.getChainId as jest.Mock;
    connect.mockResolvedValue({ accountNumber: 1, sequence: 2 });
    chainId.mockResolvedValue('devicechain');

    expect(
      await service.getSignerData(SigningStargateClient.prototype, 'address'),
    ).toStrictEqual(signerData);
  });

  it('should build a transaction', async () => {
    const connect = SigningStargateClient.prototype.getSequence as jest.Mock;
    const chainId = SigningStargateClient.prototype.getChainId as jest.Mock;
    const sign = SigningStargateClient.prototype.sign as jest.Mock;
    connect.mockResolvedValue({ accountNumber: 1, sequence: 2 });
    chainId.mockResolvedValue('devicechain');
    sign.mockResolvedValue(new Uint8Array(4));

    expect(
      await service.buildAndSignTransaction(
        tendermintConfig,
        tendermintConfig.createJobMsgType,
        jobChainDto,
        SigningStargateClient.prototype,
        signerData,
        'address',
      ),
    ).toStrictEqual(new Uint8Array(4));
  });
});
