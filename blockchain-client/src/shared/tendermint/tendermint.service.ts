/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
import { DirectSecp256k1HdWallet, Registry } from '@cosmjs/proto-signing';
import {
  BroadcastTxResponse,
  SequenceResponse,
  SignerData,
  SigningStargateClient,
} from '@cosmjs/stargate';
import { HttpService, Injectable, Logger } from '@nestjs/common';

import { TendermintConfig } from '../../settings/tendermint.config';
import { TxRaw } from '@cosmjs/stargate/build/codec/cosmos/tx/v1beta1/tx';

@Injectable()
export class TendermintService {
  private logger = new Logger(TendermintService.name);
  private account: number;
  private sequence: number;

  constructor(private httpService: HttpService) {
    this.account = 0;
  }

  public async readFromChain(
    restEndpoint: string,
    tendermint: TendermintConfig,
  ): Promise<any> {
    let restURL = `${tendermint.nodeUri}:${tendermint.nodeRestPort}${tendermint.restUri}${restEndpoint}`;

    return (await this.httpService.get(restURL).toPromise()).data;
  }

  public async writeToChain(
    msgType: string,
    msgValue: any,
    tendermint: TendermintConfig,
    types: any[][],
  ): Promise<BroadcastTxResponse> {
    try {
      const wallet = await DirectSecp256k1HdWallet.fromMnemonic(
        tendermint.bcIdentity,
      );
      const [firstAccount] = await wallet.getAccounts();
      msgValue.creator = firstAccount.address;
      const client = await this.buildStargateClient(wallet, tendermint, types);
      let signerData: SignerData = await this.getSignerData(
        client,
        firstAccount.address,
      );
      let signedTx = await this.buildAndSignTransaction(
        tendermint,
        msgType,
        msgValue,
        client,
        signerData,
        firstAccount.address,
      );

      return await client.broadcastTx(
        Uint8Array.from(TxRaw.encode(signedTx).finish()),
      );
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }

  public async buildAndSignTransaction(
    tendermint: TendermintConfig,
    msgType: string,
    msgValue: any,
    client: SigningStargateClient,
    signerData: SignerData,
    bcAddress: string,
  ): Promise<TxRaw> {
    const type = `${tendermint.chainType}.${msgType}`;

    const transaction = {
      typeUrl: type,
      value: msgValue,
    };

    const fee = {
      amount: [
        {
          denom: 'token',
          amount: '0',
        },
      ],
      gas: tendermint.gas,
    };

    return client.sign(bcAddress, [transaction], fee, '', signerData);
  }

  public async getSignerData(
    client: SigningStargateClient,
    bcAdress: string,
  ): Promise<SignerData> {
    let sequence: SequenceResponse = await client
      .getSequence(bcAdress)
      .then((res) => {
        return res;
      });
    let chainId: string = await client.getChainId().then((res) => {
      return res;
    });

    if (this.sequence == null) {
      this.sequence = sequence.sequence;
    } else {
      this.sequence++;
    }

    return {
      accountNumber: sequence.accountNumber,
      sequence: this.sequence,
      chainId: chainId,
    };
  }

  private async buildStargateClient(
    wallet: DirectSecp256k1HdWallet,
    tendermint: TendermintConfig,
    types: any[][],
  ): Promise<SigningStargateClient> {
    try {
      let registry: Registry = new Registry(<any>types);

      return SigningStargateClient.connectWithSigner(
        `${tendermint.nodeUri}`,
        wallet,
        {
          registry,
        },
      );
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }
}
