/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
import { HttpModule, Module } from '@nestjs/common';

import { TendermintService } from './tendermint.service';

@Module({
  controllers: [],
  providers: [TendermintService],
  imports: [HttpModule],
  exports: [TendermintService],
})
export class TendermintModule {}
