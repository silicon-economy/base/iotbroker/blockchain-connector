/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AppConfiguration } from './settings/app.config';
import dragonConfig from './settings/dragon.config';
import sensingConfig from './settings/tendermint.config';
import { DragonPuckModule } from './shared/datastorage/dragon-puck/dragon-puck.module';
import { MockModule } from './shared/datastorage/mock/mock.module';
import { SensingPuckModule } from './shared/datastorage/sensing-puck/sensing-puck.module';

@Module({
  imports: [
    MockModule,
    DragonPuckModule,
    SensingPuckModule,
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: [
        '.env',
        '.env.cluster',
        '.env.tendermint',
        '.env.dragon.tendermint',
      ],
      load: [sensingConfig, dragonConfig],
    }),
  ],
  controllers: [AppController],
  providers: [AppService, AppConfiguration],
})
export class AppModule {}
