/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
import { Injectable } from '@nestjs/common';

import { Job } from './dto/job.dto';
import { DeviceData } from './dto/publish.dto';
import { AppConfiguration } from './settings/app.config';
import { DeviceSources } from './settings/deviceSources.enum';
import { BCConnector } from './shared/datastorage/bc-connector';
import { DragonPuckService } from './shared/datastorage/dragon-puck/dragon-puck.service';
import { MockService } from './shared/datastorage/mock/mock.service';
import { SensingPuckService } from './shared/datastorage/sensing-puck/sensing-puck.service';

@Injectable()
export class AppService {
  constructor(
    private mockService: MockService,
    private dragonService: DragonPuckService,
    private sensingService: SensingPuckService,
    private configService: AppConfiguration,
  ) {}

  async getByJobId(jobId: number, deviceSource: DeviceSources): Promise<Job> {
    let datastorage: BCConnector = this.getDatastorage(deviceSource);
    return datastorage.readJob(jobId);
  }

  async publishToBC(
    data: DeviceData,
    deviceSource: DeviceSources,
  ): Promise<void> {
    let datastorage: BCConnector = this.getDatastorage(deviceSource);
    await datastorage.publishDeviceData(data);
  }

  private getDatastorage(deviceSource: DeviceSources): BCConnector {
    let datastorage: BCConnector;
    if (this.configService.isUseMock() === true) {
      datastorage = this.mockService;
    } else if (deviceSource === DeviceSources.dragonPuck) {
      datastorage = this.dragonService;
    } else {
      datastorage = this.sensingService;
    }

    return datastorage;
  }
}
