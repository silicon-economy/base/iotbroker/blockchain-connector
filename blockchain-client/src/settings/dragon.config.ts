/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
import { registerAs } from '@nestjs/config';

export const Dragon_Identifier = 'dragon';

export default registerAs(Dragon_Identifier, () => ({
  nodeUri: process.env.DRAGON_NODE_URI,
  nodeRpcPort: process.env.DRAGON_NODE_RPC_PORT,
  nodeRestPort: process.env.DRAGON_NODE_REST_PORT,
  restUri: process.env.DRAGON_REST_URI,
  restGetJob: process.env.DRAGON_REST_GET_JOB,
  gas: process.env.GAS,
  bcIdentity: process.env.DRAGON_MNEMONIC,
  chainType: process.env.DRAGONCHAIN_TYPE,
  createJobMsgType: process.env.DRAGON_CREATE_JOB_MSG_TYPE,
}));
