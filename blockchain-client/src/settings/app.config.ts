/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AppConfiguration {
  constructor(private configService: ConfigService) {}

  public isUseMock(): boolean {
    const retVal = this.configService.get('USE_MOCK');
    return retVal == 'true';
  }
}
