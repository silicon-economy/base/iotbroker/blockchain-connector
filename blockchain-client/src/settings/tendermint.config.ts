/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
import { registerAs } from '@nestjs/config';

export const Sensing_Identifier = 'sensing';

export interface TendermintConfig {
  nodeUri: string;
  nodeRpcPort: string;
  nodeRestPort: string;
  restUri: string;
  restGetJob: string;
  gas: string;
  chainType: string;
  createJobMsgType: string;
  bcIdentity: string;
}

export default registerAs(Sensing_Identifier, () => ({
  nodeUri: process.env.NODE_URI,
  nodeRpcPort: process.env.NODE_RPC_PORT,
  nodeRestPort: process.env.NODE_REST_PORT,
  restUri: process.env.REST_URI,
  restGetJob: process.env.REST_GET_JOB,
  gas: process.env.GAS,
  bcIdentity: process.env.SENSING_MNEMONIC,
  chainType: process.env.DEVICECHAIN_TYPE,
  createJobMsgType: process.env.CREATE_JOB_MSG_TYPE,
}));
