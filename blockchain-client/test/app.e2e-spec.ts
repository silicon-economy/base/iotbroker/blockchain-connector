/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import { MockModule } from '../src/shared/datastorage/mock/mock.module';
import { MockService } from '../src/shared/datastorage/mock/mock.service';
import { DeviceData } from '../src/dto/publish.dto';

describe('AppController (e2e) mock', () => {
  let app: INestApplication;
  let data = JSON.stringify(
    new DeviceData(12, 5, 4711, undefined, '2020-08-13', '2020-08-11', [
      'data',
    ]),
  );
  let mockService = { readFromChain: () => data };

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule, MockModule],
    })
      .overrideProvider(MockService)
      .useValue(mockService)
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/GET device data', async () => {
    return await request(app.getHttpServer())
      .get('/5')
      .expect(200)
      .expect(data);
  });

  afterAll(async () => {
    await app.close();
  });
});

describe('AppController (e2e)', () => {
  let app: INestApplication;
  let data: DeviceData = new DeviceData(
    12,
    5,
    4711,
    undefined,
    '2020-08-13',
    '2020-08-11',
    ['data'],
  );

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule, MockModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/POST publish new device data', async () => {
    // await request(app.getHttpServer).post('/publish').send(data).expect(201);
    //return await request(app.getHttpServer).get('/5').expect(200).expect(data);
  });

  afterAll(async () => {
    await app.close();
  });
});
