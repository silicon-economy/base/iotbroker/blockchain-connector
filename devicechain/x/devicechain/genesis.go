/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package devicechain

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	"siliconeconomy.org/iotbroker/devicechain/x/devicechain/keeper"
	"siliconeconomy.org/iotbroker/devicechain/x/devicechain/types"
)

// InitGenesis initializes the capability module's state from a provided genesis
// state.
func InitGenesis(ctx sdk.Context, k keeper.Keeper, genState types.GenesisState) {
	// this line is used by starport scaffolding # genesis/module/init
	// Set all the job
	for _, elem := range genState.JobList {
		k.SetJob(ctx, *elem)
	}

	// Set job count
	k.SetJobCount(ctx, genState.JobCount)

	// this line is used by starport scaffolding # ibc/genesis/init
}

// ExportGenesis returns the capability module's exported genesis.
func ExportGenesis(ctx sdk.Context, k keeper.Keeper) *types.GenesisState {
	genesis := types.DefaultGenesis()

	// this line is used by starport scaffolding # genesis/module/export
	// Get all job
	jobList := k.GetAllJob(ctx)
	for _, elem := range jobList {
		elem := elem
		genesis.JobList = append(genesis.JobList, &elem)
	}

	// Set the current count
	genesis.JobCount = k.GetJobCount(ctx)

	// this line is used by starport scaffolding # ibc/genesis/export

	return genesis
}
