/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package cli

import (
	"strconv"

	"github.com/spf13/cobra"

	"github.com/spf13/cast"

	"github.com/cosmos/cosmos-sdk/client"
	"github.com/cosmos/cosmos-sdk/client/flags"
	"github.com/cosmos/cosmos-sdk/client/tx"
	"siliconeconomy.org/iotbroker/devicechain/x/devicechain/types"
)

func CmdCreateJob() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "create-job [jobId] [deviceId] [comTimestamp] [lastComCause] [data]",
		Short: "Create a new job",
		Args:  cobra.MinimumNArgs(5),
		RunE: func(cmd *cobra.Command, args []string) error {
			argsJobId, err := strconv.ParseUint(args[0], 10, 64)
			if err != nil {
				return err
			}
			argsDeviceId, err := cast.ToStringE(args[1])
			if err != nil {
				return err
			}
			argsComTimestamp, err := cast.ToStringE(args[2])
			if err != nil {
				return err
			}
			argsLastComCause, err := cast.ToStringE(args[3])
			if err != nil {
				return err
			}
			argsData := args[4:len(args)]

			clientCtx, err := client.GetClientTxContext(cmd)
			if err != nil {
				return err
			}

			msg := types.NewMsgCreateJob(clientCtx.GetFromAddress().String(), argsJobId, argsDeviceId, argsComTimestamp, argsLastComCause, argsData)
			if err := msg.ValidateBasic(); err != nil {
				return err
			}
			return tx.GenerateOrBroadcastTxCLI(clientCtx, cmd.Flags(), msg)
		},
	}

	flags.AddTxFlagsToCmd(cmd)

	return cmd
}

func CmdUpdateJob() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "update-job [jobId] [deviceId] [comTimestamp] [lastComCause] [data]",
		Short: "Update a job",
		Args:  cobra.MinimumNArgs(6),
		RunE: func(cmd *cobra.Command, args []string) error {
			id, err := strconv.ParseUint(args[0], 10, 64)
			if err != nil {
				return err
			}

			argsJobId, err := strconv.ParseUint(args[1], 10, 64)
			if err != nil {
				return err
			}

			argsDeviceId, err := cast.ToStringE(args[2])
			if err != nil {
				return err
			}

			argsComTimestamp, err := cast.ToStringE(args[3])
			if err != nil {
				return err
			}

			argsLastComCause, err := cast.ToStringE(args[4])
			if err != nil {
				return err
			}

			argsData := args[5:len(args)]

			clientCtx, err := client.GetClientTxContext(cmd)
			if err != nil {
				return err
			}

			msg := types.NewMsgUpdateJob(clientCtx.GetFromAddress().String(), id, argsJobId, argsDeviceId, argsComTimestamp, argsLastComCause, argsData)
			if err := msg.ValidateBasic(); err != nil {
				return err
			}
			return tx.GenerateOrBroadcastTxCLI(clientCtx, cmd.Flags(), msg)
		},
	}

	flags.AddTxFlagsToCmd(cmd)

	return cmd
}

func CmdDeleteJob() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "delete-job [id]",
		Short: "Delete a job by id",
		Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			id, err := strconv.ParseUint(args[0], 10, 64)
			if err != nil {
				return err
			}

			clientCtx, err := client.GetClientTxContext(cmd)
			if err != nil {
				return err
			}

			msg := types.NewMsgDeleteJob(clientCtx.GetFromAddress().String(), id)
			if err := msg.ValidateBasic(); err != nil {
				return err
			}
			return tx.GenerateOrBroadcastTxCLI(clientCtx, cmd.Flags(), msg)
		},
	}

	flags.AddTxFlagsToCmd(cmd)

	return cmd
}
