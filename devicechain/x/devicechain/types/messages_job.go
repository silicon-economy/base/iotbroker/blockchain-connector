/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
)

var _ sdk.Msg = &MsgCreateJob{}

func NewMsgCreateJob(creator string, jobId uint64, deviceId string, comTimestamp string, lastComCause string, data []string) *MsgCreateJob {
	return &MsgCreateJob{
		Creator:      creator,
		JobId:        jobId,
		DeviceId:     deviceId,
		ComTimestamp: comTimestamp,
		LastComCause: lastComCause,
		Data:         data,
	}
}

func (msg *MsgCreateJob) Route() string {
	return RouterKey
}

func (msg *MsgCreateJob) Type() string {
	return "CreateJob"
}

func (msg *MsgCreateJob) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgCreateJob) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgCreateJob) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgUpdateJob{}

func NewMsgUpdateJob(creator string, id uint64, jobId uint64, deviceId string, comTimestamp string, lastComCause string, data []string) *MsgUpdateJob {
	return &MsgUpdateJob{
		Id:           id,
		Creator:      creator,
		JobId:        jobId,
		DeviceId:     deviceId,
		ComTimestamp: comTimestamp,
		LastComCause: lastComCause,
		Data:         data,
	}
}

func (msg *MsgUpdateJob) Route() string {
	return RouterKey
}

func (msg *MsgUpdateJob) Type() string {
	return "UpdateJob"
}

func (msg *MsgUpdateJob) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgUpdateJob) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgUpdateJob) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgDeleteJob{}

func NewMsgDeleteJob(creator string, id uint64) *MsgDeleteJob {
	return &MsgDeleteJob{
		Id:      id,
		Creator: creator,
	}
}
func (msg *MsgDeleteJob) Route() string {
	return RouterKey
}

func (msg *MsgDeleteJob) Type() string {
	return "DeleteJob"
}

func (msg *MsgDeleteJob) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgDeleteJob) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgDeleteJob) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}
