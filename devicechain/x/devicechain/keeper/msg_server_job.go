/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package keeper

import (
	"context"
	"fmt"

	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
	"siliconeconomy.org/iotbroker/devicechain/x/devicechain/types"
)

func (k msgServer) CreateJob(goCtx context.Context, msg *types.MsgCreateJob) (*types.MsgCreateJobResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)
	var id uint64

	if k.HasJob(ctx, msg.JobId) {
		var msg = types.MsgUpdateJob{
			Id:           msg.JobId,
			Creator:      msg.Creator,
		    JobId:        msg.JobId,
		    DeviceId:     msg.DeviceId,
		    ComTimestamp: msg.ComTimestamp,
		    LastComCause: msg.LastComCause,
		    Data:         msg.Data,
		}
		k.UpdateJob(goCtx, &msg)
		id = msg.JobId
	} else {
		var payload = types.Payload{
			ComTimestamp: msg.ComTimestamp,
			LastComCause: msg.LastComCause,
			Data:         msg.Data,
			Creator:      msg.Creator,
		}
		
		var job = types.Job{
			Creator:      msg.Creator,
			DeviceId:     msg.DeviceId,
			JobId:        msg.JobId,
		}

		job.Payloads = append(job.Payloads, &payload) 

		id = k.AppendJob(
			ctx,
			job,
		)
	}

	return &types.MsgCreateJobResponse{
		Id: id,
	}, nil
}

func (k msgServer) UpdateJob(goCtx context.Context, msg *types.MsgUpdateJob) (*types.MsgUpdateJobResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)
	// Checks that the element exists
	if !k.HasJob(ctx, msg.Id) {
		return nil, sdkerrors.Wrap(sdkerrors.ErrKeyNotFound, fmt.Sprintf("key %d doesn't exist", msg.Id))
	}

	var job = k.GetJob(ctx, msg.JobId)
	var payload = types.Payload{
		ComTimestamp: msg.ComTimestamp,
		LastComCause: msg.LastComCause,
		Data:         msg.Data,
		Creator:      msg.Creator,
	}

	job.Payloads = append(job.Payloads, &payload) 
	
	k.SetJob(ctx, job)

	return &types.MsgUpdateJobResponse{}, nil
}

func (k msgServer) DeleteJob(goCtx context.Context, msg *types.MsgDeleteJob) (*types.MsgDeleteJobResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	if !k.HasJob(ctx, msg.Id) {
		return nil, sdkerrors.Wrap(sdkerrors.ErrKeyNotFound, fmt.Sprintf("key %d doesn't exist", msg.Id))
	}

	k.RemoveJob(ctx, msg.Id)

	return &types.MsgDeleteJobResponse{}, nil
}
