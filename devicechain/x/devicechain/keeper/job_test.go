/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package keeper

import (
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/stretchr/testify/assert"
	"siliconeconomy.org/iotbroker/devicechain/x/devicechain/types"
)

func createNJob(keeper *Keeper, ctx sdk.Context, n int) []types.Job {
	items := make([]types.Job, n)
	for i := range items {
		items[i].Creator = "any"
		items[i].JobId = uint64(i)
		items[i].Id = keeper.AppendJob(ctx, items[i])
	}
	return items
}

func TestJobGet(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	items := createNJob(keeper, ctx, 10)
	for _, item := range items {
		assert.Equal(t, item, keeper.GetJob(ctx, item.Id))
	}
}

func TestJobExist(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	items := createNJob(keeper, ctx, 10)
	for _, item := range items {
		assert.True(t, keeper.HasJob(ctx, item.Id))
	}
}

func TestJobRemove(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	items := createNJob(keeper, ctx, 10)
	for _, item := range items {
		keeper.RemoveJob(ctx, item.Id)
		assert.False(t, keeper.HasJob(ctx, item.Id))
	}
}

func TestJobGetAll(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	items := createNJob(keeper, ctx, 10)
	assert.Equal(t, items, keeper.GetAllJob(ctx))
}

func TestJobCount(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	items := createNJob(keeper, ctx, 10)
	count := uint64(len(items))
	assert.Equal(t, count, keeper.GetJobCount(ctx))
}
