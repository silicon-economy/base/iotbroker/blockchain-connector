/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package keeper

import (
	"context"

	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
	"github.com/cosmos/cosmos-sdk/types/query"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"siliconeconomy.org/iotbroker/devicechain/x/devicechain/types"
)

func (k Keeper) JobAll(c context.Context, req *types.QueryAllJobRequest) (*types.QueryAllJobResponse, error) {
	if req == nil {
		return nil, status.Error(codes.InvalidArgument, "invalid request")
	}

	var jobs []*types.Job
	ctx := sdk.UnwrapSDKContext(c)

	store := ctx.KVStore(k.storeKey)
	jobStore := prefix.NewStore(store, types.KeyPrefix(types.JobKey))

	pageRes, err := query.Paginate(jobStore, req.Pagination, func(key []byte, value []byte) error {
		var job types.Job
		if err := k.cdc.UnmarshalBinaryBare(value, &job); err != nil {
			return err
		}

		jobs = append(jobs, &job)
		return nil
	})

	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &types.QueryAllJobResponse{Job: jobs, Pagination: pageRes}, nil
}

func (k Keeper) Job(c context.Context, req *types.QueryGetJobRequest) (*types.QueryGetJobResponse, error) {
	if req == nil {
		return nil, status.Error(codes.InvalidArgument, "invalid request")
	}

	var job types.Job
	ctx := sdk.UnwrapSDKContext(c)

	if !k.HasJob(ctx, req.Id) {
		return nil, sdkerrors.ErrKeyNotFound
	}

	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.JobKey))
	k.cdc.MustUnmarshalBinaryBare(store.Get(GetJobIDBytes(req.Id)), &job)

	return &types.QueryGetJobResponse{Job: &job}, nil
}
