/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package keeper

import (
	"testing"

	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"siliconeconomy.org/iotbroker/devicechain/x/devicechain/types"
)

func TestJobMsgServerCreate(t *testing.T) {
	srv, ctx := setupMsgServer(t)
	creator := "A"
	for i := 0; i < 5; i++ {
		var jobId = uint64(i)
		var msg = types.NewMsgCreateJob(creator, jobId, "42", "2021-11-22 11:46", "Alert", []string{"test"})
		resp, err := srv.CreateJob(ctx, msg)
		require.NoError(t, err)
		assert.Equal(t, i, int(resp.Id))
	}
}

func TestJobMsgServerUpdate(t *testing.T) {
	creator := "A"

	for _, tc := range []struct {
		desc    string
		request *types.MsgUpdateJob
		err     error
	}{
		{
			desc:    "Completed",
			request:  types.NewMsgUpdateJob(creator, 1, 1, "42", "2021-11-22 11:49", "Info", []string{"testUpdate"}),
		},
		{
			desc:    "Unauthorized",
			request:  types.NewMsgUpdateJob(creator, 10, 10, "42", "2021-11-22 11:49", "Info", []string{"testUpdate"}),
			err:     sdkerrors.ErrKeyNotFound,
		},
	} {
		tc := tc
		t.Run(tc.desc, func(t *testing.T) {
			srv, ctx := setupMsgServer(t)
			var msgCreate = types.NewMsgCreateJob(creator, 1, "42", "2021-11-22 11:46", "Alert", []string{"test"})
			_, err := srv.CreateJob(ctx, msgCreate)
			require.NoError(t, err)

			_, err = srv.UpdateJob(ctx, tc.request)
			if tc.err != nil {
				require.ErrorIs(t, err, tc.err)
			} else {
				require.NoError(t, err)
			}
		})
	}
}

func TestJobMsgServerDelete(t *testing.T) {
	creator := "A"

	for _, tc := range []struct {
		desc    string
		request *types.MsgDeleteJob
		err     error
	}{
		{
			desc:    "Completed",
			request: types.NewMsgDeleteJob(creator, 1),
		},
		{
			desc:    "KeyNotFound",
			request: types.NewMsgDeleteJob(creator, 10),
			err:     sdkerrors.ErrKeyNotFound,
		},
	} {
		tc := tc
		t.Run(tc.desc, func(t *testing.T) {
			srv, ctx := setupMsgServer(t)
			var msgCreate = types.NewMsgCreateJob(creator, 1, "42", "2021-11-22 11:46", "Alert", []string{"test"})

			_, err := srv.CreateJob(ctx, msgCreate)
			require.NoError(t, err)
			_, err = srv.DeleteJob(ctx, tc.request)
			if tc.err != nil {
				require.ErrorIs(t, err, tc.err)
			} else {
				require.NoError(t, err)
			}
		})
	}
}
