/*
 * Copyright (c) Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package keeper

import (
	"siliconeconomy.org/iotbroker/devicechain/x/devicechain/types"
)

var _ types.QueryServer = Keeper{}
