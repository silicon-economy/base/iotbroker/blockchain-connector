# devicechain
**devicechain** is a blockchain built using Cosmos SDK and Tendermint and created with [Starport](https://github.com/tendermint/starport).

## Licenses of third-party dependencies

The licenses used by this project's third-party dependencies are documented in the `third-party-licenses` directory.
This is done to encourage developers to check the licenses used by the third-party dependencies to ensure they do not conflict with the license of this project itself.
The directory contains the following files:

* `third-party-licenses.txt` - Contains the licenses used by this project's third-party dependencies.
  The content of this file is/can be generated.
* `third-party-licenses-complementary.txt` - Contains entries for third-party dependencies for which the licenses cannot be determined automatically.
  The content of this file is maintained manually.
* `third-party-licenses-summary.txt` - Contains a summary of all licenses used by the third-party dependencies in this project.
  The content of this file is/can be generated.

### Generating third-party license reports

This project uses the [go-licenses](https://opensource.google/projects/go-licenses) to generate a file containing the licenses used by the third-party dependencies.
The `third-party-licenses/third-party-licenses.txt` file can be generated using the following command:

`go-licenses csv "siliconeconomy.org/iotbroker/devicechain/x/devicechain" >> ./third-party-licenses/third-party-licenses.txt`

Third-party dependencies for which the licenses cannot be determined automatically by the license-checker have to be documented manually in `third-party-licenses/third-party-licenses-complementary.txt`.
In the `third-party-licenses/third-party-licenses.txt` file these third-party dependencies have an "UNKNOWN" license.
