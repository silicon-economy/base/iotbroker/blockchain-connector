#!/bin/bash
#
# Copyright (c) Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3
#


# turn on bash's job control
set -m
  
# Start the primary process and put it in the background
devicechaind start &
# Start the helper process
# the my_helper_process might need to know how to wait on the
# primary process to start before it does its work and returns
sleep 20s #Wait for tendermint demon start

printf 'vague problem top into feature strategy pony earn strike rule coast desert lens bracket spatial boil tooth buzz elephant usual judge wash witness save\n' | devicechaind keys add broker --recover
yes | devicechaind tx bank send $(devicechaind keys show alice -a) $(devicechaind keys show broker -a) 1token
printf 'visa atom improve toast night venue route armed hip chunk oyster warm sail holiday teach horse fantasy health trust unlock machine soon modify reveal\n' | devicechaind keys add broker2 --recover
yes | devicechaind tx bank send $(devicechaind keys show alice -a) $(devicechaind keys show broker2 -a) 1token
printf 'width tuition civil now viable subject above quick couch govern urban palace team indicate spend arctic grain tunnel lesson palm favorite normal address taste\n' | devicechaind keys add broker3 --recover
yes | devicechaind tx bank send $(devicechaind keys show alice -a) $(devicechaind keys show broker3 -a) 1token
printf 'jump very circle garbage spend bean later draft day prefer share relief narrow rib hawk labor cook artefact kit satisfy acoustic electric affair release\n' | devicechaind keys add broker4 --recover
yes | devicechaind tx bank send $(devicechaind keys show alice -a) $(devicechaind keys show broker4 -a) 1token
printf 'excess also drift magnet frog iron cotton chief frost elephant cherry virtual century mosquito climb lion opera subway riot nasty oxygen near fresh vendor\n' | devicechaind keys add broker5 --recover
yes | devicechaind tx bank send $(devicechaind keys show alice -a) $(devicechaind keys show broker5 -a) 1token

# now we bring the primary process back into the foreground
# and leave it there
fg %1