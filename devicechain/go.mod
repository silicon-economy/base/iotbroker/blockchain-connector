module siliconeconomy.org/iotbroker/devicechain

go 1.16

require (
	github.com/cosmos/cosmos-sdk v0.42.8
	github.com/gogo/protobuf v1.3.3
	github.com/golang/protobuf v1.5.2
	github.com/google/go-cmp v0.5.6 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/grpc-ecosystem/grpc-gateway v1.16.0
	github.com/spf13/cast v1.3.1
	github.com/spf13/cobra v1.1.3
	github.com/stretchr/testify v1.7.0
	github.com/tendermint/spm v0.0.2
	github.com/tendermint/tendermint v0.34.11
	github.com/tendermint/tm-db v0.6.4
	go.elastic.co/go-licence-detector v0.5.0 // indirect
	google.golang.org/genproto v0.0.0-20211118181313-81c1377c94b1
	google.golang.org/grpc v1.42.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)

replace google.golang.org/grpc => google.golang.org/grpc v1.33.2

replace github.com/gogo/protobuf => github.com/regen-network/protobuf <!--
  - Copyright (c) Open Logistics Foundation
  -
  - Licensed under the Open Logistics Foundation License 1.3.
  - For details on the licensing terms, see the LICENSE file.
  - SPDX-License-Identifier: OLFL-1.3
  -->

v1.3.3-alpha.regen.1
